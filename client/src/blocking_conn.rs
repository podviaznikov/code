#![allow(unused_imports, dead_code)]


use std::{convert};
use std::io::{self, Read, Write, BufReader, BufRead};
use std::net::{TcpStream};
use std::collections::{VecDeque};
use std::time::{Duration};
use openssl::ssl::{self, SslMethod, SslConnector, SslStream};
use edsu_common::eson::{self, Eson, Key};
use edsu_common::edsu::{self, Username, ServerMsg, ClientMsg, Value, OobCode, Multihash, Name,
                        Block, ValueRead };
use edsu_common::block_store::{self, BlockStore};
use edsu_common::prelude::*;

/// This is very barebones for the time being - it's mostly being used internally for tools 
/// related to edsud.  Notably it doesn't do chaining, subs, redirects, and retries, and it
/// won't respond to pings unless there's a call in progress, or a call is made within the
/// the server's response window.  If you want a persistent connection, send a message to the
/// server periodically, even if it's just a ping

#[derive(Debug)]
pub enum Error {
    Ssl(ssl::Error),
    SslHandshake(ssl::HandshakeError<TcpStream>),
    Io(io::Error),
    Edsu(edsu::Error),
    Eson(eson::Error),
    ProtocolVersionMismatch(String),
    UnexpectedResponse(String),
    Oob(ServerMsg),
    Unimplemented(String),
    AuthenticationError,
    MissingBlock(Multihash),
    NotFound,
}
impl convert::From<ssl::Error> for Error { fn from(e: ssl::Error) -> Self { Error::Ssl(e) } }
impl convert::From<ssl::HandshakeError<TcpStream>> for Error {
    fn from(e: ssl::HandshakeError<TcpStream>) -> Self { Error::SslHandshake(e) } }
impl convert::From<io::Error> for Error { fn from(e: io::Error) -> Self { Error::Io(e) } }
impl convert::From<edsu::Error> for Error { fn from(e: edsu::Error) -> Self { Error::Edsu(e) } }
impl convert::From<eson::Error> for Error { fn from(e: eson::Error) -> Self { Error::Eson(e) } }


pub struct Conn {
    username: Username,
    token: Option<Value>,
    visitor: Option<Username>,
    stream: SslStream<TcpStream>,
    parser: edsu::Parser,
    read_buffer: Vec<u8>,
    msgs: VecDeque<ServerMsg>,
}

impl Conn {
    pub fn new(username: Username, token: Option<Value>, visitor: Option<Username>, buffer_size: usize)
        -> Result<Self, Error>
    {
        // Determine the host
        let mut host = username.id.to_owned();
        host.extend(".edsu.".chars());
        host.extend(username.domain.chars());

        // Make the SSL connection
        let stream = TcpStream::connect((host.as_str(), EDSU_PORT))?;
        let connector = SslConnector::builder(SslMethod::tls()).unwrap().build();
        let mut stream = connector.connect(&host, stream)?;

        // Say hello
        let needs_auth;
        match (&token, &visitor) {
            (&Some(ref tok), &None) => {
                stream.write_all(b"edsu hello\nversions 0.1\ntoken ")?;
                stream.write_all(tok.as_bytes())?;
                stream.write_all(b"\n\n")?;
                stream.flush()?;
                needs_auth = true;
            },
            (&Some(_), &Some(_)) => return Err(Error::Unimplemented("visiting".to_owned())),
            _ => {
                stream.write_all(b"edsu hello\nversions 0.1\n\n")?;
                stream.flush()?;
                needs_auth = false;
            }
        }

        // Construct the conn
        let read_buffer = vec![0; buffer_size];
        let parser = edsu::Parser::new();
        let msgs = VecDeque::new();
        let mut conn = Conn { username, token, visitor, stream, parser, read_buffer, msgs };

        // Wait for server hello
        let msg = conn.get_msg()?;
        if let ServerMsg::Hello { version, .. } = msg {
            let vers_string = version.as_string();
            if &vers_string != "0.1" {
                return Err(Error::ProtocolVersionMismatch(
                    format!("got {}, we only speak 0.1", vers_string)));
            }
        } else {
            return Err(Error::UnexpectedResponse(format!("expecting hello, got {:?}", msg)));
        }

        // Wait for auth (if we sent a token)
        if needs_auth {
            match conn.get_msg()? {
                ServerMsg::Authenticated { .. } => (),
                ServerMsg::Oob { code: OobCode::AuthenticationError, .. } => {
                    return Err(Error::AuthenticationError);
                },
                x => return Err(oob_or_unexpected(x)),
            }
        }

        // We're connected!
        Ok(conn)
    }
    pub fn set_timeout(&mut self, duration: Duration) -> Result<(), Error> {
        self.stream.get_ref().set_read_timeout(Some(duration))?;
        self.stream.get_ref().set_write_timeout(Some(duration))?;
        Ok(())
    }
    fn get_msg(&mut self) -> Result<ServerMsg, Error> {
        loop {
            // If there's one in the queue, return that
            if let Some(msg) = self.msgs.pop_front() {
                return Ok(msg);
            }

            // Read bytes off the stream
            let bytes_read;
            loop {
                match self.stream.read(&mut self.read_buffer) {
                    Ok(x) => {
                        bytes_read = x;
                        break;
                    },
                    Err(ref e) if e.kind() == io::ErrorKind::Interrupted => (), // loop
                    Err(e) => Err(e)?,
                };
            };

            // Parse the bytes until the iter is empty, collecting the messages
            let mut iter = self.read_buffer[..bytes_read].iter().cloned().peekable();
            while iter.peek().is_some() {
                if let Some(msg) = self.parser.add_bytes(&mut iter)? {
                    // Ignore anything to do with advisories, chaining, subs, etc.
                    match msg {
                        ServerMsg::Pong { .. } => (),
                        ServerMsg::SubNotify { .. } => (),
                        ServerMsg::Ping { .. } => {
                            // Ignore, but respond
                            self.stream.write_all(b"edsu pong\n\n")?;
                        },
                        ServerMsg::Oob { code: OobCode::Advisory, .. } => (),
                        ServerMsg::Oob { code: OobCode::Redirect, .. } => {
                            return Err(Error::Unimplemented("redirects".to_owned()));
                        },
                        _ => self.msgs.push_back(msg),
                    }
                }
            }
        }
    }
    pub fn block_put(&mut self, block: &Block) -> Result<Multihash, Error> {
        let bytes = block.as_bytes();
        self.stream.write_all(b"edsu block-put\npayload-length ")?;
        self.stream.write_all(format!("{}\n\n", bytes.len()).as_bytes())?;
        self.stream.write_all(bytes)?;
        self.stream.write_all(b"\n")?;
        self.stream.flush()?;

        match self.get_msg()? {
            ServerMsg::Ok_ { hash: Some(hash), .. } => return Ok(hash),
            x => return Err(oob_or_unexpected(x)),
        }
    }
    pub fn block_get(&mut self, hash: &Multihash) -> Result<Block, Error> {
        self.stream.write_all(b"edsu block-get\nhash ")?;
        self.stream.write_all(hash.as_bytes())?;
        self.stream.write_all(b"\n\n")?;
        self.stream.flush()?;

        match self.get_msg()? {
            ServerMsg::Block { payload, .. } => {
                let block = Block::from_bytes(payload)?;
                return Ok(block)
            },
            ServerMsg::Oob { code: OobCode::NotFound, .. } => return Err(Error::NotFound),
            x => return Err(oob_or_unexpected(x)),
        }
    }
    pub fn name_get(&mut self, name: &Name) -> Result<Option<Multihash>, Error> {
        self.stream.write_all(b"edsu name-get\nname ")?;
        self.stream.write_all(name.as_bytes())?;
        self.stream.write_all(b"\n\n")?;
        self.stream.flush()?;

        match self.get_msg()? {
            ServerMsg::Name { hash, .. } => Ok(Some(hash)),
            ServerMsg::Oob { code: OobCode::NotFound, .. } => return Ok(None),
            x => return Err(oob_or_unexpected(x)),
        }
    }
    pub fn name_get_req(&mut self, name: &Name) -> Result<Multihash, Error> {
        if let Some(x) = self.name_get(name)? { Ok(x) } else { Err(Error::NotFound) }
    }
    pub fn name_block_get(&mut self, name: &Name) -> Result<Option<Eson>, Error> {
        self.stream.write_all(b"edsu name-get\nname ")?;
        self.stream.write_all(name.as_bytes())?;
        self.stream.write_all(b"\n\n")?;
        self.stream.flush()?;

        match self.get_msg()? {
            ServerMsg::Name { hash, .. } => {
                match self.block_get(&hash) {
                    Err(Error::NotFound) => return Err(Error::MissingBlock(hash)),
                    Err(e) => Err(e)?,
                    Ok(block) => return Ok(Some(Eson::from_bytes(block.get_contents())?)),
                }
            },
            ServerMsg::Oob { code: OobCode::NotFound, .. } => return Ok(None),
            x => return Err(oob_or_unexpected(x)),
        }
    }
    pub fn name_put(&mut self, name: &Name, hash: &Multihash, existing_hash: Option<&Multihash>)
        -> Result<(), Error>
    {
        self.stream.write_all(b"edsu name-put\nname ")?;
        self.stream.write_all(name.as_bytes())?;
        self.stream.write_all(b"\nhash ")?;
        self.stream.write_all(hash.as_bytes())?;
        if let Some(eh) = existing_hash {
            self.stream.write_all(b"\nexisting-hash ")?;
            self.stream.write_all(eh.as_bytes())?;
        }
        self.stream.write_all(b"\n\n")?;
        self.stream.flush()?;

        match self.get_msg()? {
            ServerMsg::Ok_ { .. } => return Ok(()),
            x => return Err(oob_or_unexpected(x)),
        }
    }
    pub fn name_del(&mut self, name: &Name, existing_hash: &Multihash) -> Result<(), Error> {
        self.stream.write_all(b"edsu name-put\nname ")?;
        self.stream.write_all(name.as_bytes())?;
        self.stream.write_all(b"\nexisting-hash ")?;
        self.stream.write_all(existing_hash.as_bytes())?;
        self.stream.write_all(b"\n\n")?;
        self.stream.flush()?;

        match self.get_msg()? {
            ServerMsg::Ok_ { .. } => return Ok(()),
            x => return Err(oob_or_unexpected(x)),
        }
    }
}


// BlockStore implementation

pub struct BlockStoreConn<'a> {
    conn: &'a mut Conn,
}

impl<'a> BlockStoreConn<'a> {
    pub fn new(conn: &'a mut Conn) -> Self {
        BlockStoreConn {
            conn,
        }
    }
}

impl<'a> BlockStore for BlockStoreConn<'a> {
    fn block_get(&mut self, hash: &Multihash) -> Result<Block, block_store::Error> {
        Ok(self.conn.block_get(hash)?)
    }
    fn block_put(&mut self, block: &Block) -> Result<Multihash, block_store::Error> {
        Ok(self.conn.block_put(&block)?)
    }
}

impl convert::From<Error> for block_store::Error {
    fn from(e: Error) -> Self {
        match e {
            Error::Ssl(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::SslHandshake(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::Io(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::Edsu(_) => block_store::Error::InvalidContent(format!("{:?}", e)),
            Error::Eson(_) => block_store::Error::InvalidContent(format!("{:?}", e)),
            Error::UnexpectedResponse(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::Oob(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::Unimplemented(_) => block_store::Error::Transport(format!("{:?}", e)),
            Error::AuthenticationError => block_store::Error::Transport(format!("{:?}", e)),
            Error::MissingBlock(_) => block_store::Error::InvalidContent(format!("{:?}", e)),
            Error::NotFound => block_store::Error::NotFound,
            Error::ProtocolVersionMismatch(_) =>
                block_store::Error::Transport(format!("{:?}", e)),
        }
    }
}


fn oob_or_unexpected(msg: ServerMsg) -> Error {
    match msg {    
        ServerMsg::Oob { channel, close_connection, code, encoding, payload,
                         retry_delay_ms } =>
        {
            Error::Oob(ServerMsg::Oob { channel, close_connection, code, encoding, payload,
                                        retry_delay_ms })
        },
        _ => Error::UnexpectedResponse(format!("{:?}", msg))
    }
}
