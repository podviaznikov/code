'use strict';

const MAX_CONTENTS_LEN = 64448;
const MAX_HASHES_PER_MB = 1024;
const DEFAULT_IDLE_TRIGGER_S = 4;
const DEFAULT_CLOBBER_ATTEMPTS = 16;
const DEFAULT_MAX_INITIAL_BUFFER = 16777216;
const DEFAULT_NUM_IN_FLIGHT = 8;


class StorageBasic {
    // opts: idleTriggerS (rounded to nearest integer)
    // cbs: err (required), info
    constructor(username, token, name, opts, cbs) {
        if (!Edsu.validateName(name))
            return { err: Edsu.invalidInput, debug: 'invalid name: ' + name };
        
        // Set up the conn opts
        let connOpts = {
            token,
            idleTriggerS: DEFAULT_IDLE_TRIGGER_S,
        };
        if (opts.idleTriggerS !== undefined) {
            let num = parseFloat(opts.idleTriggerS);
            if (Edsu.isNumeric(num))
                connOpts.idleTriggerS = Math.round(num);
        }

        // Set up the conn cbs
        let connCbs = {
            err: cbs.err,
        };
        if (cbs.info !== undefined)
            connCbs.info = cbs.info;

        this.conn = new EdsuConnRaw(username, connOpts, connCbs);

        // Set up our state
        this.name = name;
        this.lastSeen = {
            keyHashes: {},
            nameBlockHash: null,
            nameBlockEson: null,
        };
        this.subs = {};
        this.cbs = cbs;
        this.namePutState = {
            inFlight: false,
            queue: [],
        };
    }

    // opts: stream, maxConcurrentGets, timeoutMs, noRetry
    // cbs: progress
    async get(key, opts, cbs) {
        opts = opts === undefined ? {} : opts;
        cbs = cbs === undefined ? {} : cbs;

        if (!Edsu.validateKey(key))
            return { err: Edsu.invalidInput, debug: 'invalid key: ' + key };

        let getOpts = { timeoutMs: opts.timeoutMs, noRetry: opts.noRetry };
        let cancelled = false;

        // Grab the latest name block, and chain the first bunch of associated blocks
        let chain = '0$' + key + '>1 1*edsu:blocks';
        let nameBlockOpts = Object.assign({}, opts, { chain });
        let nameBlockGetResp = await this._nameBlockGet(nameBlockOpts);
        if (nameBlockGetResp === null)
            return null;

        let nameBlockEson = nameBlockGetResp.eson;

        if (!nameBlockEson[key])
            return null;

        // Grab the head
        let headEson = await this._esonBlockGet(nameBlockEson[key], getOpts);

        // Report the totals (if requested)
        let contentsSizeEstimate = parseInt(headEson['edsu:contents-size']);
        if (!Edsu.isNumeric(contentsSizeEstimate))
            contentsSizeEstimate = MAX_CONTENTS_LEN;

        if (cbs.progress) {
            let info = {
                type: 'head-downloaded',
            };
            if (Edsu.isNumeric(parseInt(headEson['edsu:contents-size'])))
                info.contentsSize = contentsSizeEstimate;

            let size = parseInt(headEson['edsu:size']);
            if (Edsu.isNumeric(size))
                info.size = size;

            // Call the cb and give them a chance to cancel
            let resp = cbs.progress(info);
            if (resp.cancel) {
                cancelled = true;
                return null;
            }
        }

        // Grab all the blocks (made a bit trickier because there's no promise the blocks
        // will be arrive order
        let getAllBlocks = new Promise((resolve, reject) => {
            let initialSize = Math.min(DEFAULT_MAX_INITIAL_BUFFER, contentsSizeEstimate);
            if (opts.stream)
                initialSize = 0;
            let buf = new _GrowableBuffer(initialSize);
            let bQueue = headEson['edsu:blocks'] || [];
            let mbQueue = headEson['edsu:metablocks'] || [];
            let inFlight = [];
            let downloaded = {};
            let maxInFlight = opts.maxConcurrentGets || DEFAULT_NUM_IN_FLIGHT;

            // Convert to arrays if need be
            if (!Array.isArray(bQueue))
                bQueue = [bQueue];
            if (!Array.isArray(mbQueue))
                bQueue = [mbQueue];

            let advanceQueues = () => {
                // If the queues are empty, we're done
                if (inFlight.length == mbQueue.length == bQueue.length == 0) {
                    resolve(buf.get());
                    return;
                }

                // If there's blocks in the queue, and the in-flight has room, top that up
                while (bQueue.length != 0 && inFlight.length < maxInFlight) {
                    let hash = bQueue.shift();
                    inFlight.push(hash);
                    this.conn.blockGet(hash, getOpts, { ok: blockCb, err: reject });
                }

                // If the bQueue is empty and there's metablocks to be loaded, kick that off
                if (bQueue.length == 0 && mbQueue.length != 0) {
                    let hash = mbQueue.shift();
                    this.conn.blockGet(hash, getOpts, { ok: metablockCb, err: reject });
                }
            };

            // When blocks come in
            let blockCb = resp => {
                if (cancelled)
                    return;
                if (!resp.ok) {
                    reject(resp);
                    return;
                }

                // If it's the next block in the queue, add it to the buffer (or just stream it)
                if (inFlight.length > 0 && inFlight[0] == resp.hash) {
                    inFlight.shift();
                    // Got the next block we need
                    let block = Edsu.decodeBlock(resp.bytes);
                    if (!block.ok) {
                        reject({
                            err: EdsuErr.invalidContent,
                            debug: 'invalid block: ' + resp.hash,
                        });
                        return;
                    }

                    // Add it to our buffer (if it's not streaming instead)
                    if (!opts.stream)
                        buf.extend(block.contents);

                    // Update the client
                    if (cbs.progress) {
                        let resp = cbs.progress({
                            type: 'block-downloaded',
                            block,
                        });
                        if (resp.cancel) {
                            cancelled = true;
                            resolve(buf.get());
                        }
                    }
                } else {
                    // Otherwise, it's arrived out of order - store it for later (not relying on
                    // the cache so we don't have to worry about retries if it gets bumped out)
                    downloaded[resp.hash] = resp.bytes;
                }

                // If we already have the next block in line, recurse
                if (inFlight.length > 0 && downloaded[inFlight[0]]) {
                    let hash = inFlight[0];
                    let bytes = downloaded[hash];
                    delete downloaded[hash];
                    blockCb({
                        ok: true,
                        hash,
                        bytes,
                    });
                }

                advanceQueues();
            };

            // When metablocks come in
            let metablockCb = resp => {
                if (cancelled)
                    return;
                if (!resp.ok) {
                    reject(resp);
                    return;
                }

                // Fill the queues
                // - Do the metablock decodes
                let block = Edsu.decodeBlock(resp.bytes);
                if (!block.ok) {
                    reject({
                        err: EdsuErr.invalidContent,
                        debug: 'invalid metablock: ' + resp.hash,
                    });
                    return;
                }

                let eson = Edsu.decodeEson(block.contents);
                if (!eson.ok) {
                    reject({
                        err: EdsuErr.invalidContent,
                        debug: 'invalid metablock eson: ' + resp.hash,
                    });
                    return;
                }

                let newBlocks = eson['edsu:blocks'];
                let newMBlocks = eson['edsu:metablocks'];
                
                // - Straight add to the block queue
                if (newBlocks)
                    bQueue = bQueue.concat(newBlocks);

                // - Prepend to the metablock queue (to preserve the depth-first ordering)
                if (newMBlocks)
                    mbQueue = newMBlocks + mbQueue;

                // Then advance them
                advanceQueues();
            };

            // Kick it off
            advanceQueues();
        });

        // Resolve the above promise
        try {
            return await getAllBlocks;
        } catch (e) {
            cancelled = true;
            throw e;
        }
    }

    // opts: timeoutMs, noRetry
    async put(key, bytes, opts) {
        opts = opts === undefined ? {} : opts;
        return this._put(key, bytes, opts);
    }

    // opts: maxClobberAttempts, timeoutMs, noRetry
    async clobber(key, bytes, opts) {
        opts = opts === undefined ? {} : opts;
        if (opts.numClobbers === undefined)
            opts.numClobbers = DEFAULT_CLOBBER_ATTEMPTS;
        return this._put(key, bytes, opts);
    }

    async getJson(key) {
        let bytes = await this.get(key);
        if (bytes === null)
            return null;
        return JSON.parse(Edsu.bytesToString(bytes));
    }

    async putJson(key, obj) {
        let bytes = Edsu.stringToBytes(JSON.stringify(obj));
        return this.put(key, bytes);
    }

    async clobberJson(key, obj) {
        let bytes = Edsu.stringToBytes(JSON.stringify(obj));
        return this.clobber(key, bytes, {});
    }

    // opts: [none]
    // cbs: notify
    subPut(key, opts, cbs) {
        if (!Edsu.validateKey(key))
            return { err: Edsu.invalidInput, debug: 'invalid key' };

        if (Object.keys(this.subs).length == 0) {
            this.conn.subPut(this.name, {
                existingHash: this.lastSeen.nameBlockHash,
                chain: '',
            }, {
                err: this.cbs.err,
                notify: notification => {
                    // New name block
                    this._esonBlockGet(notification.hash, {}).then(eson => {
                        this._newNameBlock(notification.hash, eson);
                    }).catch(this.cbs.err);
                },
            });
        }
        
        this.subs[key] = {
            cbs: {
                notify: Edsu._wrapCb(cbs.notify),
            }
        };
    }

    subDel(key) {
        delete this.subs[key];
        if (Object.keys(this.subs).length == 0) {
            this.conn.subsClear();
        }
    }
    
    subsClear() {
        this.subs = {};
        this.conn.subsClear();
    }

    subsList() {
        return Object.keys(this.subs);
    }

    static generateGrantRequest(name, label, explanation) {
        return {
            capabilitiesOwner: {
                'edsu:version': '0.1',
                'edsu:name-get': [name],
                'edsu:name-put': [name + ' destructive'],
            },
            label,
            explanation,
        };
    }

    ////\\\\////\\\\
    // Util

    async _put(key, bytes, opts) {
        if (!Edsu.validateKey(key))
            return { err: Edsu.invalidInput, debug: 'invalid key: ' + key };
        
        let putOpts = { timeoutMs: opts.timeoutMs, noRetry: opts.noRetry };
       
        let putAllBlocks = new Promise((resolve, reject) => {
            let keyHash = null;
            let inFlightHashes = new Set();
            let blockHashes = [];
            let blockSizeTotal = 0;

            // Callback for when a block is in place
            let blockPutCb = (resp) => {
                if (!resp.ok) {
                    reject(resp);
                    return;
                }
                inFlightHashes.delete(resp.hash);

                // If all the blocks are uploaded, including the apex, we're done with this promise
                if (inFlightHashes.size == 0 && keyHash)
                    resolve(keyHash);
            };

            // Put the blocks
            for (let readHead = 0; readHead < bytes.length; readHead += MAX_CONTENTS_LEN) {
                let slice = bytes.slice(readHead, readHead+MAX_CONTENTS_LEN);
                let blockBytes = Edsu.encodeBinaryBlock(slice, []).bytes;
                let blockHash = Edsu.multihash(blockBytes);
                inFlightHashes.add(blockHash);
                blockHashes.push(blockHash);
                blockSizeTotal += blockBytes.length;
                this.conn.blockPut(blockBytes, putOpts, { ok: blockPutCb, err: blockPutCb });
            }

            // Put the metablocks
            // - Linked list format: so reverse order of metablocks, original order for blocks
            let prevMbHash = null;
            for (let idx = Math.floor(blockHashes.length/MAX_HASHES_PER_MB) * MAX_HASHES_PER_MB;
                 idx >= 0;
                 idx -= MAX_HASHES_PER_MB)
            {
                // Build the ESON
                let eson = {
                    'edsu:blocks': blockHashes.slice(idx, idx+MAX_HASHES_PER_MB),
                };
                // - Link to the previous node
                if (prevMbHash)
                    eson['edsu:metablocks'] = prevMbHash;
                // - If this is the head, add some metadata
                if (idx == 0) {
                    eson['edsu:contents-size'] = String(bytes.length);
                    eson['edsu:size'] = String(blockSizeTotal);
                }

                // Put the block
                let blockBytes = Edsu.encodeEsonToBlockBytes(eson).bytes;
                let blockHash = Edsu.multihash(blockBytes);
                inFlightHashes.add(blockHash);
                this.conn.blockPut(blockBytes, putOpts, { ok: blockPutCb, err: blockPutCb });

                // If this is the head, this is what the name block key will point to
                if (idx == 0)
                    keyHash = blockHash;
            }
        });

        // Resolve the above promise, then update the key in the name block
        let hash = await putAllBlocks;

        // We don't want to have parallel name block puts, so put it behind a queue lock
        await this._getNamePutLock();
        try {
            return this._nameBlockKeyPut(key, hash, opts);
        } finally {
            this._clearNamePutLock();
        }
    }

    _getNamePutLock() {
        return new Promise(resolve => {
            if (!this.namePutState.inFlight)
                resolve();
            else
                this.namePutState.queue.push(resolve);
        });
    }

    _clearNamePutLock() {
        let next = this.namePutState.queue.shift();
        if (next)
            next();
        else
            this.namePutState.inFlight = false;
    }

    async _nameBlockGet(opts) {
        let getOpts = { timeoutMs: opts.timeoutMs, noRetry: opts.noRetry, chain: opts.chain };

        // Grab the hash
        let nameBlockHash = await new Promise((resolve, reject) => {
            this.conn.nameGet(this.name, getOpts, {
                ok: resp => resolve(resp.hash),
                err: resp => {
                    if (resp.err == EdsuErr.notFound)
                        resolve(null);
                    else
                        reject(resp);
                },
            });
        });

        if (!nameBlockHash)
            return null;

        // Grab the block
        let nameBlockEson = await this._esonBlockGet(nameBlockHash, getOpts);
        
        // Cache etc.
        this._newNameBlock(nameBlockHash, nameBlockEson);

        return { hash: nameBlockHash, eson: nameBlockEson };
    }
    
    _newNameBlock(nameBlockHash, nameBlockEson, supressNotifications) {
        // Alert any subs
        if (!supressNotifications) {
            for (let key of Object.keys(this.subs)) {
                if ((this.lastSeen.nameBlockEson === null && nameBlockEson !== null) ||
                    (this.lastSeen.nameBlockEson !== null && nameBlockEson === null) ||
                    (this.lastSeen.nameBlockEson[key] != nameBlockEson[key]))
                {
                    this.subs[key].cbs.notify({
                        key,
                    });
                }
            }
        }

        // Cache it
        this.lastSeen.nameBlockHash = nameBlockHash;
        this.lastSeen.nameBlockEson = nameBlockEson;
    }

    _esonBlockGet(hash, getOpts) {
        return new Promise((resolve, reject) => {
            this.conn.blockGet(hash, getOpts, {
                ok: resp => {
                    let block = Edsu.decodeBlock(resp.bytes);
                    if (!block.ok) {
                        reject({
                            err: EdsuErr.invalidContent,
                            debug: 'invalid block',
                        });
                        return;
                    }
                    let eson = Edsu.decodeEson(block.contents);
                    if (!eson.ok) {
                        reject({
                            err: EdsuErr.invalidContent,
                            debug: 'invalid block ESON',
                        });
                        return;
                    }
                    resolve(eson.eson);
                },
                err: reject,
            });
        });
    }

    async _nameBlockKeyPut(key, hash, opts) {
        let attemptsLeft = Edsu.isNumeric(opts.numClobbers) ? opts.numClobbers : 1;
        let putOpts = { timeoutMs: opts.timeoutMs, noRetry: opts.noRetry };

        for (;;) {
            // Put the new name block
            let nameBlockEson = Object.assign({}, this.lastSeen.nameBlockEson || undefined);
            nameBlockEson[key] = hash;
            let nameBlockBytes = Edsu.encodeEsonToBlockBytes(nameBlockEson).bytes;
            let nameBlockHash = await new Promise((resolve, reject) => {
                this.conn.blockPut(nameBlockBytes, putOpts, {
                    ok: resp => resolve(resp.hash),
                    err: reject,
                });
            });
     
            // Set the name
            let resp = await new Promise((resolve, reject) => {
                let opts = Object.assign({}, putOpts);
                if (this.lastSeen.nameBlockHash)
                    opts.existingHash = this.lastSeen.nameBlockHash;
                this.conn.namePut(this.name, nameBlockHash, opts, {
                    ok: resolve,
                    err: async resp => {
                        if (resp.err == EdsuErr.hashMismatch) {
                            // Something changed since we last looked - grab the new name block
                            // - Record whether the key we're interested in has changed for later
                            let lastKeyHash = this.lastSeen.nameBlockEson[key];
                            await this._nameBlockGet(opts);
                            if (lastKeyHash == this.lastSeen.nameBlockEson[key])
                                resp.keyUnchanged = true;
                            resolve(resp);
                        } else {
                            reject(resp);
                        }
                    },
                });
            });
            if (resp.ok) {
                // Done, adjust our "last seen" values - don't use this._newNameBlock so as 
                this._newNameBlock(nameBlockHash, nameBlockEson, true);
                return { ok: true };
            } else if (resp.keyUnchanged && attemptsLeft >= 0) {
                // Special case: if the name's changed, but our key hasn't, try once more
                continue;
            } else if (--attemptsLeft < 1) {
                throw resp;
            }
        }
    }
}


class _GrowableBuffer {
    constructor(initialSize) {
        this.writeHead = 0;
        this.buf = new Uint8Array(initialSize);
    }
    get() {
        return this.buf.slice(0, this.writeHead);
    }
    extend(bytes) {
        // If it won't fit, allocate and copy
        let newWriteHead = this.writeHead + bytes.length;
        if (newWriteHead > this.buf.length) {
            let newSize = this.buf.length * 2;
            while (newSize < newWriteHead)
                newSize *= 2;
            let bufNew = new Uint8Array(newSize);
            bufNew.set(this.buf);
            this.buf = bufNew;
        }

        // Copy bytes into the (now) sufficiently large buffer
        this.buf.set(bytes, this.writeHead);
        this.writeHead = newWriteHead;
    }
}
