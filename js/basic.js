'use strict';

class EdsuConn {
    // opts: secret, token, visitorUsername, idleTriggerS, opTimeoutMs
    // cbs: err, info
    constructor(username, opts, cbs) {
        this.conn = new EdsuConnRaw(username, opts, cbs);
        this.opts = opts;
    }

    async blockGet(hash, chain=null) {
        return new Promise((resolve, reject) => {
            this.conn.blockGet(hash, {
                chain,
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }

    async blockPut(bytes) {
        return new Promise((resolve, reject) => {
            this.conn.blockPut(bytes, {
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }

    async namePut(name, hash, existingHash=null) {
        return new Promise((resolve, reject) => {
            this.conn.namePut(name, hash, {
                existingHash,
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }

    async nameGet(name, chain=null) {
        return new Promise((resolve, reject) => {
            this.conn.nameGet(name, {
                chain,
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }

    async nameDel(name, existingHash=null) {
        if (!existingHash)
            existingHash = (await this.nameGet(name)).hash;
        return new Promise((resolve, reject) => {
            this.conn.nameDel(name, existingHash, {
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }

    async nameAppend(name, hash) {
        return new Promise((resolve, reject) => {
            this.conn.nameAppend(name, hash, {
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }

    // Straight passthroughs
    subPut(name, opts, cbs) { this.conn.subPut(name, opts, cbs) }
    subDel(name) { this.conn.subDel(name) }
    subsClear() { this.conn.subsClear() }
    close() { this.conn.close() }
    isClosed() { this.conn.isClosed() }
    setCbs() { this.conn.setCbs() }
    setOpts() { this.conn.setOpts() }

    /*
    async nameClobber(name) {
        for (;;) {
            let existingHash = (await this.nameGet(name)).hash;
        }


        if (!existingHash)
        return new Promise((resolve, reject) => {
            this.conn.nameDel(name, existingHash, {
                timeoutMs: this.opts.opTimeoutMs,
            }, {
                ok: resolve,
                err: reject
            });
        });
    }
    */

    async namedEsonGet(name) {
        let hash = (await this.nameGet(name, '')).hash;
        let bytes = (await this.blockGet(hash)).bytes;
        let blockMaybe = Edsu.decodeBlock(bytes);
        if (!blockMaybe.ok) {
            throw blockMaybe;
        }
        let esonMaybe = Edsu.decodeEson(blockMaybe.contents);
        if (esonMaybe.ok) {
            return esonMaybe.eson;
        } else {
            throw esonMaybe;
        }
    }

    async esonPut(eson, salt=null) {
        let blockBytes = Edsu.encodeEsonToBlockBytes(eson, salt === null ? {} : { salt });
        if (!blockBytes.ok)
            throw blockBytes;
        return this.blockPut(blockBytes.bytes);
    }

    async namedEsonPut(name, eson, existingHash=null, salt=null) {
        let hash = (await this.esonPut(eson, salt)).hash;
        return this.namePut(name, hash, existingHash);
    }

    async srvCall(name, args) {
        let hash = (await this.esonPut(args)).hash;
        await this.namePut(name, hash);
        let resultName = name + '.' + hash;
        return this.namedEsonGet(resultName);
    }

    async namedBlockClobber(name, bytes, existingHash=null) {}
    async namedEsonClobber(name, eson, existingHash=null) {}
    async namedBlockPut(name, bytes, existingHash=null) {}
    async metaBlockPut(bytes) {}
    async metaBlockGet(hash) {}
    async namedMetaBlockGet(name) {}
    
}
