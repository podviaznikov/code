
const EdsuErr = {
    notFound: Symbol('not found'),
    permissionDenied: Symbol('permission denied'),
    invalidInput: Symbol('invalid input'),
    invalidContent: Symbol('invalid content'),
    server: Symbol('server error'),
    connection: Symbol('connection error'),
    hashMismatch: Symbol('hash mismatch'),
    versionMismatch: Symbol('version mismatch'),
    timedOut: Symbol('timed out'),
    rateLimited: Symbol('rate limited'),
    authentication: Symbol('authentication error'),
    unexpected: Symbol('unexpected server response'),
};

const EdsuInfo = {
    closed: Symbol('closed'),
    authorized: Symbol('authorized'),
}

let EdsuLog = () => {}; // Set EdsuLog to console.log in your code for debugging


////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\
// EdsuConnRaw
////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\

// opts: secret, token, visitorUsername, idleTriggerS
// cbs: err, info
function EdsuConnRaw(username, opts, cbs) {
    this.setOpts(opts);
    this.setCbs(cbs);
    
    if (this._resetState === undefined) {
        this.cbs.err({
            err: EdsuErr.invalidInput,
            debug: 'EdsuConnRaw must be constructed with "new"',
            reconnecting: false,
        });
        return;
    }

    // Only enough validation to form a valid hello message
    if (!this._validateArgs('constructor', (e) => {
            Object.assign(e, { reconnecting: false });
            this.cbs.err(e);
        }, [
        {
            fn: Edsu.validateUsername,
            name: 'username',
            val: username,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateValue(x) : true,
            name: 'secret',
            val: opts.secret,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateValue(x) : true,
            name: 'token',
            val: opts.token,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateUsername(x) : true,
            name: 'visitorUsername',
            val: opts.visitorUsername,
        }, {
            fn: x => (x !== undefined && x !== null) ? Number.isInteger(x) : true,
            name: 'idleTriggerS',
            val: opts.idleTriggerS,
        }
    ])) return;

    this.domain = username.replace(/@/, '.edsu.');
    this.userTo = username;

    // Make sure there's a token if there's a visitor username, or neither if there's a secret
    if (opts.visitorUsername && !opts.token) {
        this.cbs.err({
            err: EdsuErr.invalidInput,
            debug: 'visitor username without matching token',
            reconnecting: false,
        });
        return;
    }
    if (opts.token && opts.secret) {
        this.cbs.err({
            err: EdsuErr.invalidInput,
            debug: 'secret and token are mutually exclusive',
            reconnecting: false,
        });
        return;
    }

    this._resetState();
    this._connect(false);

    // So we don't throw an error if the page is being closed in FF
    // Unload event occurs after the WS is closed, so we have to
    // note the beforeunload (which might not end up in a full unload)
    // and use it in the error handling logic
    window.addEventListener('beforeunload', () => {
        this.eventBeforeUnloadReceived = Date.now();
    });
}


EdsuConnRaw.prototype = {
    NUM_CONNECT_TRIES: 2,
    NUM_SYNC_TRIES: 2,
    ODD_RECONNECT_INTERVAL_MS: 2048,
    ODD_RECONNECT_TIMES: 3,
    DEFAULT_TIMEOUT_MS: 8192,
    DEFAULT_IDLE_TRIGGER_S: 256,
    STAGE_CONNECTING: Symbol('connecting'),
    STAGE_CLOSED: Symbol('closed'),
    STAGE_CONNECTED: Symbol('connected'),
    STAGE_IDLING: Symbol('idling'),
    TAB_CLOSE_GRACE_MS: 512,
};


// opts: idleTriggerS
EdsuConnRaw.prototype.setOpts = function(opts) {
    this.opts = Object.assign({}, opts);
}

// cbs: err, info
EdsuConnRaw.prototype.setCbs = function(cbs) {
    this.cbs = {};
    for (let cbName of ['err', 'info']) {
        this.cbs[cbName] = Edsu._wrapCb(cbs[cbName]);
    }
}

// _resetState
EdsuConnRaw.prototype._resetState = function() {
    let blank = {
        stage: this.STAGE_CONNECTING,
        ws: null,
        connectTryNum: 0,
        timer: null,
        timedFns: [],
        curChannel: 1,
        channelStates: {},
        cacheWatchers: {},
        lastNonidle: Date.now(),
        eventBeforeUnloadReceived: 0,
        recvState: {
            header: null,
            unconsumedBytes: new Uint8Array(0),
        },
        oddReconnectState: {
            times: 0,
            last: 0,
        },
        subsState: {
            localSubs: {},
            remoteSubs: {},
            syncing: false,
        }
    };
    Object.assign(this, blank);
};

// _connect
EdsuConnRaw.prototype._connect = function(oddReconnect) {
    this.stage = this.STAGE_CONNECTING;

    if (oddReconnect) {
        if (Date.now() - this.oddReconnectState.last < this.ODD_RECONNECT_INTERVAL_MS) {
            if (++this.oddReconnectState.times >= this.ODD_RECONNECT_TIMES) {
                this.cbs.err({
                    err: EdsuErr.connection,
                    debug: 'had to reconnect too many times too quickly',
                    reconnecting: false,
                });
                this.close();
                return;
            }
        } else {
            this.oddReconnectState.times = 1;
        }
        this.oddReconnectState.last = Date.now();
    }

    if (this.ws)
        this.ws.close();

    // Error out any non-retryable ops
    this.channelStates = Object.keys(this.channelStates).reduce((a, channel) => {
        let state = this.channelStates[channel];
        if (state.noRetry && state.sent) {
            state.errCb({
                err: EdsuErr.connection,
                debug: 'reconnecting, but op not retryable',
            });
        } else {
            a[channel] = state;
        }
        return a;
    }, {});

    // Initiate the WS connection
    let ws = new WebSocket('wss://' + this.domain + '/edsu/ws');
    ws.binaryType = 'arraybuffer';

    // Callbacks
    ws.onerror = e => this._handleEvent(e);
    ws.onclose = e => this._handleEvent(e);
    ws.onopen = e => this._handleEvent(e);
    ws.onmessage = e => this._handleEvent(e);

    this.ws = ws;
    this.connectTryNum++;
}


// close
EdsuConnRaw.prototype.close = function() {
    if (this.stage == this.STAGE_CLOSED)
        return;
    this.ws.close();
    if (this.cbs.info)
        this.cbs.info({ok: true, info: EdsuInfo.closed});
    this._resetState();
    this.stage = this.STAGE_CLOSED;
};


// isClosed
EdsuConnRaw.prototype.isClosed = function() {
    return this.stage == this.STAGE_CLOSED;
}


// _handleEvent
EdsuConnRaw.prototype._handleEvent = function(e) {
    if (this.stage == this.STAGE_CLOSED)
        return;

    // WebSocket Events
    if (e.target == this.ws) {
        switch (e.type) {
            case 'close': {
                if (this.stage == this.STAGE_IDLING)
                    return;
                if (Date.now() - this.eventBeforeUnloadReceived < this.TAB_CLOSE_GRACE_MS)
                    return;
            }; // NOTE: falls through!
            case 'error': {
                // All our subs are gone
                this.subsState.remoteSubs = {};
                
                // Reconnect or close, depending
                if (this.stage == this.STAGE_CLOSED)
                    return;
                if (this.stage == this.STAGE_CONNECTED) {
                    EdsuLog('error on WebSocket connection', e);
                    this.cbs.err({
                        err: EdsuErr.connection,
                        debug: 'error on WebSocket connection',
                        details: { event: e },
                        reconnecting: true,
                    });
                }
                // If we're out of reconnect tries, error out
                if (this.connectTryNum >= this.NUM_CONNECT_TRIES) {
                    this.cbs.err({
                        err: EdsuErr.connection,
                        debug: 'could not connect to Edsu server',
                        details: { event: e },
                        reconnecting: false,
                    });
                    this.close();
                } else {
                    // Otherwise, try to reconnect 
                    this._connect(this.stage != this.STAGE_CONNECTING);
                }
            }; break;
            case 'open': {
                // Set state to reflect that we're connected
                this.stage = this.STAGE_CONNECTED;
                this.connectTryNum = 0;

                // Say hello
                let eson = {
                    'versions': Edsu.PROTOCOL_VERSION,
                };
                if (this.opts.secret) {
                    eson['secret'] = Edsu.encodeEsonUtf8(this.opts.secret.trim()).padEnd(64);
                } else if (this.opts.visitorUsername) {
                    // Prepare the token
                    let tokenHead = this.opts.token.slice(0, 8);
                    let tokenTail = Edsu.stringToBytes(this.opts.token.slice(8));
                    let userB58 = Edsu.stringToBytes(this.userTo);
                    let hmac = Edsu.hmacSha256(tokenTail, userB58);
                    let token = tokenHead + Edsu.encodeB58(hmac);

                    // Put into the call
                    eson['token'] = token;
                    eson['visitor-username'] = this.opts.visitorUsername;
                } else if (this.opts.token) {
                    eson['token'] = this.opts.token;
                }
                let esonMaybe = Edsu.encodeEson(eson);
                if (!esonMaybe.ok) {
                    this.cbs.err({
                        err: EdsuErr.invalidInput,
                        debug: 'invalid hello variable: (e.g. username, visitorUsername, token)',
                        reconnecting: false,
                    });
                    this.close();
                    return;
                }
                this._sendAll([
                    Edsu.stringToBytes('edsu hello\n'),
                    esonMaybe.bytes,
                ]);

                // (Re)try any functions that were in flight while we were disconnected
                Object.keys(this.channelStates).forEach(channel => {
                    let state = this.channelStates[channel];
                    if (!state.noRetry || !state.sent) {
                        // But hold off on some of them if we're going to auth
                        if ((!this.opts.token && !this.opts.secret) || state.noAuthNeeded)
                            state.op();
                    }
                });
                
                // Sync the subs
                this._subsSync();
            }; break;
            case 'message': {
                this._handleNewBytes(new Uint8Array(e.data));
            }; break;
            default:
                EdsuLog('unknown WebSocket event', e);
        }
    }

    // Otherwise it's a timer event (though this code runs on all events)
    let now = Date.now();

    // Expired fns
    let channelsToNix = [];
    Object.keys(this.channelStates).forEach(channel => {
        let state = this.channelStates[channel];
        if (now > state.expires) {
            state.errCb({
                err: EdsuErr.timedOut,
                debug: 'timed out',
            });
            channelsToNix.push(channel);
        }
    });
    channelsToNix.forEach(x => delete this.channelStates[x]);

    // Timed fns
    for (let i = 0; i < this.timedFns.length; i++) {
        let when = this.timedFns[i][0];
        let fn = this.timedFns[i][1];
        if (when - now < 10) {
            fn();
        } else {
            this.timedFns = this.timedFns.slice(i);
            break;
        }
    }

    // Detect idling
    if (this.stage == this.STAGE_CONNECTED &&
        Object.keys(this.channelStates).length == 0 &&
        Object.keys(this.subsState.localSubs).length == 0)
    {
        let idleTriggerS = this.opts.idleTriggerS || this.DEFAULT_IDLE_TRIGGER_S;
        if (now - this.lastNonidle > idleTriggerS * 1000) {
            this.stage = this.STAGE_IDLING;
            this.ws.close();
            this.subsState.remoteSubs = {};
            EdsuLog('idling');
        }
    } else {
        this.lastNonidle = now;
    }

    // Set the timer for the next time we need to be awake
    this._setTimer();
};


// _setTimer
EdsuConnRaw.prototype._setTimer = function() {
    // Clear any existing
    if (this.timer !== null)
        clearTimeout(this.timer);

    // Find the event nearest to us
    let now = Date.now();
    let when = now + this.DEFAULT_TIMEOUT_MS;

  
    // Expiring functions
    Object.values(this.channelStates).forEach(state => {
        when = Math.min(when, state.expires);
    });

    // Timed functions
    if (this.timedFns.length != 0)
        when = Math.min(when, this.timedFns[0][0]);

    let duration = Math.max(0, when - now);
    this.timer = setTimeout(() => this._handleEvent({ timer: true }), duration);
};


// _addTimedFn
EdsuConnRaw.prototype._addTimedFn = function(when, fn) {
    this.timedFns.push([when, fn]);
    this.timedFns.sort((l, r) => l[0]-r[0]);
    this._setTimer();
};


// _handleNewBytes
EdsuConnRaw.prototype._handleNewBytes = function(newBytes) {
    // Prepend our already-received data, if we have some
    let bytes;
    if (this.recvState.unconsumedBytes.length != 0) {
        bytes = new Uint8Array(newBytes.length + this.recvState.unconsumedBytes.length);
        bytes.set(this.recvState.unconsumedBytes);
        bytes.set(newBytes, this.recvState.unconsumedBytes.length);
        this.recvState.unconsumedBytes = new Uint8Array(0);
    } else {
        bytes = newBytes;
    }

    // We might be waiting for a header, or have one already (and be waiting for a payload)
    let header;
    if (this.recvState.header === null) {
        // Parse the header from the bytes
        let maybe_eson = Edsu.decodeEsonStream(bytes);
        if (maybe_eson.err || (maybe_eson.eson && !maybe_eson.eson.edsu)) {
            this.cbs.err({
                err: EdsuErr.server,
                debug: 'server sent invalid message header',
                reconnecting: true,
            });
            this._connect(true);
            return;
        }

        if (maybe_eson.eson === null) {
            // Need more bytes to complete header
            this.recvState.unconsumedBytes = maybe_eson.unconsumedBytes;
            return;
        } else {
            // Have the header
            bytes = maybe_eson.unconsumedBytes;
            header = maybe_eson.eson;
        }
    } else {
        header = this.recvState.header;
        this.recvState.header = null;
    }

    // Process the message (if we can)
    if (header.hasOwnProperty('payload-length')) {
        // Needs a payload - first, parse and validate the val
        let payloadLen = parseInt(header['payload-length']);
        if (!Edsu.isNumeric(payloadLen) || payloadLen > Edsu.MAX_PAYLOAD_LEN) {
            this.cbs.err({
                err: EdsuErr.server,
                debug: 'server sent invalid message header: payload-length',
                reconnecting: true,
            });
            this._connect(true);
            return;
        }

        if (bytes.length >= payloadLen) {
            // Enough bytes for payload - handle msg
            let payload = bytes.subarray(0, payloadLen);
            this._handleEdsuMsg(header, payload);

            // Keep going if there's more than enough bytes
            if (bytes.length > payloadLen) {
                // Payloads are terminated by newlines, skip that
                this._handleNewBytes(bytes.subarray(payloadLen+1));
            }
        } else {
            // Not enough bytes for payload
            this.recvState.header = header;
            this.recvState.unconsumedBytes = bytes;
        }
    } else {
        // If there's no payload, then we can process the message
        this._handleEdsuMsg(header, null);
        // Recurse if there's more data
        if (bytes.length != 0)
            this._handleNewBytes(bytes);
    }
};


EdsuConnRaw.prototype._handleEdsuMsg = function(msg, payload) {
    let channel = parseInt(msg.channel);
    if (!Edsu.isNumeric(channel)) {
        this.cbs.err({
            err: EdsuErr.server,
            debug: 'server sent invalid message header: missing channel',
            reconnecting: true,
        });
        this._connect(true);
        return;
    } else if (channel === 0) {
        this._handleConnMsg(msg, payload);
    } else {
        let channelState = this.channelStates[channel];
        if (channelState === undefined) {
            // May be a chained block for a channel that's got its '0' block and therefore
            // been removed
            if (msg.edsu == 'block') {
                this._blockReceive(msg.hash, payload);
            } else if (msg.edsu == 'sub-notify') {
                this._nameReceive(msg.name, msg.hash);
            } else if (msg.code == 'just-sent') {
                // NOP: common scenario we don't need to log
            } else {
                EdsuLog('message for disappeared channel:', JSON.stringify(msg));
            }
        } else {
            channelState.msgCb.call(this, msg, payload);
        }
    }
};


////\\\\////\\\\
// Ops

// blockPut

// opts: timeoutMs, noRetry
// cbs: ok, err
EdsuConnRaw.prototype.blockPut = function(bytes, opts, cbs) {
    // Validate
    if (!this._validateArgs('blockPut', Edsu._wrapCb(cbs.err), [
        {
            fn: x => x instanceof Uint8Array,
            name: 'bytes',
            val: bytes,
        }, {
            fn: x => bytes.length < Edsu.MAX_PAYLOAD_LEN,
            name: 'payload length',
            val: bytes,
        },
    ])) return;
        
    // Set up channel state
    let state = this._initOpState('blockPut', opts, cbs);
    let op = () => {
        this._sendMsg(state, 'block-put', {
            'payload-stop': String(bytes.length),
        }, bytes);
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'ok': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                    hash: msg.hash,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on blockPut channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}



// blockGet

// opts: chain, timeoutMs, noRetry
// cbs: ok, err
EdsuConnRaw.prototype.blockGet = function(hash, opts, cbs) {
    // Validate
    if (!this._validateArgs('blockGet', Edsu._wrapCb(cbs.err), [
        {
            fn: Edsu.validateMultihash,
            name: 'hash',
            val: hash,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateChain(x) : true,
            name: 'chain',
            val: opts.chain,
        },
    ])) return;

    // Check the cache first
    let maybeBytes = EdsuCache.get(hash);
    if (maybeBytes !== null) {
        Edsu._wrapCb(cbs.ok)({
            ok: true,
            bytes: maybeBytes,
            hash, 
        });
        return;
    }
        
    // Set up channel state
    let state = this._initOpState('blockGet', opts, cbs);
    state.noAuthNeeded = true;
    let op = () => {
        this._sendMsg(state, 'block-get', {
            hash,
            chain: (opts.chain !== undefined) ? opts.chain : null,
        });
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'block': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                    bytes: payload,
                    hash,
                });
                delete this.channelStates[state.channel];
                this._blockReceive(hash, payload);
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on blockGet channel', JSON.stringify(msg));
        }
    };
    let cacheHitCb = () => {
        let bytes = EdsuCache.get(hash);
        if (bytes !== null) {
            Edsu._wrapCb(cbs.ok)({
                ok: true,
                bytes,
                hash,
            });
            delete this.channelStates[state.channel];
        }
    };
    Object.assign(state, { msgCb, op, cacheHitCb });

    // Store state and kick off
    this._initiateChannelState(state);

    // Also, watch for this hash coming in on any other channel
    let otherWatchers = this.cacheWatchers[hash];
    if (otherWatchers !== undefined)
        otherWatchers.push(state.channel);
    else
        this.cacheWatchers[hash] = [state.channel];
}


// namePut

// opts: existingHash, timeoutMs, noRetry
// cbs: ok, err
EdsuConnRaw.prototype.namePut = function(name, hash, opts, cbs) {
    // Validate
    if (!this._validateArgs('namePut', Edsu._wrapCb(cbs.err), [
        {
            fn: Edsu.validateName,
            name: 'name',
            val: name,
        }, {
            fn: Edsu.validateMultihash,
            name: 'hash',
            val: hash,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateMultihash(x) : true,
            name: 'existingHash',
            val: opts.existingHash,
        },
    ])) return;
        
    // Set up channel state
    let state = this._initOpState('namePut', opts, cbs);
    let op = () => {
        this._sendMsg(state, 'name-put', {
            name,
            hash,
            'existing-hash': opts.existingHash || null,
        });
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'ok': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on namePut channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}


// nameDel

// opts: timeoutMs, noRetry
// cbs: ok, err
EdsuConnRaw.prototype.nameDel = function(name, existingHash, opts, cbs) {
    // Validate
    if (!this._validateArgs('namePut', Edsu._wrapCb(cbs.err), [
        {
            fn: Edsu.validateName,
            name: 'name',
            val: name,
        }, {
            fn: Edsu.validateMultihash,
            name: 'existingHash',
            val: existingHash,
        },
    ])) return;
        
    // Set up channel state
    let state = this._initOpState('namePut', opts, cbs);
    let op = () => {
        this._sendMsg(state, 'name-put', {
            name,
            'existing-hash': existingHash,
        });
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'ok': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on namePut channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}


// nameGet

// opts: chain, timeoutMs, noRetry
// cbs: ok, err
EdsuConnRaw.prototype.nameGet = function(name, opts, cbs) {
    // Validate
    if (!this._validateArgs('nameGet', Edsu._wrapCb(cbs.err), [
        {
            fn: Edsu.validateName,
            name: 'name',
            val: name,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateChain(x) : true,
            name: 'chain',
            val: opts.chain,
        },
    ])) return;

    // Set up channel state
    let state = this._initOpState('nameGet', opts, cbs);
    if (name.startsWith('pub.'))
        state.noAuthNeeded = true;
    let op = () => {
        this._sendMsg(state, 'name-get', {
            name,
            chain: (opts.chain !== undefined) ? opts.chain : null,
        });
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'name': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                    hash: msg.hash,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on nameGet channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}


// nameAppend

// opts: timeoutMs, retry
// cbs: ok, err
EdsuConnRaw.prototype.nameAppend = function(name, hash, opts, cbs) {
    // This op doesn't do retries unless asked to
    opts = Object.assign({}, opts, { noRetry: !opts.retry });

    // Validate
    if (!this._validateArgs('nameAppend', Edsu._wrapCb(cbs.err), [
        {
            fn: Edsu.validateName,
            name: 'name',
            val: name,
        }, {
            fn: Edsu.validateMultihash,
            name: 'hash',
            val: hash,
        },
    ])) return;
        
    // Set up channel state
    let state = this._initOpState('nameAppend', opts, cbs);
    let op = () => {
        this._sendMsg(state, 'name-append', {
            name,
            hash,
        });
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'ok': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on nameAppend channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}


// ping

// opts: timeoutMs, noRetry
// cbs: ok, err
EdsuConnRaw.prototype.ping = function(opts, cbs) {
    // Set up channel state
    let state = this._initOpState('ping', opts, cbs);
    state.noAuthNeeded = true;
    let op = () => {
        this._sendMsg(state, 'ping', {});
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'pong': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on ping channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}


// subPut
// opts: existingHash, once, durationS, chain
// cbs: notify, err
EdsuConnRaw.prototype.subPut = function(name, opts, cbs) {
    let hash = opts.existingHash || null;
    let once = Boolean(opts.once);
    let expires = (opts.durationS) ? Math.ceil(Date.now()/1000) + opts.durationS : null;
    let chain = opts.chain || null;

    // Validate
    if (!this._validateArgs('subPut', Edsu._wrapCb(cbs.err), [
        {
            fn: Edsu.validateName,
            name: 'name',
            val: name,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateMultihash(x) : true,
            name: 'hash',
            val: hash,
        }, {
            fn: x => (x !== undefined && x !== null) ? Edsu.validateChain(x) : true,
            name: 'chain',
            val: chain,
        },
    ])) return;

    this.subsState.localSubs[name] = {
        hash,
        once,
        expires,
        chain,
        cbs,
    }
    this._subsSync();
};


// subDel
EdsuConnRaw.prototype.subDel = function(name) {
    delete this.subsState.localSubs[name];
    this._subsSync();
};

// subsClear
EdsuConnRaw.prototype.subsClear = function() {
    this.subsState.localSubs = {};
    this._subsSync();
};




////\\\\////\\\\
// Util


EdsuConnRaw.prototype._subsSync = function() {
    // Only one sync at a time
    if (this.subsState.syncing)
        return;

    // Convenience
    let local = this.subsState.localSubs;
    let remote = this.subsState.remoteSubs;

    // There's no way to remove just one sub, so clear and start over if there's a del
    let needsClear = !Object.keys(remote).every(name => local.hasOwnProperty(name));
    if (needsClear) {
        let proceedAfterClear = () => {
            this.subsState.remoteSubs = {};
            this._subsSyncPut();
        };
        // Oddly enough, we can keep on keeping on if this fails - we just ignore
        // the extra notifications - so essentially NOP on err
        this.subsState.syncing = true;
        this._internalSubClear({ ok: proceedAfterClear, err: proceedAfterClear });
        return;
    } else {
        this._subsSyncPut();
    }
};


EdsuConnRaw.prototype._subsSyncPut = function() {
    // Convenience
    let local = this.subsState.localSubs;
    let remote = this.subsState.remoteSubs;

    // Compare the local and remote sides
    let subsToPut = {};
    let needHashes = {};

    Object.keys(local).forEach(name => {
        ls = local[name];
        rs = remote[name];
        if (rs === undefined ||
            ls.once != rs.once ||
            ls.expires != rs.expires ||
            ls.chain != rs.chain)
        {
            subsToPut[name] = ls;
        } else if (ls.hash === null ||
                   ls.hash != rs.hash)
        {
            needHashes[name] = ls;
        }
    });

    // We're done here if everything's synced
    if (Object.keys(subsToPut).length == 0 && Object.keys(needHashes).length == 0) {
        this.subsState.syncing = false;
        return;
    }

    // Otherwise, sync with the server
    this.subsState.syncing = true;

    let checkIfDone = () => {
        if (Object.keys(subsToPut).length == 0 && Object.keys(needHashes).length == 0) {
            this.subsState.syncing = false;
        }
    };

    let err = e => {
        this.subsState.syncing = false;
        sub.cbs.err(e);
    };

    // Sync hashes and put subs
    Object.keys(needHashes).forEach(name => {
        let sub = needHashes[name];
        this.nameGet(name, { chain: sub.chain }, {
            ok: () => {
                delete needHashes[name];
                checkIfDone();
            },
            err,
        });
    });

    Object.keys(subsToPut).forEach(name => {
        let sub = subsToPut[name];
        this.subsState.remoteSubs[name] = {
            hash: null,
            once: sub.once,
            expires: sub.expires,
            chain: sub.chain,
        }
        this._internalSubPut(name, sub.existingHash, sub.once, sub.expires, sub.chain, {
            ok: () => {
                delete subsToPut[name];
                checkIfDone();
            },
            err,
        });
    });
};


EdsuConnRaw.prototype._internalSubClear = function(cbs) {
    // Set up channel state
    let state = this._initOpState('_subClear', {}, cbs);
    let op = () => {
        this._sendMsg(state, 'sub-clear', {});
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'ok': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on _subClear channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}


EdsuConnRaw.prototype._internalSubPut = function(name, existingHash, once, expires, chain, cbs) {
    // Set up channel state
    let state = this._initOpState('_subPut', {}, cbs);
    let op = () => {
        this._sendMsg(state, 'sub-put', {
            name,
            'existing-hash': existingHash,
            once: once ? String(Boolean(once)) : undefined,
            expires: expires ? String(expires) : undefined,
            chain,
        });
    };
    let msgCb = (msg, payload) => {
        switch (msg.edsu) {
            case 'ok': {
                Edsu._wrapCb(cbs.ok)({
                    ok: true,
                });
                delete this.channelStates[state.channel];
            }; break;
            case 'oob': {
                if (this._handleOpOob(msg, payload, state))
                    delete this.channelStates[state.channel];
            }; break;
            default: Edsulog('unexpected message on _subPut channel', JSON.stringify(msg));
        }
    };
    Object.assign(state, { msgCb, op });

    // Store state and kick off
    this._initiateChannelState(state);
}



EdsuConnRaw.prototype._nameReceive = function(name, hash) {
    let sub = this.subsState.localSubs[name];

    if (sub !== undefined) {
        if (sub.expires && Date.now() > sub.expires) {
            delete this.subsState.localSubs[name];
            delete this.subsState.remoteSubs[name];
            return;
        }
        if (sub.hash != hash) {
            sub.cbs.notify({
                ok: true,
                name,
                hash,
            });
            if (sub.once) {
                delete this.subsState.localSubs[name];
                delete this.subsState.remoteSubs[name];
            } else {
                sub.hash = hash;
                this.subsState.remoteSubs[name].hash = hash;
            }
        }
    }
};


// Verify hash, cache, and alert any watchers
EdsuConnRaw.prototype._blockReceive = function(hash, bytes) {
    if (Edsu.multihash(bytes) != hash) {
        this.cbs.err({
            err: EdsuErr.server,
            debug: 'recieved bad block (hash didn\'t match)',
            reconnecting: true,
        });
        this._connect();
        return false;
    }
    if (EdsuCache.put(hash, bytes)) {
        EdsuLog('put in cache: ', hash);
        let channels = this.cacheWatchers[hash];
        if (channels !== undefined) {
            channels.forEach(channel => {
                let state = this.channelStates[channel];
                if (state !== undefined && state.cacheHitCb) {
                    state.cacheHitCb();
                }
            });
        }
    } else {
        EdsuLog('pulled from cache: ', hash);
    }
    // Rebuild cacheWatchers to only include channels that exist
    this.cacheWatchers = Object.keys(this.cacheWatchers).reduce((a, k) => {
        let v = this.cacheWatchers[k];
        let channels = [];
        v.forEach(channel => {
            if (this.channelStates.hasOwnProperty(channel))
                channels.push(channel);
        });
        if (channels.length != 0)
            a[k] = channels;
        return a;
    }, {});

    return true;
}


// Return: true == op is complete; remove the state
EdsuConnRaw.prototype._handleOpOob = function(msg, payload, state) {
    let retryMs = parseInt(msg['retry-delay-ms']);

    if (!state.noRetry && Edsu.isNumeric(retryMs) && Date.now() + retryMs < state.expires) {
        // Do retry (unless asked not to, or we won't have time)
        this._addTimedFn(Date.now() + retryMs, () => {
            if (this.stage == this.STAGE_IDLING) {
                this._connect();
            } else if (this.stage == this.STAGE_CONNECTED) {
                state.op();
            }
        });
        return false;
    } else {
        // Report error
        // These are the expected channel errors
        let err = {
            'not-found': EdsuErr.notFound,
            'hash-mismatch': EdsuErr.hashMismatch,
            'timed-out': EdsuErr.timedOut,
            'permission-denied': EdsuErr.permissionDenied,
            'invalid-input': EdsuErr.invalidInput,
            'invalid-content': EdsuErr.invalidContent,
            'server-error': EdsuErr.server,
            'rate-limited': EdsuErr.rateLimited,
        }[msg.code];

        // If there's an unexpected message on the channel, or it wants to close
        // the connection, let the connection OOB handler deal with it
        if (msg['close-connection'] == 'true' || err === undefined) {
            state.errCb({
                err: EdsuErr.unexpected,
                debug: 'unexpected OOB message',
            });
            this._handleConnMsg(msg, payload);
            return true;
        }

        let debug = payload ? Edsu.decodeEson(payload).eson['edsu:debug'] : {};

        state.errCb({
            err,
            debug: 'error during ' + state.opId + ': ' + JSON.stringify(debug),
        });
        return true;
    }
}


EdsuConnRaw.prototype._handleConnMsg = function(msg, payload) {
    // Handle OOB
    let closeConn = msg['close-connection'] == 'true';
    switch (msg.edsu) {
        case 'hello': {
            if (msg['version'] != '0.1') {
                this.cbs.err({
                    err: EdsuErr.versionMismatch,
                    debug: 'server cannot speak our Edsu version',
                    reconnecting: false,
                });
                this.close();
            }
        }; break;
        case 'ping': {
            this.ws.send(Edsu.stringToBytes('edsu pong\n\n'));
        }; break;
        case 'pong': {
            // NOP
        }; break;
        case 'authenticated': {
            if (this.cbs.info)
                this.cbs.info({ok: true, info: EdsuInfo.authenticated});
            // (Re)try any auth-requiring functions that were in flight while we were not authed
            Object.keys(this.channelStates).forEach(channel => {
                let state = this.channelStates[channel];
                if (!state.noAuthNeeded && (!state.noRetry || !state.sent))
                    state.op();
            });
        }; break;
        case 'block': {
            // Chained block
            this._blockReceive(msg.hash, payload);
        }; break;
        case 'oob': {
            switch (msg.code) {
                case 'advisory': {
                    // NOP
                }; break;
                case 'redirect': {
                    let payloadEsonResult = Edsu.decodeEson(payload);
                    if (payloadEsonResult.ok &&
                        Edsu.validateUsername(payloadEsonResult.eson.username))
                    {
                        let newUsername = payloadEsonResult.eson.username;
                        this.domain = newUsername.replace(/@/, '.edsu.');
                        this.userTo = newUsername;
                        EdsuLogger('redirecting to: ' + newUsername);
                        this._connect(true);
                    } else {
                        this.cbs.err({
                            err: EdsuErr.server,
                            debug: 'server error - bad redirect',
                            reconnecting: false,
                        });
                        this.close();
                    }
                }; break;
                case 'authentication-error': {
                    this.cbs.err({
                        err: EdsuErr.authentication,
                        debug: 'authentication error',
                        reconnecting: false,
                        retryDelayMs: msg['retry-delay-ms'],
                    });
                    this.close();
                }; break;
                case 'server-error': {
                    this.cbs.err({
                        err: EdsuErr.server,
                        debug: 'server error',
                        reconnecting: true,
                    });
                    this._connect(true);
                }; break;
                case 'dropped-subscription-notifications': {
                    // Our local state is now all suspect, reflect that and re-sync
                    this.subsState.localSubs.forEach(sub => sub.hash = null);
                    this._subsSync();
                }; break;
                default: {
                    this.cbs.err({
                        err: EdsuErr.unexpected,
                        debug: 'unexpected OOB message',
                        details: { msg, payload },
                        reconnecting: true,
                    });
                    this._connect(true);
                }
            }
            // Catch all close-connection
            if (msg['close-connection'] == 'true' && this.stage == this.STAGE_CONNECTED) {
                this._connect(true);
            }
        }; break;
        default: {
            this.cbs.err({
                err: EdsuErr.unexpected,
                debug: 'unexpected message on the connection channel',
                details: { msg },
                reconnecting: true,
            });
            // Reconnect
            this._connect(true);
        }
    }
};


// Firefox (at least) has a naive WS send() - this is to help group a whole msg together first
EdsuConnRaw.prototype._sendAll = function(bytesArray) {
    let len = bytesArray.reduce((a, x) => a + x.length, 0);
    let bytes = new Uint8Array(len);
    bytesArray.reduce((a, x) => {
        bytes.set(x, a);
        return a + x.length;
    }, 0);
    this.ws.send(bytes);

    // Debug
    EdsuLog(Edsu.bytesToString(bytes));
}


// opts: timeoutMs, noRetry
EdsuConnRaw.prototype._initOpState = function(opId, opts, cbs) {
    return {
        channel: ++this.curChannel,
        opId,
        sent: false,
        expires: Date.now() + ((opts.timeoutMs) ? opts.timeoutMs : this.DEFAULT_TIMEOUT_MS),
        noRetry: Boolean(opts.noRetry),
        errCb: cbs.err,
    };
};

EdsuConnRaw.prototype._validateArgs = function(fnName, errCb, validations) {
    for (let i = 0; i < validations.length; i++) {
        let valid = validations[i];
        if (!valid.fn(valid.val)) {
            errCb({
                err: EdsuErr.invalidInput,
                debug: fnName + ': invalid ' + valid.name,
                detail: { arg: valid.val },
            });
            return false;
        }
    }
    return true;
};


EdsuConnRaw.prototype._sendMsg = function(state, edsu, eson, payload) {
    let esonBytes = Edsu.encodeEson(Object.assign({
        channel: String(state.channel),
    }, eson));
    if (!esonBytes.ok)
        throw new Error('invalid ESON for message');
    let arr = [
        Edsu.stringToBytes('edsu '),
        Edsu.stringToBytes(edsu),
        new Uint8Array([0x0a]),
        esonBytes.bytes,
    ];
    if (payload)
        arr.push(payload);
    this._sendAll(arr);
    state.sent = true;
};


EdsuConnRaw.prototype._initiateChannelState = function(state) {
    this.channelStates[state.channel] = state;
    if (this.stage == this.STAGE_IDLING) {
        this._connect();
    } else if (this.stage == this.STAGE_CONNECTED) {
        state.op();
    }

    return { ok: true, channel: state.channel };
};



////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\
// Edsu
////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\

const Edsu = {
    PROTOCOL_VERSION: '0.1',
    BLOCK_FORMAT_VERSION: 0b00000001,
    MAX_BLOCK_SIZE: 63 * 1024,
    MAX_PAYLOAD_LEN: 1024 * 63,
    SALT_LEN: 8,
    B58_ALPHABET: '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz',
    B58_ALPHABET_INV: { 1: 0, 2: 1, 3: 2, 4: 3, 5: 4, 6: 5, 7: 6, 8: 7, 9: 8, A: 9, B: 10,
                        C: 11, D: 12, E: 13, F: 14, G: 15, H: 16, J: 17, K: 18, L: 19,
                        M: 20, N: 21, P: 22, Q: 23, R: 24, S: 25, T: 26, U: 27, V: 28,
                        W: 29, X: 30, Y: 31, Z: 32, a: 33, b: 34, c: 35, d: 36, e: 37, f: 38,
                        g: 39, h: 40, i: 41, j: 42, k: 43, m: 44, n: 45, o: 46, p: 47, q: 48,
                        r: 49, s: 50, t: 51, u: 52, v: 53, w: 54, x: 55, y: 56, z: 57 },
    // opts: salt
    encodeBinaryBlock(contents, hashes, opts) {
        if (opts === undefined)
            opts = {};
        // Generate the salt if necessary
        let salt;
        if (typeof opts.salt == 'string' || opts.salt instanceof String) {
            salt = Edsu.stringToBytes(opts.salt);
        } else if (opts.salt instanceof Uint8Array) {
            salt = opts.salt;
        } else {
            salt = Edsu.generateSalt(Edsu.SALT_LEN);
        }
        // Convert the hashes to bytes and allocate the block buffer
        let byteHashes = hashes.map(x => Edsu.decodeB58(x));
        let byteHashesLen = byteHashes.reduce((a, x) => x.length + a, 0);
        let byteHashesLoc = 5 + salt.length;
        let contentsLoc = byteHashesLoc + byteHashesLen;
        let blockLen = contentsLoc + contents.length;

        if (blockLen > Edsu.MAX_BLOCK_SIZE)
            return { err: EdsuErr.invalidInput, debug: 'block too large' }; 
        let block = new Uint8Array(blockLen);
                
        // Set the version
        block[0] = Edsu.BLOCK_FORMAT_VERSION;
                    
        // Set the hash and contents locations (big endian)
        block[1] = (contentsLoc) >> 8;
        block[2] = (contentsLoc) & 0xff;
        block[3] = (byteHashesLoc) >> 8;
        block[4] = (byteHashesLoc) & 0xff;

        // Copy the salt in
        block.set(salt, 5);

        // Copy the hashes in
        let loc = byteHashesLoc;
        byteHashes.forEach(h => {
            block.set(h, loc);
            loc += h.length;
        });

        // Copy the contents in
        block.set(contents, contentsLoc);

        return { ok: true, bytes: block };
    },

    // opts: salt
    encodeTextBlock(text, opts) {
        if (opts === undefined)
            opts = {};
        // Generate the salt if necessary
        let salt;
        if (opts.salt === undefined || opts.salt === null) {
            salt = Edsu.encodeB58(Edsu.generateSalt(Edsu.SALT_LEN));
        } else {
            salt = opts.salt;
        }
        let saltBytes = Edsu.stringToBytes(salt);

        // Convert to bytes
        let bytes = Edsu.stringToBytes(text);
        let blockLen = bytes.length + 2 + saltBytes.length;
        if (blockLen > Edsu.MAX_BLOCK_SIZE)
            return { err: EdsuErr.invalidInput, debug: 'block too large' }; 

        // Allocate array then copy in
        let block = new Uint8Array(blockLen);
        block[0] = 0x7e; // Tilde
        block.set(saltBytes, 1);
        block[saltBytes.length+1] = 0x0a; // Newline
        block.set(bytes, 2 + saltBytes.length);
        return { ok: true, bytes: block };
    },

    decodeBlock(bytes) {
        // Grab the version
        let version = bytes[0];

        // Special case: 0x7e == tilde == text block
        if (version == 0x7e) {
            let nlLoc = bytes.indexOf(0x0a);
            if (nlLoc == -1)
                return { err: EdsuErr.invalidInput, debug: 'header missing newline' };
            let salt = bytes.subarray(1, nlLoc);
            let contents = bytes.subarray(nlLoc+1);
            return { ok: true, contents, salt, hashes: [], isText: true };
        }

        // Otherwise it's a binary block
        // - Decode the locations from big-endianland
        let contentsLoc =   bytes[1] << 8;
        contentsLoc |=      bytes[2];
        let hashesLoc =     bytes[3] << 8;
        hashesLoc |=        bytes[4];

        // - Contents & salt are easy
        let contents = bytes.subarray(contentsLoc);
        let salt = bytes.subarray(5, hashesLoc);

        // - Hashes, we need to loop through and decode their lengths (only
        //   supporting 3 bytes (~1M hash functions)
        let hashes = [];
        let loc = hashesLoc;
        while (loc < contentsLoc) {
            // Skip the hash function bytes
            let startLoc = loc;
            if (bytes[loc] & 0b10000000 != 0) {
                loc += 1;
                if (bytes[loc] & 0b11000000 != 0)
                    loc += 1;
                if (bytes[loc] & 0b10000000 != 0)
                    return { err: EdsuErr.invalidInput, debug: 'multihash has giant varint' };
            }
            loc += 1;

            // Grab the digest length (under 128 bytes)
            if (bytes[loc] & 0b10000000 != 0)
                return { err: EdsuErr.invalidInput, debug: 'multihash has giant varint' };
            let hashEnd = startLoc + (loc - startLoc) + 1 + bytes[loc];
            hashes.push(Edsu.decodeB58(bytes.subarray(startLoc, hashEnd)));
            loc = hashEnd;
            if (loc > contentsLoc)
                return { err: EdsuErr.invalidInput, debug: 'multihash' };
        }

        return { ok: true, contents, salt, hashes, isText: false };
    },

    decodeEson(bytes) {
        // Special case: empy ESON (i.e. '\n')
        if (bytes.length == 1 && bytes[0] == 0xa)
            return { ok: true, eson: {} };
        let result = Edsu.decodeEsonStream(bytes);
        if (result.ok) {
            if (result.eson === null) {
                return { err: EdsuErr.invalidInput, debug: 'does not contain complete ESON' };
            } else if (result.unconsumedBytes.length != 0) {
                return { err: EdsuErr.invalidInput, debug: 'trailing bytes' };
            } else {
                return { ok: true, eson: result.eson };
            }
        }
        return result;
    },

    decodeEsonStream(bytes) {
        let loc = 0;
        let consumedTo = 0;
        let eson = {};
        let curKey = null;

        while (loc < bytes.length) {
            // Double newline - ESON is done (or it's newline preamble)
            if (bytes[loc] == 0x0a) {
                if (Object.keys(eson).length != 0) {
                    return {
                        ok: true,
                        eson,
                        unconsumedBytes: bytes.subarray(loc+1),
                    }
                }
                // Skip newlines
                while (bytes[loc] == 0x0a) {
                    loc++;
                    consumedTo = loc;
                    if (loc == bytes.length)
                        break;
                }
            }

            // Get key
            let spaceLoc = bytes.indexOf(0x20, loc);
            if (spaceLoc == -1)
                break;
            let key = Edsu.bytesToString(bytes.subarray(loc, spaceLoc));
            loc = spaceLoc + 1;
            if (loc >= bytes.length)
                break;

            // Get val
            let newlineLoc = bytes.indexOf(0x0a, loc);
            if (newlineLoc == -1)
                break;
            let val = Edsu.bytesToString(bytes.subarray(loc, newlineLoc));
            loc = newlineLoc + 1;
            if (loc >= bytes.length)
                break;

            // Add item
            if (key == '^') {
                if (curKey === null)
                    return { err: EdsuErr.InvalidInput, debug: 'orphaned ^' };
                if (Array.isArray(eson[curKey]))
                    eson[curKey].push(val);
                else
                    eson[curKey] = [eson[curKey], val];
            } else {
                eson[key] = val;
                curKey = key;
            }
        }

        return {
            ok: true,
            eson: null,
            unconsumedBytes: bytes.subarray(consumedTo),
        };
    },

    encodeEson(eson) {
        // Convert to a list of byte arrays
        let bytesArray = [];
        let pushVal = v => {
            if (!Edsu.validateValue(v)) {
                return {
                    err: EdsuErr.invalidInput,
                    debug: 'tried to ESON encode bad value: ' + v,
                };
            }
            bytesArray.push(new Uint8Array([0x20]));
            bytesArray.push(Edsu.stringToBytes(v));
            bytesArray.push(new Uint8Array([0x0a]));
        };
        let errMaybe;
        Object.keys(eson).sort().every(key => {
            let val = eson[key];
            if (val === null || val === undefined || (Array.isArray(val) && val.length == 0))
                return !errMaybe;
            if (Array.isArray(val)) {
                val.every((v, i) => {
                    bytesArray.push(Edsu.stringToBytes(i == 0 ? key : '^'));
                    errMaybe = pushVal(v);
                    return !errMaybe;
                });
            } else {
                bytesArray.push(Edsu.stringToBytes(key));
                errMaybe = pushVal(val);
            }
            return !errMaybe;
        });
        if (errMaybe)
            return errMaybe;
        bytesArray.push(new Uint8Array([0x0a]));

        // Copy to a single byte array
        let bytesLen = bytesArray.reduce((a, x) => x.length + a, 0);
        let bytes = new Uint8Array(bytesLen);
        let loc = 0;
        bytesArray.forEach(bytesFragment => {
            bytes.set(bytesFragment, loc);
            loc += bytesFragment.length;
        });
        return { ok: true, bytes };
    },
    
    encodeB58(bytes) {
        if (!(bytes instanceof Uint8Array))
            throw { err: EdsuErr.invalidInput, debug: 'tried to base58 encode a non-Uint8Array' };
        let digits = [0];
        for (let i = 0; i < bytes.length; ++i) {
            let carry = bytes[i];
            for (let j = 0; j < digits.length; ++j) {
                carry += digits[j] << 8;
                digits[j] = carry % 58;
                carry = carry / 58 | 0;
            }
            while (carry > 0) {
                digits.push(carry % 58);
                carry = (carry / 58) | 0;
            }
        }
        return digits.reverse().map(x => Edsu.B58_ALPHABET[x]).join('');
    },

    decodeB58(text) {
        let bytes = [0]
        for (let i = 0; i < text.length; i++) {
            let carry = Edsu.B58_ALPHABET_INV[text[i]];
            if (carry === undefined)
                throw new Error('invalid B58 encoding');
            for (let j = 0; j < bytes.length; ++j) {
                carry += bytes[j] * 58;
                bytes[j] = carry & 0xff;
                carry >>= 8;
            }
            while (carry > 0) {
                bytes.push(carry & 0xff);
                carry >>= 8;
            }
        }
        for (let i = 0; text[i] == Edsu.B58_ALPHABET[0] && i < text.length - 1; i++)
            bytes.push(0);

        return Uint8Array.from(bytes.reverse());
    },

    // opts: salt
    encodeEsonToBlockBytes(eson, opts) {
        if (opts === undefined)
            opts = {};

        // Convert to bytes
        let bytes = Edsu.encodeEson(eson);
        if (!bytes.ok)
            return bytes;
        else
            bytes = bytes.bytes;

        // Generate the salt if necessary
        let salt;
        if (opts.salt === undefined || opts.salt === null) {
            salt = Edsu.encodeB58(Edsu.generateSalt(Edsu.SALT_LEN));
        } else {
            salt = opts.salt;
        }
        let saltBytes = Edsu.stringToBytes(salt);

        // Allocate array then copy in
        let blockLen = bytes.length + 2 + saltBytes.length;
        if (blockLen > Edsu.MAX_BLOCK_SIZE)
            return { err: EdsuErr.invalidInput, debug: 'block too large' }; 
        let block = new Uint8Array(blockLen);
        block[0] = 0x7e; // Tilde
        block.set(saltBytes, 1);
        block[saltBytes.length+1] = 0x0a; // Newline
        block.set(bytes, 2 + saltBytes.length);
        return { ok: true, bytes: block };
    },

    multihash(bytes) {
        let hash = Edsu._hashSha256(bytes);

        // Put in the hash identifier and b58 encode
        let toHash = new Uint8Array(34);
        toHash.set(hash, 2);
        toHash[0] = 0x12;
        toHash[1] = 0x20;

        return Edsu.encodeB58(toHash);
    },

    hmacSha256(key, text) {
        const PADDED_KEY_LEN = 64;
        const INNER_PAD = 0x36;
        const OUTER_PAD = 0x5c;

        // Proper HMAC handles keys longer than the padded length, which we don't need
        if (key.length > PADDED_KEY_LEN)
            throw { err: EdsuErr.invalidInput, debug: 'this HMAC implementation only handles ' +
                                                      'keys up to ' + PADDED_KEY_LEN + ' bytes' };

        // Do the padding
        let keyPadded = new Uint8Array(PADDED_KEY_LEN);
        keyPadded.set(key, 0);
        let innerPadded = new Uint8Array(PADDED_KEY_LEN);
        let outerPadded = new Uint8Array(PADDED_KEY_LEN);
        innerPadded.fill(INNER_PAD);
        outerPadded.fill(OUTER_PAD);
        for (let i = 0; i < PADDED_KEY_LEN; i++) {
            innerPadded[i] ^= keyPadded[i];
            outerPadded[i] ^= keyPadded[i];
        }

        // Inner hash
        let innerBytes = new Uint8Array(PADDED_KEY_LEN + text.length);
        innerBytes.set(innerPadded, 0);
        innerBytes.set(text, PADDED_KEY_LEN);
        let innerHash = Edsu._hashSha256(innerBytes);

        // Outer hash
        let outerBytes = new Uint8Array(PADDED_KEY_LEN + innerHash.length);
        outerBytes.set(outerPadded, 0);
        outerBytes.set(innerHash, PADDED_KEY_LEN);
        return Edsu._hashSha256(outerBytes);
    },

    bytesToString(bytes) {
        let dec = new TextDecoder('utf-8');
        return dec.decode(bytes);
    },

    stringToBytes(text) {
        let enc = new TextEncoder('utf-8');
        return enc.encode(text);
    },

    generateSalt(length) {
        let salt = new Uint8Array(length);
        for (let i = 0; i < length; i++)
            salt[i] = Math.floor(Math.random() * 256);
        return salt;
    },

    validateMultihash(x) {
        let bytes;
        try { 
            bytes = Edsu.decodeB58(x);
        } catch (e) {
            return false;
        }

        if (bytes.length < 8)
            return false;

        // Skip the varint hash function bytes.  Only considering varints of up to
        // three bytes valid (enough to encode ~1M hash functions)
        let i = 0;
        if (bytes[i] & 0b10000000 != 0) {
            i += 1;
            if (bytes[i] & 0b11000000 != 0)
                i += 1;
            if (bytes[i] & 0b10000000 != 0)
                return false;
        }
        i += 1;

        // Grab the length (under 128 bytes)
        if (bytes[i] & 0b10000000 != 0)
            return false;
        let digestLen = bytes[i];
        return i + 1 + digestLen == bytes.length;
    },

    validateName(x) {
        if (typeof x != 'string')
            return false;
        let split = x.split('.');
        return split.length >= 5 && x.split('.').every(s => /^[a-zA-Z0-9_-]+$/.exec(s));
    },

    validateNamePattern(x) {
        if (x.endsWith('.*'))
            return Edsu.validateName(x.slice(0, -1) + 'a');
        else
            return Edsu.validateName(x);
    },

    validateKey(x) {
        if (typeof x != 'string')
            return false;
        return x.split(':').every(s => /^[a-z0-9-]+$/.exec(s));
    },

    validateChain(x) {
        // a*k a$k a$k>b a@k 
        if (typeof x != 'string')
            return false;
        if (x === '')
            return true;
        let re = /^([a-z0-9-]+)([*$@][^*$@>]+)([>][a-z0-9-]+)?$/;
        return x.split(' ').every(item => {
            let arr = re.exec(item);
            if (!arr)
                return false;
            let sel = arr[2][0];
            let key = arr[2].slice(1);

            return (
                ((sel == '*' || sel == '@') && Edsu.validateKey(key) && arr[3] === undefined) ||
                (sel == '$' && Edsu.validateKey(key))
            );
        });
    },

    validateValue(x) {
        if (typeof x != 'string')
            return false;
        return Boolean(/^[ -~]*$/.exec(x));
    },

    validateUsername(x) {
        if (typeof x != 'string')
            return false;
        let parts = /^([a-z0-9-]+)@([a-z0-9-]+)[.]([a-z0-9-]+)$/.exec(x);
        if (!parts)
            return false;
        return parts.slice(1).every(y => {
            return !y.startsWith('-') && !y.endsWith('-');
        });
    },
    
    encodeEsonUtf8(x) {
        return x.replace(/([^ -}]+)/g, (_dc, y) => {
            return ['~', Edsu.encodeB58(Edsu.stringToBytes(y)), '^'].join('');
        });
    },

    decodeEsonUtf8(x) {
        return x.replace(/(~[1-9A-HJ-NP-Za-km-z]*\^)/g, (_dc, y) => {
            return Edsu.bytesToString(Edsu.decodeB58(y.slice(1, -1)));
        });
    },

    isNumeric(x) {
        return !isNaN(parseFloat(x)) && isFinite(x);
    },

    //////////////////////////////////////////////
    // Util
    //////////////////////////////////////////////

    // based on https://github.com/AndersLindman/SHA256
    _hashSha256(bytes) {
        let K = [
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b,
            0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01,
            0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7,
            0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
            0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152,
            0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
            0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
            0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819,
            0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08,
            0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f,
            0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
            0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2];

        let h0 = 0x6a09e667;
        let h1 = 0xbb67ae85;
        let h2 = 0x3c6ef372;
        let h3 = 0xa54ff53a;
        let h4 = 0x510e527f;
        let h5 = 0x9b05688c;
        let h6 = 0x1f83d9ab;
        let h7 = 0x5be0cd19;
        let length = bytes.length;
        let byteLength = Math.floor((length + 72) / 64) * 64;
        let wordLength = byteLength / 4;
        let bitLength = length * 8;
        let m = new Uint8Array(byteLength);
        m.set(bytes);
        m[length] = 0x80;
        m[byteLength - 4] = bitLength >>> 24;
        m[byteLength - 3] = (bitLength >>> 16) & 0xff;
        m[byteLength - 2] = (bitLength >>> 8) & 0xff;
        m[byteLength - 1] = bitLength & 0xff;
        let words = new Int32Array(wordLength);
        let byteIndex = 0;
        for (let i = 0; i < words.length; i++) {
            let word = m[byteIndex] << 24;
            word |= m[byteIndex + 1] << 16;
            word |= m[byteIndex + 2] << 8;
            word |= m[byteIndex + 3];
            words[i] = word;
            byteIndex += 4;
        }
        let w = new Int32Array(64);
        for (let j = 0; j < wordLength; j += 16) {
            for (let i = 0; i < 16; i++) {
                w[i] = words[j + i];
            }
            for (let i = 16; i < 64; i++) {
                let v = w[i - 15];
                let s0 = (v >>> 7) | (v << 25);
                s0 ^= (v >>> 18) | (v << 14);
                s0 ^= (v >>> 3);
                v = w[i - 2];
                let s1 = (v >>> 17) | (v << 15);
                s1 ^= (v >>> 19) | (v << 13);
                s1 ^= (v >>> 10);
                w[i] = (w[i - 16] + s0 + w[i - 7] + s1) & 0xffffffff;
            }
            let a = h0;
            let b = h1;
            let c = h2;
            let d = h3;
            let e = h4;
            let f = h5;
            let g = h6;
            let h = h7;
            for (let i = 0; i < 64; i++) {
                let s1 = (e >>> 6) | (e << 26);
                s1 ^= (e >>> 11) | (e << 21);
                s1 ^= (e >>> 25) | (e << 7);
                let ch = (e & f) ^ (~e & g);
                let temp1 = (h + s1 + ch + K[i] + w[i]) & 0xffffffff;
                let s0 = (a >>> 2) | (a << 30);
                s0 ^= (a >>> 13) | (a << 19);
                s0 ^= (a >>> 22) | (a << 10);
                let maj = (a & b) ^ (a & c) ^ (b & c);
                let temp2 = (s0 + maj) & 0xffffffff;
                h = g;
                g = f;
                f = e;
                e = (d + temp1) & 0xffffffff;
                d = c;
                c = b;
                b = a;
                a = (temp1 + temp2) & 0xffffffff;
            }
            h0 = (h0 + a) & 0xffffffff;
            h1 = (h1 + b) & 0xffffffff;
            h2 = (h2 + c) & 0xffffffff;
            h3 = (h3 + d) & 0xffffffff;
            h4 = (h4 + e) & 0xffffffff;
            h5 = (h5 + f) & 0xffffffff;
            h6 = (h6 + g) & 0xffffffff;
            h7 = (h7 + h) & 0xffffffff;
        }
        let hash = new Uint8Array(32);
        for (let i = 0; i < 4; i++) {
            hash[i]      = (h0 >>> (8 * (3 - i))) & 0xff;
            hash[i + 4]  = (h1 >>> (8 * (3 - i))) & 0xff;
            hash[i + 8]  = (h2 >>> (8 * (3 - i))) & 0xff;
            hash[i + 12] = (h3 >>> (8 * (3 - i))) & 0xff;
            hash[i + 16] = (h4 >>> (8 * (3 - i))) & 0xff;
            hash[i + 20] = (h5 >>> (8 * (3 - i))) & 0xff;
            hash[i + 24] = (h6 >>> (8 * (3 - i))) & 0xff;
            hash[i + 28] = (h7 >>> (8 * (3 - i))) & 0xff;
        }
        return hash;
    },

    _wrapCb(f) {
        function ff() {
            if (!f)
                return;
            try {
                f.apply(null, arguments);
            } catch (e) {
                EdsuLog('caught exception in callback:', e);
            }
        }
        return ff;
    },
};


////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\
// EdsuCache
////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\////\\\\

let EdsuCache = {
    memSizeKb: 16 * 1024, 
    blockStates: {},

    // opts: memSizeKb
    setOpts(opts) {
        if (opts.memSizeKb) {
            this.memSizeKb = memSizeKb;
            this.cull();
        }
    },

    put(hash, bytes) {
        let existingBlockState = this.blockStates[hash];
        if (existingBlockState) {
            existingBlockState.lastAccess = Date.now();
            existingBlockState.timesAccessed++;
            return false;
        } else {
            this.blockStates[hash] = {
                bytes,
                lastAccess: Date.now(),
                timesAccessed: 1,
            }
            this.cull();
            return true;
        }
    },

    get(hash) {
        let blockState = this.blockStates[hash];
        if (!blockState)
            return null;
        blockState.lastAccess = Date.now();
        blockState.timesAccessed++;
        return blockState.bytes;
    },

    cull () {
        // Collect the info we need
        let stats = Object.keys(this.blockStates).reduce((acc, hash) => {
            let state = this.blockStates[hash];
            acc.totalSize += state.bytes.length;
            acc.blocks.push([state.timesAccessed, state.lastAccess, hash]);
            return acc;
        }, { totalSize: 0, blocks: [] });

        // If the total size is OK, we're done
        let over = stats.totalSize - this.memSizeKb * 1024;
        if (over < 0)
            return;

        // Do a sort as a naive algo for prioritizing
        let now = Date.now();
        let blocks = stats.blocks;
        blocks.sort((l, r) => {
            // The time since accessed decreased by a factor of how many accesses
            let lScore = (now - l[1]) / l[0];
            let rScore = (now - r[1]) / r[0];
            // Sort largest to smallest (i.e. l & r reversed)
            return rScore - lScore;
        });

        // Spin through the blocks, removing until the overage is gone
        // This is best efforts - we might not be able to get it under the goal
        for (let i = 0; i < blocks.length && over > 0; i++) {
            let blk = blocks[i];
            // Never remove if less than 2 seconds old
            if (now - blk[1] < 2000)
                continue;
            over -= this.blockStates[blk[2]] || 0;
            delete this.blockStates[blk[2]];
        }
    },
};


// Following (until END) Copyright 2017 Sam Thorogood. All rights reserved. under http://www.apache.org/licenses/LICENSE-2.0 - For Edge 44
(function(l){function m(b){b=void 0===b?"utf-8":b;if("utf-8"!==b)throw new RangeError("Failed to construct 'TextEncoder': The encoding label provided ('"+b+"') is invalid.");}function k(b,a){b=void 0===b?"utf-8":b;a=void 0===a?{fatal:!1}:a;if("utf-8"!==b)throw new RangeError("Failed to construct 'TextDecoder': The encoding label provided ('"+b+"') is invalid.");if(a.fatal)throw Error("Failed to construct 'TextDecoder': the 'fatal' option is unsupported.");}if(l.TextEncoder&&l.TextDecoder)return!1;
Object.defineProperty(m.prototype,"encoding",{value:"utf-8"});m.prototype.encode=function(b,a){a=void 0===a?{stream:!1}:a;if(a.stream)throw Error("Failed to encode: the 'stream' option is unsupported.");a=0;for(var h=b.length,f=0,c=Math.max(32,h+(h>>1)+7),e=new Uint8Array(c>>3<<3);a<h;){var d=b.charCodeAt(a++);if(55296<=d&&56319>=d){if(a<h){var g=b.charCodeAt(a);56320===(g&64512)&&(++a,d=((d&1023)<<10)+(g&1023)+65536)}if(55296<=d&&56319>=d)continue}f+4>e.length&&(c+=8,c*=1+a/b.length*2,c=c>>3<<3,
g=new Uint8Array(c),g.set(e),e=g);if(0===(d&4294967168))e[f++]=d;else{if(0===(d&4294965248))e[f++]=d>>6&31|192;else if(0===(d&4294901760))e[f++]=d>>12&15|224,e[f++]=d>>6&63|128;else if(0===(d&4292870144))e[f++]=d>>18&7|240,e[f++]=d>>12&63|128,e[f++]=d>>6&63|128;else continue;e[f++]=d&63|128}}return e.slice(0,f)};Object.defineProperty(k.prototype,"encoding",{value:"utf-8"});Object.defineProperty(k.prototype,"fatal",{value:!1});Object.defineProperty(k.prototype,"ignoreBOM",{value:!1});k.prototype.decode=
function(b,a){a=void 0===a?{stream:!1}:a;if(a.stream)throw Error("Failed to decode: the 'stream' option is unsupported.");b=new Uint8Array(b);a=0;for(var h=b.length,f=[];a<h;){var c=b[a++];if(0===c)break;if(0===(c&128))f.push(c);else if(192===(c&224)){var e=b[a++]&63;f.push((c&31)<<6|e)}else if(224===(c&240)){e=b[a++]&63;var d=b[a++]&63;f.push((c&31)<<12|e<<6|d)}else if(240===(c&248)){e=b[a++]&63;d=b[a++]&63;var g=b[a++]&63;c=(c&7)<<18|e<<12|d<<6|g;65535<c&&(c-=65536,f.push(c>>>10&1023|55296),c=56320|
c&1023);f.push(c)}}return String.fromCharCode.apply(null,f)};l.TextEncoder=m;l.TextDecoder=k})("undefined"!==typeof window?window:"undefined"!==typeof global?global:this);
// END
