#![allow(unused_imports, dead_code)]
extern crate bs58;
extern crate rand;
#[macro_use] extern crate edsu_server;
extern crate edsu_client;
#[macro_use] extern crate edsu_common;
use std::{convert, fs, env, process};
use std::io::{self, Read, Write};
use std::path::{PathBuf};
use edsu_server::{util, logging};
use edsu_server::ext::{self};
use edsu_server::prelude::*;
use edsu_common::eson::{self};
use edsu_common::edsu::{self, NameSegment};

//const CONFIG_PATH_STR: &'static str = "/usr/local/edsu/etc/forms.eson";
const FORMS_ROOT: &'static str = "/usr/local/edsu/db/forms";
const NAME_PREFIX: &'static str = "pub.srv.edclave.forms.";

#[derive(Debug)]
enum Error {
    Eson(eson::Error),
    Edsu(edsu::Error),
    Io(io::Error),
    InvalidInput(String),
}
impl convert::From<Error> for String { fn from(e: Error) -> Self { format!("{:?}", e) } }
impl convert::From<edsu::Error> for Error { fn from(e: edsu::Error) -> Self { Error::Edsu(e) } }
impl convert::From<eson::Error> for Error { fn from(e: eson::Error) -> Self { Error::Eson(e) } }
impl convert::From<io::Error> for Error { fn from(e: io::Error) -> Self { Error::Io(e) } }


fn append_form(form_name: &str, line: &str) -> Result<(), Error> {
    NameSegment::validate_str(form_name)?; // Name segments are fs-safe
    let mut form_path = PathBuf::from(FORMS_ROOT);
    if !form_path.is_dir() {
        fs::DirBuilder::new()
            .recursive(true)
            .create(&form_path).map_err(|e| Error::Io(e))?;
    }
    form_path.push(form_name);
    let mut file = fs::OpenOptions::new()
        .append(true)
        .create(true)
        .open(form_path)?;
    file.write_all(format!("{}\n", line).as_bytes())?;
    Ok(())
}


fn main() {
    ext::handler(Box::new(|req| {
        if let ext::Op::NameGet(name) = req.op {
            if !name.starts_with(NAME_PREFIX) {
                e_err!("unknown name: {:?}", name);
                return Ok(None);
            }
            let short_name = name.as_str().split_at(NAME_PREFIX.len()).1;
            let mut iter = short_name.split('.');
            match iter.next().unwrap() {
                "email" => {
                    let form_name = if let Some(x) = iter.next() { x } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing form name")));
                    };
                    let email_enc = if let Some(x) = iter.next() { x } else {
                        return Ok(Some(ext::err_block_put("invalid-input", "missing email")));
                    };
                    let email = unescape_field("email", email_enc)?;
                    let line = format!("{} {}", util::now(), email);
                    append_form(&form_name, &line)?;
                    return Ok(Some(ext::ok_block_put()));
                },
                _ => {
                    e_err!("unknown sub-name: {:?}", short_name);
                    return Ok(None);
                },
            }
        } else {
            e_err!("unknown op: {:?}", req.op);
            return Ok(None);
        }
    }));
}


fn unescape_field(field_name: &str, text: &str) -> Result<String, Error> {
    let bytes = bs58::decode(text.as_bytes()).into_vec().map_err(|_| {
        Error::InvalidInput(format!("invalid b58 encoding of field: {:?}", field_name))
    })?;
    Ok(String::from_utf8_lossy(&bytes).into_owned())
}

