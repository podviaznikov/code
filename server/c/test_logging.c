#include <unistd.h>
#include <assert.h>
#include "logging.c"

void *read_thread(void *data) {
    size_t buf_len = *((size_t *)data);
    char buf[buf_len];
    for (;;) {
        usleep(1000000 * 0.1);
        get_log(buf, buf_len);
        printf("Got: '%s'\n", buf);
    }
}

int main() {
    pthread_t rt;
    size_t buf_len = 64;

    char *buf = malloc(LOG_MAX_LEN * 2);
    for (int i = 0; i < LOG_MAX_LEN * 2; i++)
        buf[i] = 'A';

    char *buf2 = malloc(LOG_MAX_LEN * 3);
    assert(put_log(LOG_DEBUG, buf) == LOG_RET_OK);
    assert(get_log(buf2, LOG_MAX_LEN * 3) == LOG_RET_OK);
    assert(strlen(buf2) == LOG_MAX_LEN-1);
    assert(buf2[LOG_MAX_LEN-1] == '\0');
    assert(buf2[LOG_MAX_LEN-2] == 'A');

    assert(put_log(LOG_DEBUG, buf) == LOG_RET_OK);
    assert(alog(LOG_INFO, 3, "Testing %d", 1453) == 3);
    pthread_create(&rt, NULL, read_thread, (void *)(&buf_len));
    assert(put_log(LOG_FATAL, NULL) == LOG_RET_OK);

    usleep(1000000 * 0.3);
    assert(put_log(LOG_FATAL, "hi") == LOG_RET_OK);
    usleep(1000000 * 0.1);
    assert(put_log(LOG_ERR, "looks good") == LOG_RET_OK);
    usleep(1000000 * 0.5);

    return 0;
}
