#include <stdint.h>
#include <stdbool.h>
#include <lmdb.h>
    
/*

NOTE: LMDB must not access the same db file twice from the same process.

This segfaults on any get/put :(

*/

struct db_conn {
    MDB_env *env;
};


/////////////////////////
// Synced with Rust

#define DB_OK                   0
#define DB_NOT_FOUND            1
#define DB_ERR                 -1
#define DB_FATAL_ERR           -2


/*
int32_t db_open(char *file_name, struct db_conn** db_conn);
int32_t db_del(struct db_conn *conn, uint8_t *key, uint64_t key_len);
int32_t db_put(struct db_conn *conn, uint8_t *key, uint64_t key_len, uint8_t *data,
               uint32_t data_len);
int32_t db_get(struct db_conn *conn, uint8_t *key, uint64_t key_len, uint8_t **data,
               uint32_t *data_len);
*/
void db_close(struct db_conn *conn);

/////////////////////////

int32_t db_open(char *file_name, struct db_conn **db_conn) {
    // Create env
    struct db_conn *conn = (struct db_conn*)malloc(sizeof(struct db_conn));
    int create_result = mdb_env_create(&(conn->env));
    if (create_result != 0) {
        free(conn);
        return alog(LOG_ERR, DB_ERR, "db_open create: %s", mdb_strerror(create_result));
    }

    // This is a built-in limiter - this (should be) the equivalent of no limit
    int set_result = mdb_env_set_mapsize(conn->env, SIZE_MAX);
    if (set_result != 0) {
        db_close(conn);
        return alog(LOG_ERR, DB_ERR, "db_open set: %s", mdb_strerror(set_result));
    }

    // Open our created env
    int open_result = mdb_env_open(conn->env, file_name, MDB_NOSUBDIR, 0600);
    if (open_result != 0) {
        db_close(conn);
        return alog(LOG_ERR, DB_ERR, "db_open open: %s", mdb_strerror(open_result));
    }

    // We're done!
    *db_conn = conn;
    return DB_OK;
}


void db_close(struct db_conn *conn) {
    mdb_env_close(conn->env);
    free(conn);
}


int32_t db_txn_open(struct db_conn *conn, MDB_txn **txn, MDB_dbi *dbi) {
    // Start transaction
    int txn_result = mdb_txn_begin(conn->env, NULL, 0, txn);
    if (txn_result != 0)
        return alog(LOG_ERR, DB_ERR, "db_txn_open: %s", mdb_strerror(txn_result));

    // Open the dbi
    int dbi_result = mdb_dbi_open(*txn, NULL, 0, dbi);
    if (dbi_result != 0)
        return alog(LOG_ERR, DB_ERR, "db_txn_open dbi: %s", mdb_strerror(dbi_result));

    return DB_OK;
}

int32_t db_txn_close(MDB_txn *txn, bool commit) {
    if (commit) {
        int txn_result = mdb_txn_commit(txn);
        if (txn_result != 0)
            return alog(LOG_ERR, DB_ERR, "db_txn_close: %s", mdb_strerror(txn_result));
    } else {
        mdb_txn_abort(txn);
    }
    return DB_OK;
}


int32_t db_get(struct db_conn *conn, uint8_t *key, size_t key_len, uint8_t **data,
               size_t *data_len)
{
    // Start transaction
    MDB_txn *txn;
    MDB_dbi dbi;
    int txn_result = db_txn_open(conn, &txn, &dbi);
    if (txn_result != DB_OK)
        return txn_result;

    // Do get
    struct MDB_val mv_key = { .mv_data = &key, .mv_size = key_len };
    struct MDB_val mv_data;
    int get_result = mdb_get(txn, dbi, &mv_key, &mv_data);

    // Close the transaction
    db_txn_close(txn, false);

    // Handle errors
    if (get_result == MDB_NOTFOUND) {
        return DB_NOT_FOUND;
    } else if (get_result < 0) {
        return alog(LOG_ERR, DB_ERR, "db_get: %s", mdb_strerror(get_result));
    }
    
    // Return result
    *data = mv_data.mv_data;
    *data_len = mv_data.mv_size;
    return DB_OK;
}


int32_t db_put(struct db_conn *conn, uint8_t *key, size_t key_len, uint8_t *data,
               size_t data_len)
{
    // Start transaction
    MDB_txn *txn;
    MDB_dbi dbi;
    int txn_result = db_txn_open(conn, &txn, &dbi);
    if (txn_result != DB_OK)
        return txn_result;

    mdb_set_relctx(txn, dbi, NULL);

    // Do put
    //struct MDB_val mv_key = { .mv_data = key, .mv_size = key_len };
    //struct MDB_val mv_data = { .mv_data = data, .mv_size = data_len };

    struct MDB_val mv_key = { .mv_data = key, .mv_size = key_len };
    struct MDB_val mv_data = { .mv_data = data, .mv_size = data_len };
    unsigned int woot;
    mdb_dbi_flags(txn, dbi, &woot);
    int put_result = mdb_put(txn, dbi, &mv_key, &mv_data, 0);

    // Handle errors
    if (put_result < 0) {
        db_txn_close(txn, false);
        return alog(LOG_ERR, DB_ERR, "db_put: %s", mdb_strerror(put_result));
    }

    // Commit the transaction
    int txn_close_result = db_txn_close(txn, true);
    if (txn_close_result != DB_OK)
        return txn_close_result;
    
    // We're done!
    return DB_OK;
}


int32_t db_del(struct db_conn *conn, uint8_t *key, size_t key_len) {
    // Start transaction
    MDB_txn *txn;
    MDB_dbi dbi;
    int txn_result = db_txn_open(conn, &txn, &dbi);
    if (txn_result != DB_OK)
        return txn_result;

    // Do del
    struct MDB_val mv_key = { .mv_data = &key, .mv_size = key_len };
    int del_result = mdb_del(txn, dbi, &mv_key, NULL);

    // Handle result
    if (del_result < 0) {
        db_txn_close(txn, false);
        return alog(LOG_ERR, DB_ERR, "db_del: %s", mdb_strerror(del_result));
    } else if (del_result == MDB_NOTFOUND) {
        db_txn_close(txn, false);
        return DB_NOT_FOUND;
    }

    // Commit the transaction
    int txn_close_result = db_txn_close(txn, true);
    if (txn_close_result != DB_OK)
        return txn_close_result;

    return DB_OK;
}


