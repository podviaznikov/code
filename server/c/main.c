#include <stdlib.h>
#include <unistd.h>
#include "aux.c"
#include "assert.h"

//////////////////


void print_logs() {
    char buf[4096];
    while (Head != NULL) {
        get_log((uint8_t*)buf, 4096);
        printf("%s\n", buf);
    }
}

int cmp(const void *lhs, const void *rhs) {
    int f = *((uint8_t*)lhs);
    int s = *((uint8_t*)rhs);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}


void dump_buffer(char *fn, uint8_t *buf, int buf_len) {
    printf("Writing %p: %d\n", buf, buf_len);
    int fd = open(fn, O_CREAT | O_WRONLY | O_TRUNC, 0644);
    write(fd, buf, buf_len);
    close(fd);
}



int main() {
    uint32_t num_socks = 16;
    struct net_sock *socks;
    struct net_sock_status *statuses;
    uint8_t buf[65536];

    assert(net_init(num_socks, &socks, &statuses) == NET_OK);
    print_logs();

    for (;;) {
        assert(net_select(10) == NET_OK);

        for (int sock_id = 0; sock_id < num_socks; sock_id++) {
            struct net_sock *sock = &(socks[sock_id]);
            struct net_sock_status *status = &(statuses[sock_id]);

            if (status->stype == NET_SOCK_TYPE_CLOSED)
                continue;
            if (status->event & NET_EV_NEW_SOCK) {
                printf("New connection!\n");
            }
            if (status->event & NET_EV_NEW_BYTES) {
                int num_bytes = sock->buf_in_live_bytes;
                memcpy(buf, sock->buf_in, num_bytes);

                //char woot[num_bytes + 1];
                //strncpy(woot, (char *)sock->buf_in, num_bytes + 1);
                //printf("'%s'", woot);

                // Echo (sorted)
                qsort(buf, num_bytes-1, 1, cmp);
                net_write(sock_id, buf, num_bytes);

                net_report_consumption(sock_id, num_bytes);
                printf("Got %d bytes!\n", num_bytes);
            }
        }
        print_logs();
    }

    return 0;
}
