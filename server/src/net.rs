use std::{slice, mem};
use std::time::{Duration};
use std::collections::{HashSet, HashMap};
use std::sync::{Arc};
use std::cmp::Ordering;
use bs58::{self};
use prelude::*;
use logging;
use util;
use config::{Config};
use edsu_common::edsu::{self, Username, Value};
use edsu_common::eson::{Eson};
use dynamic_config::{SslCert};
use hub::{ConnId};


// //////////////////////////////////////
// Synced with C


const NET_OK: i32 =                     0;
const NET_BUF_WOULD_OFLOW: i32 =        1;
const NET_OUT_OF_GENERATIONS: i32 =     2;
const NET_ERR: i32 =                    -1;
const NET_FATAL: i32 =                  -2;
const NET_EV_NEW_SOCK: u8 =             0b00000001;
const NET_EV_SOCK_HANDSHAKE_DONE: u8 =  0b00000010;
const NET_EV_NEW_BYTES: u8 =            0b00000100;
const NET_EV_RUST_CLOSE: u8 =           0b00001000;
const NET_SOCK_TYPE_CLOSED: u8 =        0x00;
const NET_SOCK_TYPE_EDSU_WAITING: u8 =  0x01;
const NET_SOCK_TYPE_EDSU: u8 =          0x02;
const NET_SOCK_TYPE_WS_WAITING: u8 =    0x03;
const NET_SOCK_TYPE_WS: u8 =            0x04;

const NET_MAX_DOMAIN_LEN: usize = 256;
const NET_CERT_DECODE_STRUCT_LEN: usize = 2560;
pub const NET_CERT_MAX_CHAIN_LEN: usize = 4;

#[repr(C)]
struct NetSock {
    fd: u32,
    sock_4_not_6: bool,
    sock_addr: [u8; 16],
    buf_in_live_bytes: u16,
    buf_out_live_bytes: u16,
    last_send: u64,
    last_recv: u64,
    cert_generation: u64,
    domain: [u8; NET_MAX_DOMAIN_LEN],
    buf_in: [u8; 0x10000],
    buf_out: [u8; 0x10000],
}

#[repr(C)]
struct NetSockStatus {
    stype: u8,
    event: u8,
    call: u8,
}

#[repr(C)]
struct NetCertItem {
    key_idx: u32,
    key_len: u32,
    num_certs: u32,
    valid: bool,
    domain: [u8; NET_MAX_DOMAIN_LEN],
    chain_array: [u64; NET_CERT_MAX_CHAIN_LEN * 2],
    decode_struct_mem: [u8; NET_CERT_DECODE_STRUCT_LEN],
}


extern {
    fn net_init(num_socks: u32, unix_user: *const u8, unix_group: *const u8,
                socks: *mut *mut NetSock, statuses: *mut *mut NetSockStatus) -> i32;
    fn net_select(tick_us: u32) -> i32;
    fn net_write(sock_id: u32, bytes: *const u8, num_bytes: u16) -> i32;
    fn net_report_consumption(sock_id: u32, num_bytes: u16) -> i32;
    fn net_fini();
    fn net_set_cert_table(cert_table: *const NetCertItem, cert_table_len: u32,
                          cert_heap: *const u8, cert_heap_len: u32) -> i32;
}

// //////////////////////////////////////


// Ordering of NetCertItems
impl Ord for NetCertItem {
    fn cmp(&self, other: &NetCertItem) -> Ordering {
        self.domain.cmp(&other.domain)
    }
}
impl PartialOrd for NetCertItem {
    fn partial_cmp(&self, other: &NetCertItem) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl PartialEq for NetCertItem {
    fn eq(&self, other: &NetCertItem) -> bool {
        self.domain.iter().zip(other.domain.iter()).all(|(a,b)| a == b)
    }
}
impl Eq for NetCertItem {}


pub enum Error {
    WouldOflow,
    AuxError,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct SockId(u32);


pub enum SockType {
    Closed,
    Edsu,
    Ws,
}


pub enum NetEvent {
    SockClosed(SockId),
    SockNew(SockId, Username),
    SockNewBytes(SockId),
}


pub struct Net {
    config: Arc<Config>,
    num_socks: u32,
    net_socks: Box<[NetSock]>,
    net_statuses: Box<[NetSockStatus]>,
}

impl Net {
    pub fn new(config: &Arc<Config>) -> Self {
        // Init the C side
        let mut net_socks_ptr: *mut NetSock = 0 as *mut NetSock;
        let mut net_statuses_ptr: *mut NetSockStatus = 0 as *mut NetSockStatus;
        let result = unsafe {
            net_init(config.num_socks, config.unix_user.as_ptr(), config.unix_group.as_ptr(),
                     &mut net_socks_ptr, &mut net_statuses_ptr)
        };
        if result == NET_FATAL { panic!("got fatal error from net during init"); }

        // Map the NetSocks (NOTE: because I'm converting directly into a boxed slice, it looks
        //                         like I'm avoiding all the dire consequences warned about in
        //                         Vec::from_raw_parts, in that I'm not using an allocator)
        let net_socks_len = config.num_socks as usize;
        let net_socks = unsafe {
            Vec::from_raw_parts(net_socks_ptr, net_socks_len, net_socks_len).into_boxed_slice()
        };
        let net_statuses_len = config.num_socks as usize;
        let net_statuses = unsafe {
            Vec::from_raw_parts(net_statuses_ptr, net_statuses_len, net_statuses_len)
                .into_boxed_slice()
        };

        // Set up our state
        Net {
            config: config.clone(),
            num_socks: config.num_socks,
            net_socks,
            net_statuses,
        }
    }
    pub fn select(&mut self, sock_to_conn: &HashMap<SockId, ConnId>, events: &mut Vec<NetEvent>) {
        // Call the aux lib's select()
        let result = unsafe { net_select(self.config.tick_us) };
        if result == NET_FATAL { panic!("got fatal error from net during net_select()"); }

        // Synchronize our socket state with the C side's
        for (sock_id, net_status) in self.net_statuses.iter_mut().enumerate() {
            let sock_id = SockId(sock_id as u32);

            if net_status.stype == NET_SOCK_TYPE_CLOSED {
                // If it's closed, make sure any associated conn is closed too
                if sock_to_conn.contains_key(&sock_id) {
                    events.push(NetEvent::SockClosed(sock_id));
                } 
            } else {
                if net_status.event & NET_EV_NEW_SOCK != 0 {
                    info!("new socket");
                    // New socket, but still need to do handshake before we can create the Conn
                    // If there was an old socket attached to a conn, that was closed, evidently
                    if sock_to_conn.contains_key(&sock_id) {
                        events.push(NetEvent::SockClosed(sock_id));
                    }
                }
                if net_status.event & NET_EV_SOCK_HANDSHAKE_DONE != 0 {
                    // SSL handshake done, create the Conn
                    // Parse the domain into a username, then push the event
                    info!("SSL handshake complete");
                    let net_sock = &self.net_socks[sock_id.0 as usize];
                    let null_idx = net_sock.domain.iter().position(|c| *c == 0).unwrap();
                    match Username::from_sni_domain(&net_sock.domain[..null_idx]) {
                        Ok(user_to) => events.push(NetEvent::SockNew(sock_id, user_to)),
                        Err(e) => {
                            info!("domain could not be parsed into user_to: {:?}", e);
                            net_status.event |= NET_EV_RUST_CLOSE;
                        },
                    };
                }
                if net_status.event & NET_EV_NEW_BYTES != 0 {
                    // New bytes on a socket
                    events.push(NetEvent::SockNewBytes(sock_id));
                }
            }
        }
    }
    pub fn set_certs(&mut self, ssl_certs: Vec<SslCert>) {
        // Put into a format that is easy for the C side to work with
        let mut heap: Vec<u8> = Vec::new();
        let mut items = Vec::new();

        'next_eson: for cert in ssl_certs {
            // Decode the ESON
            let SslCert { domains, key: key_b58, certs: certs_b58 } = cert;
            if certs_b58.len() > NET_CERT_MAX_CHAIN_LEN {
                err!("bad cert ESON - too many certs: {:?}", domains);
                continue;
            }

            // Decode the b58
            // - Key
            let key = if let Ok(x) = bs58::decode(key_b58.as_bytes()).into_vec() { x } else {
                err!("bad cert ESON - bad b58 encoding for key: {:?}", domains);
                continue;
            };
            // - Certs  TODO: redo if iterator_try_fold becomes ungated
            let mut certs = Vec::with_capacity(certs_b58.len());
            for cb58 in certs_b58 {
                if let Ok(x) = bs58::decode(cb58.as_bytes()).into_vec() {
                    certs.push(x);
                } else {
                    err!("bad cert ESON - bad b58 encoding for key: {:?}", domains);
                    continue 'next_eson;
                };
            }

            // Put into the heap
            let key_idx = heap.len() as u32;
            heap.extend(key.iter());

            let mut cert_idxs = Vec::with_capacity(certs.len());
            for crt in certs.iter() {
                cert_idxs.push(heap.len());
                heap.extend(crt.iter());
            }

            // Create chain array (index of the cert followed by its length)
            let mut chain_array = [0; NET_CERT_MAX_CHAIN_LEN * 2];
            for (i, crt) in certs.iter().enumerate() {
                chain_array[i*2] = cert_idxs[i] as u64;
                chain_array[i*2+1] = crt.len() as u64;
            }

            // Create an item for each domain
            for domain in domains {
                if domain.as_bytes().len() >= NET_MAX_DOMAIN_LEN {
                    err!("bad cert ESON - domain too long: {:?}", domain);
                    continue 'next_eson;
                }

                let mut item = NetCertItem {
                    key_idx,
                    key_len: key.len() as u32,
                    num_certs: certs.len() as u32,
                    valid: false,
                    domain: [0; NET_MAX_DOMAIN_LEN],
                    chain_array: chain_array.clone(),
                    decode_struct_mem: [0; NET_CERT_DECODE_STRUCT_LEN],
                };
                // Fill in domain
                // - Knock off the * if it's a wildcard
                let domain_bytes = if domain.as_str().starts_with('*') {
                    &domain.as_bytes()[1..]
                } else {
                    domain.as_bytes()
                };

                for (dst, src) in item.domain.iter_mut().zip(domain_bytes.iter()) {
                    *dst = *src;
                }
                items.push(item);
            }
        }

        // Sort the items (they're going to be looked up using a binary search)
        items.sort_unstable();

        // Call the C code
        let result = unsafe {
            net_set_cert_table(items.as_ptr(), items.len() as u32,
                               heap.as_ptr(), heap.len() as u32)
        };
        match result {
            NET_FATAL => panic!("fatal error doing net_set_cert_table()"),
            NET_OUT_OF_GENERATIONS => self.trigger_restart_soft(),
            NET_ERR => alarm!("net_set_cert_table() returned NET_ERR"),
            NET_OK => (),
            _ => panic!(),
        };
    }
    pub fn trigger_restart_soft(&self) {
        // TODO: do this properly
        panic!("\"soft\" restart!");
    }
    pub fn get_in_buf(&self, sock_id: SockId) -> &[u8] {
        let net_sock = &self.net_socks[sock_id.0 as usize];
        &net_sock.buf_in[..net_sock.buf_in_live_bytes as usize]
    }
    pub fn report_consumption(&self, sock_id: SockId, num_bytes: u16) -> Result<(), Error> {
        let result = unsafe { net_report_consumption(sock_id.0, num_bytes) };
        match result {
            NET_FATAL => panic!("fatal error doing net_report_consumption()"),
            NET_ERR => Err(Error::AuxError),
            NET_OK => Ok(()),
            _ => panic!(),
        }
    }
    pub fn write(&self, sock_id: SockId, bytes: &[u8]) -> Result<(), Error> {
        if bytes.len() > 0xffff { panic!() };
        let result = unsafe { net_write(sock_id.0, bytes.as_ptr(), bytes.len() as u16) };
        match result {
            NET_FATAL => panic!("fatal error doing net_write()"),
            NET_BUF_WOULD_OFLOW => Err(Error::WouldOflow),
            NET_ERR => Err(Error::AuxError),
            NET_OK => Ok(()),
            _ => panic!(),
        }
    }
    pub fn close_hard(&mut self, sock_id: SockId) {
        debug!("requesting a hard close: {:?}", sock_id);
        self.net_statuses[sock_id.0 as usize].event |= NET_EV_RUST_CLOSE;
    }
    pub fn sock_type(&self, sock_id: SockId) -> SockType {
        let net_status = &self.net_statuses[sock_id.0 as usize];
        match net_status.stype {
            NET_SOCK_TYPE_CLOSED => SockType::Closed,
            NET_SOCK_TYPE_EDSU => SockType::Edsu,
            NET_SOCK_TYPE_WS => SockType::Ws,
            _ => panic!(),
        }
    }
}

impl Drop for Net {
    fn drop(&mut self) {
        unsafe { net_fini() }
    }
}



