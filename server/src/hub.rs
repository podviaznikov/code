use std::{mem, vec, thread, time, process};
use std::sync::{Arc};
use std::time::{SystemTime, Instant, Duration};
use std::collections::{HashMap, BTreeSet, VecDeque, HashSet};
use std::path::{PathBuf};
use std::sync::{RwLock, mpsc};
use bs58::{self};
use prelude::*;
use prelude::Either::{Right, Left};
use util::{self};
use rand::{self};
use logging::{self, LogLevel};
use edsu_common::edsu::{self, ClientMsg, ServerMsg, Username, Multihash, Block, Value, OobCode,
                        Name, Chain, ChainAlias, ChainItem, ChainSelector, ValueRead, Version};
use edsu_common::eson::{self, Eson};
use edsu_common::block_store::{self};
use dynamic_config::{self, DynamicConfig, UserActive};
use ws::{self};
use net::{self, Net, NetEvent, SockId, SockType};
use threaded::{self, OpId, Op, OpPayload, OpResult, OpResultPayload};
use capabilities::{Capabilities, Role, User};
use config::{Config, Plan};
use db::{self, Db};
use email::{self};


// ////////////////////
// Connection state
// ////////////////////

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct ConnId(u64);


pub struct Conn {
    id: ConnId,
    sock_id: SockId,
    reader: ConnReader,
    writer: ConnWriter,
    user_to: Username,
    user_from: Option<Username>,
    caps: Arc<Capabilities>,
    greeted: bool,
    write_blocked: bool,
    read_closed: bool,
    unexamined_in_buf_bytes: bool,
    queued_ops: VecDeque<QueuedOp>,
    op_manager: OpManager,
    chain_manager: ChainManager,
    sub_manager: SubManager,
}


impl Conn {
    fn new(id: ConnId, sock_id: SockId, reader: ConnReader, writer: ConnWriter,
               user_to: Username)
        -> Self
    {
        Conn {
            id,
            sock_id,
            reader,
            writer,
            user_to,
            user_from: None,
            caps: Arc::new(Capabilities::for_anon()),
            greeted: false,
            write_blocked: false,
            read_closed: false,
            unexamined_in_buf_bytes: false,
            queued_ops: VecDeque::new(),
            op_manager: OpManager::new(),
            chain_manager: ChainManager::new(),
            sub_manager: SubManager::new(),
        }
    }
    fn tick(&mut self, op_results: &mut Vec<OpResult>, now: SystemTime) {
        self.op_manager.tick(op_results, self.id, now);
    }
}


pub enum ConnReader {
    Edsu(edsu::Parser),
    Ws((Option<ws::ParserState>, edsu::Parser)),
}


impl ConnReader {
    pub fn new(stype: SockType) -> Self {
        match stype {
            SockType::Closed => unreachable!(),
            SockType::Edsu => ConnReader::Edsu(edsu::Parser::new()),
            SockType::Ws => ConnReader::Ws((None, edsu::Parser::new())),
        }
    }
}


#[derive(Clone, Copy)]
pub enum ConnWriter {
    None,
    Ws(bool),
}



// ////////////////////
// User state
// ////////////////////

struct UserState {
    db_arc: Arc<RwLock<Db>>,
    arc: Arc<UserStateArc>,
    gc: GcState,
    secret: SecretState,
    limiter: LimiterState,
    active: UserActive,
}


impl UserState {
    pub fn new(config: &Arc<Config>, user_to: &Username, arc: UserStateArc, secret: Option<Value>)
        -> Self
    {
        // Scatter the GC last_run across the period so that unused accounts don't bunch up
        let random_gc_delta = (rand::random::<f64>() * GC_PERIODIC_S as f64).floor() as u64;

        let db_arc = Arc::new(RwLock::new(Db::new(&config.db_root, user_to)));
        UserState {
            db_arc,
            arc: Arc::new(arc),
            gc: GcState {
                last_run: Instant::now() - Duration::new(random_gc_delta, 0),
                bytes_written_since: 0,
                bytes_used: 0,
                bytes_last_collected: 0,
                limiter_requested: None,
            },
            secret: SecretState { secret, heat: 0.0, last_fail: Instant::now() },
            limiter: LimiterState {
                bw_bytes_since: 0,
                bw_usages: VecDeque::new(),
                disk_usage: DiskUsage::Ok,
                email_warning_sent: Instant::now() - Duration::new(LIMITER_EMAIL_PERIOD_S, 0),
                email_full_sent: Instant::now() - Duration::new(LIMITER_EMAIL_PERIOD_S, 0),
                op_modulus: 1,
                plan: None,
            },
            active: UserActive::Deactivated,
        }
    }
    pub fn update_arc(&mut self, arc: UserStateArc) {
        self.arc = Arc::new(arc);
    }
}


pub struct UserStateArc {
    pub user_to: Username,
    pub email: String,
}


pub struct SecretState {
    secret: Option<Value>,
    heat: f64,
    last_fail: Instant,
}


struct GcState {
    last_run: Instant,
    bytes_written_since: u64,
    bytes_used: u64,
    bytes_last_collected: u64,
    limiter_requested: Option<Instant>,
}


#[derive(Clone, Copy)]
enum DiskUsage {
    Ok,
    Warning,
    Full,
}


struct LimiterState {
    bw_bytes_since: u64,
    bw_usages: VecDeque<(Instant, u64)>,
    disk_usage: DiskUsage,
    email_warning_sent: Instant,
    email_full_sent: Instant,
    op_modulus: u64,
    plan: Option<Plan>,
}


// ////////////////////
// Op manager
// ////////////////////

struct OpManager {
    cur_op_id: u64,
    pub in_progress: Option<OpInProgress>,
}

// impl OpManager is with the rest of the code below

pub struct OpInProgress {
    pub expires: SystemTime,
    pub channel: Value,
    pub op_id: OpId,
    pub kind: OpKind,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug, Hash)]
pub enum OpKind {
    Msg,
    Sub,
    Chain,
}

struct QueuedOp {
    expires: SystemTime,
    channel: Value,
    payload: OpPayload
}


// ////////////////////
// Thread manager
// ////////////////////

struct ThreadState {
    send: mpsc::Sender<Op>,
    busy: bool,
}

struct ThreadManager {
    config: Arc<Config>,
    op_results: Vec<OpResult>,
    recv: mpsc::Receiver<(u32, OpResult)>,
    states: Vec<ThreadState>,
}

impl ThreadManager {
    fn new(config: Arc<Config>) -> Self {
        // Open the thread-to-manager channel
        let (send, recv) = mpsc::channel();

        // Spawn threads
        let mut states = Vec::with_capacity(config.num_threads as usize);
        for thread_num in 0..config.num_threads {
            // It's probably a little heavy to use a channel for this (only one producer)
            let thread_to_manager_send = send.clone();
            let (manager_to_thread_send, manager_to_thread_recv) = mpsc::channel();
            let thread_config = config.clone();

            thread::spawn(move || {
                loop {
                    let op = manager_to_thread_recv.recv().unwrap();
                    let op_result = threaded::handle_op(&thread_config, op);
                    thread_to_manager_send.send((thread_num, op_result)).unwrap();
                }
            });

            // Store the thread's state
            states.push(ThreadState {
                send: manager_to_thread_send,
                busy: false,
            });
        }

        // Set up our state
        ThreadManager {
            config,
            op_results: Vec::new(),
            recv,
            states,
        }
    }
    fn is_full(&self) -> bool { self.states.iter().all(|x| x.busy) }
    fn start_op(&mut self, op: Op) {
        // NOTE: You must call is_full first!
        let thread_num = self.states.iter().position(|x| !x.busy).unwrap();
        self.states[thread_num].busy = true;
        self.states[thread_num].send.send(op).unwrap_or_else(|_| {
            // Thread is incommunicado, it's been marked as busy above, but log
            alert!("thread died: {}", thread_num);
        });
    }
    fn drain_results(&mut self) -> vec::Drain<OpResult> {
        loop {
            match self.recv.try_recv() {
                Ok((thread_num, op_result)) => {
                    self.states[thread_num as usize].busy = false;
                    self.op_results.push(op_result);
                },
                Err(mpsc::TryRecvError::Empty) => {
                    return self.op_results.drain(..);
                },
                _ => panic!("thread channel died"),
            }
        }
    }
}



// ////////////////////
// Chain manager
// ////////////////////

struct ChainManager {
    chains: VecDeque<(SystemTime, Chain)>,
    chain_sent: Vec<(SystemTime, Multihash)>,
}

// impl ChainManager is with the rest of the code below


// ////////////////////
// Subs
// ////////////////////

struct SubManager {
    cur_id: u64,
    subs: HashMap<Name, Sub>,
    notify_queue: VecDeque<QueuedOp>,
    get_queue: VecDeque<QueuedOp>,
}

// impl SubManager is with the rest of the code below

struct Sub {
    hash: Option<Option<Multihash>>,
    channel: Value,
    once: bool,
    expires: Option<SystemTime>,
    chain: Option<Chain>,
    previously_permitted: bool,
    id: SubId,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub struct SubId(u64);


// ////////////////////
// Server state
// ////////////////////

struct ServerState {
    ssl_cert_block_hash: Option<Multihash>,
}


impl ServerState {
    fn new() -> Self {
        ServerState {
            ssl_cert_block_hash: None,
        }
    }
}


// ////////////////////
// Tasks
// ////////////////////

pub enum Task {
    Delayed {
        when: SystemTime,
        task: Box<Task>,
    },
    Log {
        level: LogLevel,
        msg: String,
    },
    Panic(String),
    ConnCloseHard(ConnId),
    SendMsg(ConnId, ServerMsg),
    SendBytes(ConnId, Vec<u8>, bool),
    RefreshDynamicConfig {
        conn_id: ConnId,
        channel: Value,
    },
}


// ////////////////////
// Functions
// ////////////////////

pub fn run(config: Arc<Config>) {
    // State
    let mut cur_conn_id = 1; // Incremented before use, but need to be certain that 0 is never used
    let mut tick_num = 1u64;
    let mut conns = HashMap::new();
    let mut sock_to_conn = HashMap::new();
    let mut net_events = Vec::new();
    let mut tasks = Vec::new();
    let mut tasks_restack = Vec::new();
    let mut user_states = HashMap::new();
    let mut op_results = Vec::new();
    let mut server_state = ServerState::new();
    let mut name_changes = Vec::new();
    let mut thread_manager = ThreadManager::new(config.clone());
    let mut gc_running = false;
    
    // Intialize the network server and drop privileges
    let mut net = net::Net::new(&config);

    // Set up the user states, SSL certs, etc.
    let dc_ok = refresh_dynamic_config(&config, &mut net, &mut server_state, &mut user_states);
    if let Err(e) = dc_ok {
        alarm!("dynamic config error, shutting down: {:?}", e);
        return;
    }

    // Main loop
    loop {
        // Generate NetEvents from network activity
        net.select(&sock_to_conn, &mut net_events);

        // Generate ClientMsgs from NetEvents (as well housekeeping new and closed sockets)
        for nev in net_events.drain(..) {
            handle_net_event(&config, &mut net, &mut cur_conn_id, &mut conns, &mut sock_to_conn,
                             &mut tasks, &mut user_states, nev);
        }

        // Start op threads
        for conn in conns.values_mut() {
            if let Some(us) = user_states.get_mut(&conn.user_to) {
                // We might have some conns with data waiting to be parsed (we were busy previously
                // with another op - check them now because we might get an op out of it
                if conn.unexamined_in_buf_bytes {
                    parse_new_bytes(&config, &mut net, conn, us, &mut tasks);
                }

                // In any case, give the OpManager a chance to start one up
                conn.op_manager.start_op(&mut thread_manager, us, &mut conn.queued_ops,
                                         &mut conn.sub_manager, &mut conn.chain_manager,
                                         conn.id);
            } else {
                continue;
            }
        }

        // Handle the op results
        for op_result in thread_manager.drain_results().chain(op_results.drain(..)) {
            handle_op_result(&mut conns, &mut user_states, &mut tasks, &mut name_changes,
                             op_result);
        }

        // Periodically run the GC, limiter, and "tick" Conns (for e.g. timeouts)
        if tick_num % config.gc_tick_ratio as u64 == 0 {
            if !gc_running {
                gc_running = run_gc(&mut thread_manager, &mut user_states);
            }
        }
        if tick_num % config.limiter_tick_ratio as u64 == 0 {
            if !config.plans.is_empty() {
                update_limiters(&config, &mut user_states);
            }
        }
        if tick_num % CONN_TICK_RATIO == 0 {
            let now = SystemTime::now();
            for conn in conns.values_mut() { conn.tick(&mut op_results, now) }
        }

        // Notify subs of name changes
        for conn in conns.values_mut() {
            for &(actor_conn_id, ref name, ref hash) in name_changes.iter() {
                // Don't notify the Conn that made the change
                if conn.id == actor_conn_id {
                    continue;
                }
                if conn.sub_manager.update(&conn.caps, name, hash).is_err() {
                    // If something went wrong (probably blew a limit), drop the subs
                    conn.sub_manager.drop(&mut tasks, conn.id);
                    break;
                }
            }
        }
        name_changes.clear();

        // Handle the tasks
        for task in tasks.drain(..) {
            handle_task(&config, &mut net, &mut conns, &mut tasks_restack, &mut server_state,
                        &mut user_states, task);
        }
        mem::swap(&mut tasks, &mut tasks_restack);

        tick_num += 1;
    }
}


fn handle_net_event(config: &Arc<Config>, net: &mut net::Net, cur_conn_id: &mut u64,
                    conns: &mut HashMap<ConnId, Conn>, sock_to_conn: &mut HashMap<SockId, ConnId>,
                    tasks: &mut Vec<Task>, user_states: &mut HashMap<Username, UserState>,
                    nev: NetEvent)
{
    match nev {
        NetEvent::SockClosed(sock_id) => {
            // Remove the Conn and the sock_id from the mapping
            let conn_id = sock_to_conn.remove(&sock_id).unwrap();
            conns.remove(&conn_id);
            debug!("sock closed: {:?} {:?}", conn_id, sock_id);
        },
        NetEvent::SockNew(sock_id, user_to) => {
            // Create a new Conn value
            *cur_conn_id += 1;
            let conn_id = ConnId(*cur_conn_id);
            let reader = ConnReader::new(net.sock_type(sock_id));
            let writer = match net.sock_type(sock_id) {
                SockType::Ws => ConnWriter::Ws(false),
                _ => ConnWriter::None,
            };
            let mut conn = Conn::new(conn_id, sock_id, reader, writer, user_to);

            // If the user doesn't exist, say so and and exit
            let found = if let Some(us) = user_states.get(&conn.user_to) {
                match us.active {
                    UserActive::Active => true,
                    UserActive::Redirect(_, _) => true,
                    UserActive::Deactivated => false,
                }
            } else {
                false
            };
            if !found {
                info!("user not found: {:?}", conn.user_to);
                match net.sock_type(sock_id) {
                    SockType::Ws => {
                        const HTTP_NOT_FOUND: &'static [u8] = b"HTTP/1.1 404 Not Found\r\n\
                                                                Content-Type: text/plain\r\n\
                                                                Content-Length: 18\r\n\
                                                                Connection: close\r\n\r\n\
                                                                404 User not found";
                        tasks.push(Task::SendBytes(conn_id, HTTP_NOT_FOUND.to_vec(), false));
                        conn.read_closed = true;
                        tasks.push(Task::Delayed {
                            when: util::seconds_from_now(SOFT_CLOSE_TIMEOUT_S),
                            task: Box::new(Task::ConnCloseHard(conn_id)),
                        });
                    },
                    _ => {
                        tasks.push(Task::SendMsg(conn_id, ServerMsg::Oob {
                            channel: util::default_channel(),
                            close_connection: Some(true),
                            code: OobCode::NotFound,
                            retry_delay_ms: None,
                            payload: util::oob_msg("user not found"),
                            encoding: None,
                        }));
                    },
                }
            }

            // Put it in our Conns collection and the sock->conn ID mapping
            conns.insert(conn_id, conn);
            sock_to_conn.insert(sock_id, conn_id);

            debug!("new sock: {:?} {:?}", conn_id, sock_id);
        },
        NetEvent::SockNewBytes(sock_id) => {
            // Grab the conn, the in buffer, and set up some state
            let conn_id = *sock_to_conn.get(&sock_id).unwrap();
            let conn = conns.get_mut(&conn_id).unwrap();
            conn.unexamined_in_buf_bytes = true;

            if let Some(us) = user_states.get_mut(&conn.user_to) {
                parse_new_bytes(config, net, conn, us, tasks)
            } else {
                alert!("user state not found: {:?} {:?}", conn_id, conn.user_to);
                tasks.push(Task::SendMsg(conn_id, util::oob_server_err()));
            }
        },
    }
}


fn handle_task(config: &Arc<Config>, net: &mut Net, conns: &mut HashMap<ConnId, Conn>,
               tasks_restack: &mut Vec<Task>, server_state: &mut ServerState,
               user_states: &mut HashMap<Username, UserState>, task: Task)
{
    match task {
        Task::Panic(msg) => panic!("panic task: {}", msg),
        Task::Log { level, msg } => logging::log(level, &msg, &"(log task)", 0),
        Task::Delayed { when, task: delayed_task } => {
            if SystemTime::now() > when {
                tasks_restack.push(*delayed_task);
            } else {
                tasks_restack.push(Task::Delayed { when, task: delayed_task });
            }
        },
        Task::ConnCloseHard(conn_id) => {
            if let Some(conn) = conns.get(&conn_id) {
                net.close_hard(conn.sock_id);
            }
        },
        Task::SendMsg(conn_id, msg) => {
            // If it's an OOB with a close connection, close read and set a timer for a hard close
            if let ServerMsg::Oob { close_connection, .. } = &msg {
                if close_connection == &Some(true) {
                    if let Some(conn) = conns.get_mut(&conn_id) {
                        // Close the read half of the socket
                        conn.read_closed = true;
                    } else {
                        info!("SendMsg for non-existant conn_id");
                        return;
                    }
                    tasks_restack.push(Task::Delayed {
                        when: util::seconds_from_now(SOFT_CLOSE_TIMEOUT_S),
                        task: Box::new(Task::ConnCloseHard(conn_id)),
                    });
                }
            }

            // Convert to bytes then recurse as SendBytes
            let bytes = msg.to_eson_bytes();
            handle_task(config, net, conns, tasks_restack, server_state, user_states,
                        Task::SendBytes(conn_id, bytes, false));
        },
        Task::SendBytes(conn_id, bytes, converted) => {
            // Grab the sock ID
            let (sock_id, writer) = if let Some(c) = conns.get(&conn_id) {
                (c.sock_id, c.writer)
            } else {
                info!("SendBytes for non-existant conn_id: {:?}", conn_id);
                return;
            };

            // Convert to an encapsulating protocol if we need to
            let converted_bytes = if let ConnWriter::Ws(upgraded) = writer {
                if !converted && upgraded {
                    ws::encapsulate(bytes)
                } else {
                    bytes
                }
            } else {
                bytes
            };

            // Double-check to make sure it's not too big to send
            if converted_bytes.len() > 0xffff {
                err!("encapsulated SendBytes for conn {:?} is bigger than out buffer - dropping \
                     and closing", conn_id);
                handle_task(config, net, conns, tasks_restack, server_state, user_states,
                            Task::SendMsg(conn_id, util::oob_server_err()));
            }

            // Send, or restack, or error out
            let converted_bytes_len = converted_bytes.len();
            let write_blocked = match net.write(sock_id, &converted_bytes) {
                Ok(_) => false,
                Err(net::Error::WouldOflow) => {
                    // Restack
                    tasks_restack.push(Task::SendBytes(conn_id, converted_bytes, true));
                    true
                },
                _ => {
                    // Error
                    err!("aux error while SendBytes {:?} - closing", conn_id);
                    handle_task(config, net, conns, tasks_restack, server_state, user_states,
                                Task::SendMsg(conn_id, util::oob_server_err()));
                    true
                }
            };
            let conn = conns.get_mut(&conn_id).unwrap();
            conn.write_blocked = write_blocked;

            // Let the limiter know how many bytes we wrote
            if !write_blocked {
                if let Some(us) = user_states.get_mut(&conn.user_to) {
                    us.limiter.bw_bytes_since += converted_bytes_len as u64;
                }
            }
        },
        Task::RefreshDynamicConfig { conn_id, channel } => {
            if let Err(e) = refresh_dynamic_config(config, net, server_state, user_states) {
                tasks_restack.push(Task::SendMsg(conn_id, ServerMsg::Oob {
                    channel,
                    close_connection: Some(true),
                    code: edsu::OobCode::ServerError,
                    encoding: None,
                    payload: util::oob_msg(&format!("{:?}", e)),
                    retry_delay_ms: None,
                }));
            } else {
                // Send back an OK
                tasks_restack.push(Task::SendMsg(conn_id, ServerMsg::Name {
                    channel,
                    name: name!(NAME_REFRESH_DYNAMIC_CONFIG),
                    hash: edsu::from_bytes(NULL_HASH.as_bytes()).unwrap(),
                }));
                info!("dynamic config reloaded");
            }
        },
    }
}


fn handle_message(config: &Arc<Config>, conn: &mut Conn, us: &mut UserState, tasks: &mut Vec<Task>,
                  msg: ClientMsg)
{
    let channel = msg.get_channel();
    match msg {

        // BlockPut
        ClientMsg::BlockPut { payload, .. } => {
            debug!("{:?} block-put", conn.id);
            // Get permission
            if !conn.caps.block_put() {
                tasks.push(Task::SendMsg(conn.id, util::oob_perm_denied(channel)));
                return;
            }

            // Parse block
            let block = match Block::from_bytes(payload) {
                Ok(b) => b,
                Err(e) => {
                    tasks.push(Task::SendMsg(conn.id, util::edsu_err_to_oob(e, Some(channel),
                                             false)));
                    return;
                },
            };

            // Get hash for it
            let hash = Multihash::from_source(block.as_bytes());

            // Stack the Op
            let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
            let payload = OpPayload::BlockPut { hash, block };
            conn.queued_ops.push_back(QueuedOp { expires, channel, payload });
        },

        // BlockGet
        ClientMsg::BlockGet { hash, chain, .. } => {
            debug!("{:?} block-get", conn.id);
            // No permission required

            // We can short circuit if it's the null hash (also, guarantees its existence)
            if hash.as_bytes() == NULL_HASH.as_bytes() {
                tasks.push(Task::SendMsg(conn.id, ServerMsg::Block {
                    channel,
                    encoding: None,
                    hash: hash,
                    payload: b"~\n".to_vec(),
                }));
                return;
            } 
            
            // Stack the Op
            let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
            let payload = OpPayload::BlockGet { hash, chain, from_chain: false };
            conn.queued_ops.push_back(QueuedOp { expires, channel, payload });
        },

        // NamePut
        ClientMsg::NamePut { existing_hash, name, hash: maybe_hash, .. } => {
            debug!("{:?} name-put", conn.id);
            if let Some(hash) = maybe_hash {
                // Name put
                // Get permission to even try to put a name (there's further checks later)
                if !conn.caps.name_put(&name) {
                    tasks.push(Task::SendMsg(conn.id, util::oob_perm_denied(channel)));
                    return;
                }

                // Stack the Op
                let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
                let payload = OpPayload::NamePut {
                    caps: conn.caps.clone(),
                    name,
                    existing_hash,
                    hash,
                };
                conn.queued_ops.push_back(QueuedOp { expires, channel, payload });
            } else {
                // Name delete
                // Get permission
                if !conn.caps.name_del(&name) {
                    tasks.push(Task::SendMsg(conn.id, util::oob_perm_denied(channel)));
                    return;
                }

                // NameDel requires an existing-hash
                let eh = if let Some(x) = existing_hash { x } else {
                    tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                        channel: channel,
                        close_connection: None,
                        code: OobCode::InvalidInput,
                        retry_delay_ms: None,
                        payload: util::oob_msg("name-put without block (i.e. delete) \
                                                requires 'existing-hash'"),
                        encoding: None,
                    }));
                    return;
                };

                // Stack the Op
                let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
                let payload = OpPayload::NameDel {
                    caps: conn.caps.clone(),
                    name,
                    existing_hash: eh
                };
                conn.queued_ops.push_back(QueuedOp { expires, channel, payload });
            }
        },

        // NameGet
        ClientMsg::NameGet { name, chain, .. } => {
            debug!("{:?} name-get", conn.id);
            // Get permission
            if !conn.caps.name_get(&name) {
                tasks.push(Task::SendMsg(conn.id, util::oob_perm_denied(channel)));
                return;
            }

            // Special case: config triggering a reload of the dynamic config
            if conn.caps.get_role() == Role::Config &&
               name.as_str() == NAME_REFRESH_DYNAMIC_CONFIG
            {
                tasks.push(Task::RefreshDynamicConfig { conn_id: conn.id, channel });
                return;
            }

            // Special case: asking the server to shut down TODO: more gracefully
            if conn.caps.get_role() == Role::Config && name.as_str() == NAME_SERVER_EXIT {
                process::exit(REQUESTED_SERVER_EXIT_CODE);
            }
   
            // Stack the Op
            let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
            let payload = OpPayload::NameGet { caps: conn.caps.clone(), name, chain };
            conn.queued_ops.push_back(QueuedOp { expires, channel, payload });
        },
        
        // Ping
        ClientMsg::Ping { .. } => {
            debug!("{:?} ping", conn.id);
            // No permission required

            // Respond immediately with Pong
            tasks.push(Task::SendMsg(conn.id, ServerMsg::Pong {
                channel,
            }));
        },

        // Pong
        ClientMsg::Pong { .. } => {
            debug!("{:?} pong", conn.id);
            // NOP - the purpose of the ping was to make something (anything) update the "last
            // network activity" counter in Net
        },

        // SubPut
        ClientMsg::SubPut { name, existing_hash, once: maybe_once, expires, chain, .. } => {
            debug!("{:?} sub-put", conn.id);
            // No permission required - things may have changed by the time the notification
            // happens, and it's always checked then

            // Convert some values to a more useful format
            let once = maybe_once.unwrap_or(false);
            let expires_parsed = expires.map(|x| util::unix_to_system_time(x));
            // - We can't distinguish between the client not knowing what the hash is, and
            //   knowing that it's None - c'est la vie
            let sub_hash = if let Some(x) = existing_hash { Some(Some(x)) } else { None };

            let sub_id = conn.sub_manager.get_id();
            let result = conn.sub_manager.put(conn.caps.clone(), name.clone(), Sub {
                hash: sub_hash,
                channel: channel.clone(),
                once,
                expires: expires_parsed,
                chain,
                previously_permitted: false,
                id: sub_id,
            });
            if let Err((debug, code)) = result {
                tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                    channel: channel,
                    close_connection: None,
                    code,
                    retry_delay_ms: None,
                    payload: util::oob_msg(debug),
                    encoding: None,
                }));
            } else {
                tasks.push(Task::SendMsg(conn.id, ServerMsg::Ok_ {
                    channel,
                    hash: None,
                }));
            }
        },

        // SubClear
        ClientMsg::SubClear { .. } => {
            debug!("{:?} sub-clear", conn.id);
            // No permission required
            
            conn.sub_manager.clear();

            // Done, send OK
            tasks.push(Task::SendMsg(conn.id, ServerMsg::Ok_ {
                channel,
                hash: None,
            }));
        },

        // NameAppend
        ClientMsg::NameAppend { name, hash, .. } => {
            debug!("{:?} name-append", conn.id);
            // Get permission to proceed (based on what we know at this point)
            if !conn.caps.name_append(&name) {
                tasks.push(Task::SendMsg(conn.id, util::oob_perm_denied(channel)));
                return;
            }

            let user_from = if let Some(ref u) = conn.user_from { u.clone() } else {
                if conn.caps.get_user() == &User::Owner {
                    conn.user_to.clone()
                } else {
                    tasks.push(Task::SendMsg(conn.id,
                        util::edsu_err_to_oob(edsu::Error::PermissionDenied, Some(channel),
                                              false)));
                    return;
                }
            };
            
            // Stack Op
            let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
            let payload = OpPayload::NameAppend {
                append_name: name,
                user_from,
                hash
            };
            conn.queued_ops.push_back(QueuedOp { expires, channel, payload });
        },

        // Hello
        ClientMsg::Hello { versions, token, visitor_username, secret, .. } => {
            debug!("{:?} hello", conn.id);

            let greet = |tasks_: &mut Vec<Task>, conn_: &mut Conn, channel_: &Value| { 
                tasks_.push(Task::SendMsg(conn_.id, ServerMsg::Hello {
                    channel: channel_.clone(),
                    version: util::protocol_version(),
                    encodings: None,
                }));
                conn_.greeted = true;
            };

            if !versions.contains(&util::protocol_version()) {
                alert!("client doesn't support our protocol version: {:?}",
                       util::protocol_version());
                tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                    channel: channel.clone(),
                    close_connection: Some(true),
                    code: OobCode::ServerError,
                    retry_delay_ms: None,
                    encoding: None,
                    payload: util::oob_msg("no supported protocol version found"),
                }));
                return;
            }

            // Before anything, check if there's a redirect/alias
            if let UserActive::Redirect(username, is_alias) = &us.active {
                greet(tasks, conn, &channel);
                let payload = Some(format!("username {}\nalias {}\n\n",
                                           username.to_string(), is_alias).as_bytes().to_vec());
                tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                    channel: channel.clone(),
                    close_connection: Some(true),
                    code: OobCode::Redirect,
                    retry_delay_ms: None,
                    encoding: None,
                    payload,
                }));
                return;
            }

            if let Some(enc_sk) = secret {
                // Retry progressive delay (heat == number of seconds they need to wait before
                // retry)
                let mut retry_delay_ms = None;
                let since_last_fail = (Instant::now() - us.secret.last_fail).as_secs();
                let wait_delta = since_last_fail as i64 - us.secret.heat.floor() as i64;
                if wait_delta < 0 {
                    retry_delay_ms = Some((wait_delta as f64 * -1000.0).floor() as u32);
                }
                us.secret.heat = (us.secret.heat - (since_last_fail as f64 / 1400.0)
                                 .powi(10)).max(1.1).powi(2).min(3600.0);
                let next_delay = (us.secret.heat * 1000.0).floor() as u32;

                // See if it failed (or they haven't waited long enough)
                if us.secret.secret.is_none() || retry_delay_ms.is_some() {
                    if retry_delay_ms.is_none() {
                        retry_delay_ms = Some(next_delay);
                    }
                    tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                        channel: channel.clone(),
                        close_connection: Some(true),
                        code: edsu::OobCode::AuthenticationError,
                        encoding: None,
                        payload: None,
                        retry_delay_ms,
                    }));
                    return;
                }

                if enc_sk.len() != SECRET_VALUE_LEN {
                    tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                        channel: channel,
                        close_connection: Some(true),
                        code: OobCode::InvalidInput,
                        retry_delay_ms: None,
                        payload: util::oob_msg(
                            &format!("hello secret ESON value must be exactly {} bytes long - \
                                      pad with spaces", SECRET_VALUE_LEN)),
                        encoding: None,
                    }));
                    return;
                };

                // Decode the secret
                let trimmed = enc_sk.as_str().trim_right();
                let to_test = if let Ok(x) = util::eson_utf8_decode(trimmed) { x } else {
                    tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                        channel: channel,
                        close_connection: Some(true),
                        code: OobCode::InvalidInput,
                        retry_delay_ms: None,
                        payload: util::oob_msg("hello secrets must be ESON-encoded UTF-8"),
                        encoding: None,
                    }));
                    return;
                };

                // Kick off the secret verification
                let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
                let payload = OpPayload::SecretTest {
                    to_test: Value::from_bytes(to_test.as_bytes()).unwrap(),
                    secret: us.secret.secret.as_ref().unwrap().clone(),
                    retry_delay: next_delay
                };
                conn.queued_ops.push_back(QueuedOp { expires, channel: channel.clone(), payload });
                greet(tasks, conn, &channel);
            } else if visitor_username.is_some() && token.is_none() {
                // visitor-* must be a pair
                tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                    channel: channel.clone(),
                    close_connection: Some(true),
                    code: edsu::OobCode::InvalidInput,
                    encoding: None,
                    payload: util::oob_msg("visitor-username needs a token"),
                    retry_delay_ms: None,
                }));
                return;
            } else if let (&Some(ref tk), &Some(ref user_from)) = (&token, &visitor_username) {
                // Visitor

                // Validate token
                if tk.validate().is_err() {
                    tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                        channel: channel,
                        close_connection: Some(true),
                        code: OobCode::InvalidInput,
                        retry_delay_ms: None,
                        payload: util::oob_msg("invalid token"),
                        encoding: None,
                    }));
                    return;
                }

                // Stack Op
                let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
                let payload = OpPayload::AuthVisitor {
                    token: tk.clone(),
                    user_from: user_from.clone()
                };
                conn.queued_ops.push_back(QueuedOp { expires, channel: channel.clone(), payload });

                // Greet in the meantime
                greet(tasks, conn, &channel);
            } else if let &Some(ref tk) = &token {
                // Owner

                // Validate token
                if tk.validate().is_err() {
                    tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                        channel: channel,
                        close_connection: Some(true),
                        code: OobCode::InvalidInput,
                        retry_delay_ms: None,
                        payload: util::oob_msg("invalid token"),
                        encoding: None,
                    }));
                    return;
                }

                // Stack Op
                let role = if conn.user_to == config.config_user {
                    Role::Config
                } else if conn.user_to == config.server_user {
                    Role::Server
                } else {
                    Role::None
                };
                let expires = util::seconds_from_now(DEFAULT_OP_TIMEOUT_S);
                let payload = OpPayload::AuthOwner { token: tk.clone(), role };
                conn.queued_ops.push_back(QueuedOp { expires, channel: channel.clone(), payload });

                // Greet in the meantime
                greet(tasks, conn, &channel);
            } else {
                // Anon
                greet(tasks, conn, &channel);
            }
        },
    }
}




// Op result
fn handle_op_result(conns: &mut HashMap<ConnId, Conn>,
                    user_states: &mut HashMap<Username, UserState>, tasks: &mut Vec<Task>,
                    name_changes: &mut Vec<(ConnId, Name, Option<Multihash>)>, op_result: OpResult)
{
    let OpResult { conn_id, op_id, result } = op_result;

    // Handle non-connection Ops
    if conn_id == ConnId(0) {
        match result {
            Err(e) => {
                err!("non-connection op resulted in error: {:?}", e);
            },
            Ok(payload) => {
                match payload {
                    OpResultPayload::Gc { username, bytes_used, bytes_collected, run_time } => {
                        let us = if let Some(x) = user_states.get_mut(&username) { x } else {
                            alert!("user state disappeared while GCing: {:?}", username);
                            return;
                        };
                        let gc = &mut us.gc;
                        gc.last_run = Instant::now();
                        gc.bytes_written_since = 0;
                        gc.bytes_used = bytes_used;
                        gc.bytes_last_collected = bytes_collected;
                        gc.limiter_requested = None;
                        info!("GC completed for {:?} in: {:?}", username, run_time);
                    },
                    _ => unreachable!(),
                }
            }
        }
        return;
    }

    // Grab the conn
    let conn = if let Some(x) = conns.get_mut(&conn_id) { x } else {
        info!("dropping OpResult for non-existant conn_id: {:?}", conn_id);
        return;
    };

    // Verify it matches the in_progress and grab the channel
    let channel;
    if let Some(OpInProgress { channel: chl, kind, .. }) = conn.op_manager.complete(op_id) {
        // Special case errors
        if result.is_err() && kind == OpKind::Chain {
            // Errors on chains don't get reported
            return;
        } else if result.is_err() && kind == OpKind::Sub {
            // Sub errors leave us in an unknown state, so panic and drop everything
            conn.sub_manager.drop(tasks, conn.id);
            return;
        }

        channel = chl;
    } else {
        info!("op finished, but in_progress doesn't match (may have timed out)");
        return;
    }

    // Handle the response
    match result {
        Err(e) => tasks.push(Task::SendMsg(conn_id, threaded::err_to_oob(e, channel))),
        Ok(payload) => {
            match payload {

                // BlockPut
                OpResultPayload::BlockPut{ hash, bytes_written } => {
                    // Update the list of recently put blocks (for the GC's reference)
                    let us = if let Some(x) = user_states.get_mut(&conn.user_to) { x } else {
                        alert!("user state not found {:?}", conn.user_to);
                        tasks.push(Task::SendMsg(conn_id, util::oob_server_err()));
                        return;
                    };

                    // Update the total for the GC
                    us.gc.bytes_written_since += bytes_written as u64;

                    // Send the OK
                    tasks.push(Task::SendMsg(conn_id, ServerMsg::Ok_ {
                        channel,
                        hash: Some(hash),
                    }));
                },

                // BlockGet
                OpResultPayload::BlockGet { hash, block, chain: chain_maybe, from_chain } => {
                    // Start a chain if we've got one, now that we've got the root block
                    if let Some(chain) = chain_maybe {
                        conn.chain_manager.push(chain, &hash);
                    }

                    // Update all the chains with this new information
                    conn.chain_manager.update(&hash, &block);

                    // Don't send if we just sent it because of a chain (and therefore the client
                    // wouldn't know about it yet
                    if !from_chain && conn.chain_manager.just_sent(&hash) {
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                            channel,
                            close_connection: None,
                            code: edsu::OobCode::JustSent,
                            encoding: None,
                            payload: None,
                            retry_delay_ms: Some(CHAIN_DURATION_S as u32 * 1000),
                        }));
                    } else {
                        // If it's a chain-initiated block, then it's sent on channel 0
                        let channel_to_send = if from_chain {
                            util::default_channel()
                        } else {
                            channel
                        };
 
                        tasks.push(Task::SendMsg(conn_id, ServerMsg::Block {
                            channel: channel_to_send,
                            encoding: None,
                            hash: hash,
                            payload: block.into_bytes(),
                        }));
                    }
                },

                // NameDel
                OpResultPayload::NameDel(name) => {
                    // Alert any subs
                    name_changes.push((conn_id, name, None));

                    // Send OK
                    tasks.push(Task::SendMsg(conn_id, ServerMsg::Ok_ {
                        channel,
                        hash: None,
                    }));
                },

                // NamePut
                OpResultPayload::NamePut(name, hash) => {
                    // Alert any subs
                    name_changes.push((conn_id, name, Some(hash)));

                    // Send OK
                    tasks.push(Task::SendMsg(conn_id, ServerMsg::Ok_ {
                        channel,
                        hash: None,
                    }));
                },

                // NameGet
                OpResultPayload::NameGet { name, hash, chain: chain_maybe, block } => {
                    // Start a chain if we've got one
                    if let Some(mut chain) = chain_maybe {
                        // Special case: if there's any chain at all, prepend the name block
                        chain.push_front(ChainItem {
                            block: Right(hash.clone()),
                            selector: ChainSelector::Resolved,
                        });
                        conn.chain_manager.push(chain, &hash);
                        if let Some(blk) = block {
                            conn.chain_manager.update(&hash, &blk);
                        }
                    }

                    tasks.push(Task::SendMsg(conn_id, ServerMsg::Name {
                        channel,
                        name,
                        hash,
                    }));
                },

                // Gc (handled above
                OpResultPayload::Gc { .. } => panic!(),

                // NameAppend
                OpResultPayload::NameAppend(name, hash) => {
                    // Alert any subs
                    name_changes.push((conn_id, name, Some(hash)));

                    // Purposely not returning hash of name because there might not be
                    // read permissions
                    tasks.push(Task::SendMsg(conn_id, ServerMsg::Ok_ {
                        channel,
                        hash: None,
                    }));
                },

                // AuthOwner
                OpResultPayload::AuthOwner(maybe_caps) => {
                    if let Some(caps) = maybe_caps {
                        conn.caps = Arc::new(caps);
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Authenticated {
                            channel: util::default_channel(),
                        }));
                        info!("{:?} authenticated as {:?}", conn.id, conn.user_to);
                    } else {
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                            channel: util::default_channel(),
                            close_connection: Some(true),
                            code: edsu::OobCode::AuthenticationError,
                            encoding: None,
                            payload: None,
                            retry_delay_ms: None,
                        }));
                    }
                },

                // AuthVisitor
                OpResultPayload::AuthVisitor(maybe_caps) => {
                    if let Some(caps) = maybe_caps {
                        info!("{:?} authenticated as {:?}", conn.id, caps.get_user());
                        conn.caps = Arc::new(caps);
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Authenticated {
                            channel: util::default_channel(),
                        }));
                    } else {
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                            channel: util::default_channel(),
                            close_connection: Some(true),
                            code: edsu::OobCode::AuthenticationError,
                            encoding: None,
                            payload: None,
                            retry_delay_ms: None,
                        }));
                    }
                },

                // SubNameGet
                OpResultPayload::SubNameGet(name, hash) => {
                    // Alert the sub that requested this
                    if conn.sub_manager.update(&conn.caps, &name, &hash).is_err() {
                        conn.sub_manager.drop(tasks, conn.id);
                    }
                },
                
                // SubPermissionsCheck
                OpResultPayload::SubPermissionsCheck { name, hash: hash_maybe, chain: chain_maybe,
                                                       sub_id, permitted, name_block }
                => {
                    // Record if we got permissions (for an edge case)
                    conn.sub_manager.set_previously_permitted(&name, permitted);
                
                    // Send a notification (if they're allowed to know)
                    if permitted {
                        // Delete the sub if this was a once
                        conn.sub_manager.del_if_once(&name, sub_id);

                        // Send the notification
                        tasks.push(Task::SendMsg(conn_id, ServerMsg::SubNotify {
                            channel,
                            name,
                            hash: hash_maybe.clone(),
                        }));

                        // Start a chain if there is one
                        if let Some(hash) = hash_maybe {
                            if let Some(chain) = chain_maybe {
                                conn.chain_manager.push(chain, &hash);
                                // If we have the name block, we can fill that blank in on the
                                // chain, as well as send it
                                if let Some(block) = name_block {
                                    conn.chain_manager.update(&hash, &block);
                                    tasks.push(Task::SendMsg(conn_id, ServerMsg::Block {
                                        channel: util::default_channel(),
                                        encoding: None,
                                        hash: hash,
                                        payload: block.into_bytes(),
                                    }));
                                }
                            }
                        }
                    }
                },

                // SecretTest
                OpResultPayload::SecretTest { matches, retry_delay } => {
                    if matches {
                        // Success - reset the heat
                        if let Some(us) = user_states.get_mut(&conn.user_to) {
                            us.secret.heat = 0.0;
                        } else {
                            alert!("user state not found {:?}", conn.user_to);
                            tasks.push(Task::SendMsg(conn_id, util::oob_server_err()));
                            return;
                        };

                        // Upgrade the connection caps
                        conn.caps = Arc::new(Capabilities::for_secret_authenticated());

                        // Push the authenticated message
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Authenticated {
                            channel,
                        }));
                        info!("{:?} authenticated using secret as {:?}", conn.id, conn.user_to);
                    } else {
                        tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                            channel,
                            close_connection: Some(true),
                            code: edsu::OobCode::AuthenticationError,
                            encoding: None,
                            payload: None,
                            retry_delay_ms: Some(retry_delay),
                        }));
                        info!("{:?} failed pw authentication for {:?}", conn.id, conn.user_to);
                    }
                },

            }
        }
    }
}



fn parse_new_bytes(config: &Arc<Config>, net: &mut net::Net, conn: &mut Conn, us: &mut UserState,
                   tasks: &mut Vec<Task>)
{
    let in_buf = net.get_in_buf(conn.sock_id);
    
    // If the read has closed (i.e. a soft close) discard the bytes (can't just do a
    // shutdown() because it doesn't play well with SSL
    if conn.read_closed {
        if net.report_consumption(conn.sock_id, in_buf.len() as u16).is_err() {
            err!("net.report_consumption() failed for {:?}, hard closing", conn.id);
            tasks.push(Task::ConnCloseHard(conn.id));
        }
        return;
    }

    // Don't proceed if there's nothing to read, or there's a backlog of Ops
    if in_buf.len() == 0 {
        conn.unexamined_in_buf_bytes = false;
        return;
    } else if !conn.queued_ops.is_empty() {
        return;
    }

    let mut consumed = 0;
    let mut client_msg = None;
    match conn.reader {
        ConnReader::Edsu(ref mut parser) => {
            let mut iter = in_buf.iter().cloned();
            match parser.add_bytes(&mut iter) {
                Ok(None) => (),
                Ok(Some(msg)) => client_msg = Some(msg),
                Err(e) => {
                    info!("edsu parse error: {:?}", e);
                    tasks.push(Task::SendMsg(conn.id, util::edsu_err_to_oob(e, None, true)));
                },
            }
            consumed = in_buf.len() - iter.count();
        },
        ConnReader::Ws((ref mut maybe_parser_state, ref mut edsu_parser)) => {
            if maybe_parser_state.is_none() {
                // Need to handshake first
                match ws::handshake(config, in_buf) {
                    Ok(None) => (), // Need more bytes - do nothing until then
                    Ok(Some((http_resp, is_ws))) => {
                        // Parsed OK, respond with either the HTTP upgrade or the auth page
                        tasks.push(Task::SendBytes(conn.id, http_resp, true));
                        consumed = in_buf.len();

                        if is_ws {
                            // Set state for the next time around
                            *maybe_parser_state = Some(ws::ParserState::new());
                            conn.writer = ConnWriter::Ws(true);
                        } else {
                            // "Soft close" the conn - we've sent the HTTP data so we're done
                            conn.read_closed = true;
                            tasks.push(Task::Delayed {
                                when: util::seconds_from_now(SOFT_CLOSE_TIMEOUT_S),
                                task: Box::new(Task::ConnCloseHard(conn.id)),
                            });
                        }
                    },
                    Err(ws::Error::Http(http_resp)) => {
                        tasks.push(Task::SendBytes(conn.id, http_resp.to_vec(), true));
                        consumed = in_buf.len();
                    },
                    Err(ws::Error::HttpWithClose(http_resp)) => {
                        tasks.push(Task::SendBytes(conn.id, http_resp.to_vec(), true));
                        tasks.push(Task::ConnCloseHard(conn.id));
                        return;
                    },
                }
            } else {
                // Handshake is done, start or continue parsing
                let parser_state = maybe_parser_state.take().unwrap();

                // Create iter and feed it to the Edsu parser
                let mut iter = ws::ParserIter::new(parser_state, in_buf);
                match edsu_parser.add_bytes(&mut iter) {
                    Ok(None) => (),
                    Ok(Some(msg)) => client_msg = Some(msg),
                    Err(e) => {
                        info!("edsu parse error: {:?}", e);
                        tasks.push(Task::SendMsg(conn.id, util::edsu_err_to_oob(e, None, true)));
                    },
                }
                
                // Decompose the iter into the parts we'll need
                let (iter_consumed, resp, new_parser_state) = iter.decompose();

                // Handle response (if any)
                match resp {
                    None => (),
                    Some(ws::ParserResp::Close) => {
                        // Either the client or the iter requested a close connection
                        tasks.push(Task::ConnCloseHard(conn.id));
                        return;
                    },
                    Some(ws::ParserResp::Ping(pong_bytes)) => {
                        // WS ping, respond with a pong
                        tasks.push(Task::SendBytes(conn.id, pong_bytes, false));
                    },
                }

                // Set state for the next time around
                consumed = iter_consumed;
                *maybe_parser_state = Some(new_parser_state);
            }
        },
    }

    // If we got a client message, turn it into an Op/Task
    // However, it's an error to send anything but a Hello as the first message, or Hello twice
    if let Some(msg) = client_msg {
        let is_hello = if let ClientMsg::Hello { .. } = msg { true } else { false };
        if (!is_hello && conn.greeted) || (is_hello && !conn.greeted) {
            handle_message(config, conn, us, tasks, msg);
        } else {
            tasks.push(Task::SendMsg(conn.id, ServerMsg::Oob {
                channel: msg.get_channel(),
                close_connection: Some(true),
                code: edsu::OobCode::InvalidInput,
                encoding: None,
                payload: util::oob_msg("hello message must be the first message, and only once"),
                retry_delay_ms: None,
            }));
        }
    } else {
        // If we don't have a message at this point, that means we've looked at all the bytes and
        // haven't been able to get a message out of it - wait for more bytes
        conn.unexamined_in_buf_bytes = false;
    }

    // Report how many bytes where consumed (and so can be freed)
    assert!(consumed <= 0xffff);
    if let Err(_) = net.report_consumption(conn.sock_id, consumed as u16) {
        tasks.push(Task::SendMsg(conn.id, util::oob_server_err()));
    }

    // Update the limiter
    us.limiter.bw_bytes_since += consumed as u64;
}



fn refresh_dynamic_config(config: &Arc<Config>, net: &mut net::Net, server_state: &mut ServerState,
                          user_states: &mut HashMap<Username, UserState>)
    -> Result<(), dynamic_config::Error>
{
    // We're outside of the system that prevents two mut dbs for a single user concurrently, so
    // we need to make sure none of the constraints aren't violated here (e.g. doing a read on
    // a name that will have side effects, etc).  Straight name reads are atomic (the file is
    // never in an inconsistent state), so we should be good on the non-mut operations front
    let mut db = Db::new(&config.db_root, &config.config_user);

    let config_hash = db.name_get(&name!(NAME_DYNAMIC_CONFIG), true).map_err(|e| match e {
        db::Error::NotFound => block_store::Error::Server(
            "db has not been bootstrapped - see the setup documentation".to_owned()),
        e => block_store::Error::Server(format!("{:?}", e)),
    })?.0;

    // Grab the dynamic config
    let dyn_config = {
        let mut bs = db::BlockStoreDb::new(&mut db);
        DynamicConfig::from_block_store(&mut bs, &config_hash)
    }?;
    
    // User configs
    let mut seen = HashSet::new();
    for uc in dyn_config.user_configs {
        // Prevent dups
        if seen.contains(&uc.username) {
            alarm!("found duplicate username in user configs: {:?}", uc.username);
            continue;
        } else {
            seen.insert(uc.username.clone());
        }

        let arc = UserStateArc {
            user_to: uc.username.clone(),
            email: uc.email.into_string(),
        };
        if user_states.contains_key(&uc.username) {
            user_states.get_mut(&uc.username).unwrap().update_arc(arc);
        } else {
            let us = UserState::new(config, &uc.username, arc, uc.secret.clone());
            user_states.insert(uc.username.clone(), us);
        }
        let us = user_states.get_mut(&uc.username).unwrap();
        us.limiter.plan = if let Some(x) = config.plans.get(uc.plan.as_str()) { Some(x.clone()) } else {
            if uc.plan.as_str() != OMNI_PLAN_NAME {
                alarm!("missing plan, defaulting to unlimited: {:?}", uc.plan.as_str());
            }
            None
        };
        us.active = uc.active;
        us.secret.secret = uc.secret;
    }

    // SSL certs - it's an expensive op (because old certs are stored if any conns are using them),
    // so only do it if there's been a change
    let ssl_cert_block_hash = Some({
        let block = db.block_get(&config_hash).map_err(|e|
            block_store::Error::Server(format!("getting ssl cert root hash{:?}", e)))?;
        let eson = Eson::from_bytes(block.get_contents())?;
        let root_hash_val = eson.get_req_single(dynamic_config::KEY_SSL_CERTS)?;
        edsu::from_bytes(root_hash_val.as_bytes())
    }.map_err(|e| block_store::Error::Server(format!("getting ssl cert root hash{:?}", e)))?);
    if ssl_cert_block_hash != server_state.ssl_cert_block_hash {
        net.set_certs(dyn_config.ssl_certs);
        server_state.ssl_cert_block_hash = ssl_cert_block_hash;
    }

    Ok(())
}



impl OpManager {
    fn new() -> Self {
        OpManager {
            cur_op_id: 0,
            in_progress: None,
        }
    }
    fn start_op(&mut self, thread_manager: &mut ThreadManager, us: &mut UserState,
                queued_ops: &mut VecDeque<QueuedOp>, sub_manager: &mut SubManager,
                chain_manager: &mut ChainManager, conn_id: ConnId)
    {
        // Only one Op at a time
        if self.in_progress.is_some() || thread_manager.is_full() {
            return;
        }

        // Limit if requested
        if us.limiter.op_modulus != 1 {
            // Do this in deciseconds - seconds is a little too chunky
            let s = time::SystemTime::now().duration_since(time::UNIX_EPOCH).unwrap();
            let ds = s.as_secs() * 10 + (s.subsec_nanos() / 100000000) as u64;
            if ds % us.limiter.op_modulus != 0 {
                return;
            }
        }

        // Ops can come from several places; try each in priority order
        let mut op_maybe = None;
        let mut kind = OpKind::Msg;
        if op_maybe.is_none() { kind = OpKind::Sub; op_maybe = sub_manager.get_notify_op() }
        if op_maybe.is_none() { kind = OpKind::Sub; op_maybe = sub_manager.get_name_op() }
        if op_maybe.is_none() { kind = OpKind::Msg; op_maybe = queued_ops.pop_front() }
        if op_maybe.is_none() { kind = OpKind::Chain; op_maybe = chain_manager.get_op() }
        
        let op = if let Some(x) = op_maybe { x } else {
            return;
        };

        // We've got an Op
        self.cur_op_id += 1;
        let op_id = OpId::new(self.cur_op_id);

        // Record as in-progress
        self.in_progress = Some(OpInProgress {
            op_id,
            kind,
            expires: op.expires,
            channel: op.channel,
        });

        // Start the Op
        thread_manager.start_op(Op {
            db_arc: us.db_arc.clone(),
            us_arc: us.arc.clone(),
            op_id,
            conn_id,
            payload: op.payload,
        });
    }
    fn tick(&mut self, op_results: &mut Vec<OpResult>, conn_id: ConnId, now: SystemTime) {
        if let Some(ref ip) = self.in_progress.as_ref() {
            if now > ip.expires {
                info!("Op timed out: {:?}", ip.op_id);
                op_results.push(OpResult {
                    conn_id,
                    op_id: ip.op_id,
                    result: Err(threaded::Error::TimedOut),
                });
            }
        }
    }
    fn complete(&mut self, op_id: OpId) -> Option<OpInProgress> {
        self.in_progress.take().and_then(|ip| {
            if ip.op_id != op_id {
                info!("Op died because of op_id mismatch - could be timeout");
                None
            } else {
                Some(ip)
            }
        })
    }
}


impl ChainManager {
    fn new() -> Self {
        ChainManager {
            chains: VecDeque::new(),
            chain_sent: Vec::new(),
        }
    }
    fn is_empty(&self) -> bool { self.chains.is_empty() }
    fn just_sent(&mut self, hash: &Multihash) -> bool {
        let mut sent_prime = Vec::new();
        let now = SystemTime::now();
        let mut found = false;
        for (created, sent_hash) in self.chain_sent.drain(..) {
            let since = if let Ok(x) = now.duration_since(created) { x } else {
                continue; // Weird clock skew - drop the item
            };
            if since.as_secs() > 0 {
                continue; // Timed out - drop the item
            }
            found = found || &sent_hash == hash;
            sent_prime.push((created, sent_hash));
        }
        self.chain_sent = sent_prime;
        found
    }
    fn push(&mut self, mut chain: Chain, root_hash: &Multihash) {
        // Replace root
        let root_block_alias = ChainAlias::root();
        let mut chain_prime = Chain::with_capacity(chain.len());
        for item in chain.drain_all() {
            if let Left(ba) = item.block {
                let block = if ba != root_block_alias { Left(ba) } else {
                    Right(root_hash.clone())
                };
                chain_prime.push_back(ChainItem {
                    block,
                    ..item
                });
            } else {
                chain_prime.push_back(item);
            }
        }

        // Add it to the front
        self.chains.push_front((SystemTime::now(), chain_prime));
    }
    fn update(&mut self, hash: &Multihash, block: &Block) {
        // Try to parse it as ESON
        let eson_maybe = Eson::from_bytes(block.get_contents()).ok();

        // Spin through the chains and chain items, discarding and resolving
        let now = SystemTime::now();
        let mut chains_prime = VecDeque::new();
        for (created, chain) in self.chains.drain(..) {
            let since = if let Ok(x) = now.duration_since(created) { x } else {
                continue; // Weird clock skew - drop the chain
            };
            if since.as_secs() > 0 {
                continue; // Timed out - drop the chain
            }

            // It's still live - resolve
            let (mut chain_prime, again) = Self::resolve_chain(hash, &eson_maybe, chain);
            if again {
                // Run it again if there was at least one include
                chain_prime = Self::resolve_chain(hash, &eson_maybe, chain_prime).0;
            }
            if !chain_prime.is_empty() {
                chains_prime.push_back((created, chain_prime));
            }
        }

        self.chains = chains_prime;
    }
    fn get_op(&mut self) -> Option<QueuedOp> {
        let now = SystemTime::now();
        for _ in 0..self.chains.len() {
            let mut chain = self.chains.pop_front().unwrap();
            let since = if let Ok(x) = now.duration_since(chain.0) { x } else {
                // Weird clock skew - drop the chain
                continue;
            };
            if since.as_secs() >= CHAIN_DURATION_S {
                continue; // Timed out - drop the chain
            }

            if let Some(item) = chain.1.pop_front() {
                if let Right(hash) = item.block {
                    let expires = chain.0 + Duration::new(CHAIN_DURATION_S, 0);
                    self.chains.push_back(chain);
                    // Reflect that we're sending a chained block
                    self.chain_sent.push((now, hash.clone()));
                    return Some(QueuedOp {
                        expires,
                        channel: util::default_channel(),
                        payload: OpPayload::BlockGet {
                            hash,
                            chain: None,
                            from_chain: true,
                        },
                    });
                } else {
                    chain.1.push_front(item);
                    self.chains.push_back(chain);
                }
            }
        }
        None
    }
    fn resolve_chain(hash: &Multihash, eson_maybe: &Option<Eson>, mut chain: Chain)
        -> (Chain, bool)
    {
        let root_block_alias = Left(ChainAlias::root());

        let mut again = false;
        // Spin through the chain items, updating them if the hash or alias matches
        let mut chain_prime = Chain::with_capacity(0);
        let mut aliases = HashMap::new();
        for item in chain.drain_all() {
            match item.block {
                Right(h) => {
                    // Do nothing if the hash doesn't match
                    if h != *hash {
                        chain_prime.push_back(ChainItem {
                            block: Right(h),
                            ..item
                        });
                        continue;
                    }

                    // If it does and there's no ESON here, let the item disappear
                    let eson = if let &Some(ref x) = eson_maybe { x } else {
                        continue;
                    };

                    // OK, we've got the block that this item is referencing
                    match item.selector {
                        ChainSelector::Wildcard(key) => {
                            if let Some(vec_val) = eson.get(&key) {
                                for val in vec_val {
                                    if let Ok(h) = Multihash::from_value(val.clone()) {
                                        chain_prime.push_back(ChainItem {
                                            block: Right(h.clone()),
                                            selector: ChainSelector::Resolved,
                                        });
                                    }
                                }
                            }
                        },
                        ChainSelector::Key(key) => {
                            if let Some(val) = eson.get(&key).and_then(|x| x.first()) {
                                if let Ok(h) = Multihash::from_value(val.clone()) {
                                    chain_prime.push_back(ChainItem {
                                        block: Right(h.clone()),
                                        selector: ChainSelector::Resolved,
                                    });
                                }
                            }
                        },
                        ChainSelector::KeyWithAlias(key, into_alias) => {
                            if let Some(val) = eson.get(&key).and_then(|x| x.first()) {
                                if let Ok(h) = Multihash::from_value(val.clone()) {
                                    chain_prime.push_back(ChainItem {
                                        block: Right(h.clone()),
                                        selector: ChainSelector::Resolved,
                                    });
                                    aliases.insert(into_alias, h);
                                }
                            }
                        },
                        ChainSelector::Include(key) => {
                            if let Some(val) = eson.get(&key).and_then(|x| x.first()) {
                                if let Ok(mut c) = Chain::from_value(val.clone()) {
                                    // Put the included chain into the one we're building, while
                                    // replacing "0" with that block's hash.  Reverse and
                                    // push_front to append to the beginning (in effect replacing
                                    // the include item with its contents)
                                    for ci in c.drain_all().rev() {
                                        if ci.block == root_block_alias {
                                            chain_prime.push_front(ChainItem {
                                                block: Right(hash.clone()),
                                                ..ci
                                            });
                                        } else {
                                            chain_prime.push_front(ci);
                                        }
                                    }
                                    // We need to run this function again, to intepret the items
                                    // we just put in that has this block's hash
                                    again = true;
                                }
                            }
                        },
                        ChainSelector::Resolved => {
                            // Passthrough (used when name-get injects the naked root-block)
                            chain_prime.push_back(ChainItem {
                                block: Right(hash.clone()),
                                selector: ChainSelector::Resolved,
                            });
                        },
                    }
                },
                Left(alias) => {
                    chain_prime.push_back(if let Some(hash) = aliases.get(&alias) {
                        ChainItem {
                            block: Right(hash.clone()),
                            ..item
                        }
                    } else {
                        ChainItem {
                            block: Left(alias),
                            ..item
                        }
                    });
                }
            }
        }
        (chain_prime, again)
    }
}


impl SubManager {
    fn new() -> Self {
        SubManager {
            cur_id: 0,
            subs: HashMap::new(),
            notify_queue: VecDeque::new(),
            get_queue: VecDeque::new(),
        }
    }
    fn get_id(&mut self) -> SubId {
        self.cur_id += 1;
        SubId(self.cur_id)
    }
    fn clear(&mut self) {
        self.subs.clear();
        self.get_queue.clear();
        self.notify_queue.clear();
    }
    fn drop(&mut self, tasks: &mut Vec<Task>, conn_id: ConnId) {
        self.clear();
        tasks.push(Task::SendMsg(conn_id, ServerMsg::Oob {
            channel: util::default_channel(),
            close_connection: None,
            code: OobCode::DroppedSubs,
            retry_delay_ms: None,
            payload: None,
            encoding: None,
        }));
    }
    fn put(&mut self, caps: Arc<Capabilities>, name: Name, sub: Sub)
        -> Result<(), (&'static str, OobCode)>
    {
        let prev_maybe = self.subs.remove(&name);
        
        // Exit if maxed out
        if prev_maybe.is_none() && self.subs.len() + 1 >= MAX_SUBS {
            return Err(("maximum subs reached", OobCode::InvalidInput));
        }

        // Queue a request to find out what the hash is, unless the client knows already
        if sub.hash.is_none() || prev_maybe.is_none() || prev_maybe.unwrap().hash != sub.hash {
            self.get_queue.push_back(QueuedOp {
                expires: util::seconds_from_now(SUB_OP_TIMEOUT_S),
                channel: util::default_channel(),
                payload: OpPayload::SubNameGet(caps, name.clone()),
            });
        }

        // Record the sub
        self.subs.insert(name, sub);
        Ok(())
    }
    // An error means you should call .drop()
    fn update(&mut self, caps: &Arc<Capabilities>, name: &Name, hash: &Option<Multihash>)
        -> Result<(), ()>
    {
        let mut delete = false;
        if let Some(sub) = self.subs.get_mut(&name) {
            // Don't do anything if the sub's expired (except delete it)
            if let Some(expires) = sub.expires {
                if SystemTime::now() > expires {
                    delete = true;
                }
            } else {
                // If it's live, compare with what we know, and notify if different
                // Special case: if the existing-hash was None, don't notify if the name
                // is non-existant
                let matches = match (hash, &sub.hash) {
                    (&Some(ref l), &Some(Some(ref r))) => l == r,
                    (&None, &Some(None)) => true,
                    (&None, &None) => true,
                    _ => false,
                };
                if !matches {
                    sub.hash = Some(hash.clone());

                    // If we've got too many notifications, flip out
                    if self.notify_queue.len() + 1 >= MAX_SUB_NOTIFICATIONS_QUEUED {
                        return Err(());
                    }

                    self.notify_queue.push_back(QueuedOp {
                        expires: util::seconds_from_now(SUB_OP_TIMEOUT_S),
                        channel: sub.channel.clone(),
                        payload: OpPayload::SubPermissionsCheck {
                            caps: caps.clone(),
                            name: name.clone(),
                            hash: hash.clone(),
                            chain: sub.chain.clone(),
                            previously_permitted: sub.previously_permitted,
                            sub_id: sub.id,
                        },
                    });
                }
            }
        }
        if delete {
            self.subs.remove(&name);
        }
        Ok(())
    }
    fn del_if_once(&mut self, name: &Name, sub_id: SubId) {
        if let Some(sub) = self.subs.get(name) {
            if sub.id != sub_id || !sub.once {
                return;
            }
        } else {
            return;
        }
        self.subs.remove(name);
    }
    fn set_previously_permitted(&mut self, name: &Name, previously_permitted: bool) {
        if let Some(sub) = self.subs.get_mut(name) {
            sub.previously_permitted = previously_permitted;
        }
    }
    fn get_notify_op(&mut self) -> Option<QueuedOp> { self.notify_queue.pop_front() }
    fn get_name_op(&mut self) -> Option<QueuedOp> { self.get_queue.pop_front() }
}


fn run_gc(thread_manager: &mut ThreadManager, user_states: &mut HashMap<Username, UserState>)
    -> bool
{
    if thread_manager.is_full() {
        return false;
    }

    // Find the most GC-worthy account
    let now = Instant::now();
    let mut any = false;
    let (mut username, mut us) = if let Some(x) = user_states.iter().max_by_key(|(_, us)| {
        let gc = &us.gc;
        let since = (now - gc.last_run).as_secs();
        if (gc.bytes_written_since == 0 && gc.limiter_requested.is_none()) ||
           since < BLOCK_UPLOAD_GC_WINDOW_S
        {
            return 0;
        } 
        any = true;
        let lim_since = if let Some(x) = gc.limiter_requested {
            let d = now - x;
            d.as_secs() as f64 + d.subsec_nanos() as f64 * 1e-9
        } else { 0.0 };
        // Winging this algo until I've got some empirical basis for it
        (((((gc.bytes_written_since + gc.bytes_last_collected) as u64 * (since + 1u64)) as f64).ln() +
        lim_since * 256.0) * 1.0e9) as u64
    }) { x } else { return false };

    // If there's nothing immediate, see if there's any accounts due for a periodic GC
    if !any {
        let (oldest_un, oldest_us) = if let Some(x) = user_states.iter().max_by_key(|(_, us)| {
            (now - us.gc.last_run).as_secs()
        }) { x } else { return false };
        if (now - oldest_us.gc.last_run).as_secs() < GC_PERIODIC_S {
            return false;
        }
        username = oldest_un;
        us = oldest_us;
    }

    // Got a user state, start the GC
    thread_manager.start_op(Op {
        db_arc: us.db_arc.clone(),
        us_arc: us.arc.clone(),
        op_id: OpId::new(0),
        conn_id: ConnId(0),
        payload: OpPayload::Gc { username: username.clone() }
    });

    true
}


fn update_limiters(config: &Arc<Config>, user_states: &mut HashMap<Username, UserState>) {
    let now = Instant::now();
    for (_, us) in user_states.iter_mut() {
        let lim = &mut us.limiter;
        let gc = &mut us.gc;
        let plan = if let Some(x) = &lim.plan { x } else {
            continue;
        };

        // Bandwidth
        // - Add this run's data to the usages
        lim.bw_usages.push_back((now, lim.bw_bytes_since));
        lim.bw_bytes_since = 0;

        // - Knock off any usages outside of the window, and total the rest
        let window_start = now - Duration::new(plan.bw_window_s, 0);
        let mut bytes_in_window = 0;
        lim.bw_usages.retain(|(when, bytes)| {
            if *when <= window_start {
                return false;
            } 
            bytes_in_window += bytes;
            true
        });
        let bw_usage = bytes_in_window as f64 / plan.bw_bytes as f64;

        // - Calculate the modulus
        let bw_op_modulus;
        if bw_usage > 1.0 {
            bw_op_modulus = bw_usage.powi(LIMITER_BW_EXPONENT).floor() as u64;
        } else {
            bw_op_modulus = 1;
        }

        // Disk
        let disk_usage = gc.bytes_used as f64 / plan.disk_bytes as f64 ;
        // - Modulus
        let disk_op_modulus;
        if disk_usage > 1.0 {
            let limiter_period_s = config.tick_us * config.limiter_tick_ratio * 1000000;
            if (now - gc.last_run).as_secs() > limiter_period_s as u64 * 2 &&
               gc.bytes_written_since != 0
            {
                // Haven't done a GC lately, jump to the head of the queue before we decide
                gc.limiter_requested = Some(now);
                disk_op_modulus = 1;
            } else {
                disk_op_modulus = disk_usage.powi(LIMITER_DISK_EXPONENT).floor() as u64;
            }
        } else {
            disk_op_modulus = 1;
        }
        // - Warnings
        let recent_warn = (now - lim.email_warning_sent).as_secs() < LIMITER_EMAIL_PERIOD_S;
        let recent_full = (now - lim.email_full_sent).as_secs() < LIMITER_EMAIL_PERIOD_S;
        let mut send_warn = false;
        let mut send_full = false;
        match lim.disk_usage {
            DiskUsage::Ok => {
                if disk_usage > 1.0 {
                    if !recent_full {
                        send_full = true;
                    }
                    lim.disk_usage = DiskUsage::Full;
                } else if disk_usage > LIMITER_DISK_RATIO_WARNING {
                    if !recent_full && !recent_warn {
                        send_warn = true;
                    }
                    lim.disk_usage = DiskUsage::Warning;
                }
            },
            DiskUsage::Warning => {
                if disk_usage > 1.0 {
                    if !recent_full {
                        send_full = true;
                    }
                    lim.disk_usage = DiskUsage::Full;
                } else if disk_usage <= LIMITER_DISK_RATIO_WARNING {
                    lim.disk_usage = DiskUsage::Ok;
                }
            },
            DiskUsage::Full => {
                if disk_usage <= LIMITER_DISK_RATIO_WARNING {
                    lim.disk_usage = DiskUsage::Ok;
                }
            },
        }
        if send_full || send_warn {
            let (name, maybe_template) = if send_full {
                ("disk full", &config.email_template_disk_full)
            } else {
                ("disk warn", &config.email_template_disk_warning)
            };
            if let (Some(template), Some(email_service)) = (maybe_template, &config.email_service) {
                let result = email::send_email(email_service, template, &us.arc.email,
                                               &HashMap::new());
                if let Err(e) = result {
                    alarm!("failed while trying to send '{}' email to {:?}: {:?}",
                           name, us.arc.email, e);
                } else {
                    info!("sent '{}' email to: {:?}", name, us.arc.email);
               }
            } else {
                info!("would have sent '{}' email to: {:?}, but template not configured",
                      name, us.arc.email);
            }
        }
        // Take the most limiting modulus
        lim.op_modulus = bw_op_modulus.max(disk_op_modulus);
    }

}
