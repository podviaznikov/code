use std::{env};
use std::process::Command;
use std::path::{Path};

fn main() {
    // Compile aux.c
    let out_dir = env::var("OUT_DIR").unwrap();
    let cc_status = Command::new("cc").args(&["c/aux.c", "-c", "-I/usr/local/include", "-fPIC",
                                                 "-o"])
                                         .arg(&format!("{}/aux.o", out_dir))
                                         .status().expect("Compilation failed!");
    if !cc_status.success() {
        panic!("Compilation failed!");
    }
    let ar_status = Command::new("ar").args(&["crus", "libaux.a", "aux.o"])
                                      .current_dir(&Path::new(&out_dir))
                                      .status().expect("Converting to .a failed!");
    if !ar_status.success() {
        panic!("Converting to .a failed!");
    }
    
    // Cargo options
    println!("cargo:rustc-link-search=native={}", out_dir);
    println!("cargo:rustc-link-search=native=/usr/local/lib");
    println!("cargo:rustc-link-lib=static=aux");
    println!("cargo:rustc-link-lib=bearssl");
}    

