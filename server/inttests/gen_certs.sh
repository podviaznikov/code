#!/bin/sh

mkdir -p certs

echo -e '\n#######################################################'
echo -e '\nDefaults are OK, but use "localhost" for the FQDN'
echo -e '\n#######################################################'

# Root
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out certs/rootCA.key
openssl req -x509 -new -nodes -key certs/rootCA.key -sha256 -days 1024 -out certs/rootCA.crt

echo -e '\n#######################################################'
echo -e '\nDefaults are OK, but use "*.edsu.example.com" for the FQDN'
echo -e '\n#######################################################'

# test@example.com
openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out certs/edsu.example.com.key
openssl req -new -key certs/edsu.example.com.key -out certs/edsu.example.com.csr
openssl x509 -req -in certs/edsu.example.com.csr -CA certs/rootCA.crt -CAkey certs/rootCA.key -CAcreateserial -out certs/edsu.example.com.crt -days 24000 -sha256

# Chain
cat certs/edsu.example.com.crt certs/rootCA.crt > certs/chain.crt
