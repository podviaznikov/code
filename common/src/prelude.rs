// Configuration
pub const MAX_PAYLOAD_LEN: usize = 63 * 1024;
pub const MAX_BLOCK_LEN: usize = MAX_PAYLOAD_LEN;
pub const MAX_TEXT_MULTIHASH_LEN: usize = 190;
pub const MIN_TEXT_MULTIHASH_LEN: usize = 32;
pub const MAX_DOMAIN_SEG_LEN: usize = 63;
pub const MAX_NAME_LEN: usize = 255;
pub const MAX_USERNAME_LEN: usize = 191;
pub const MAX_TOKEN_LEN: usize = 64;
pub const DEFAULT_SALT_LEN: usize = 8;
pub const SECRET_VALUE_LEN: usize = 64;
pub const TOKEN_UNENCODED_BYTE_LEN: usize = 24;

// Useful data
pub const CURRENT_BLOCK_VERSION: u8 = 0b0000_0001;
pub const NULL_HASH: &'static str = "QmW8f8Cf33pCSz92SeVyN92KiBqNY3VdJHvwZEHofKJqY5";
pub const MULTIHASH_BYTE_LEN: usize = 34;
pub const EDSU_PORT: u16 = 3216;

// Reified ESON keys
// - Name Blocks
pub const KEY_NB_DENY: &'static str = "edsu:deny";
pub const KEY_NB_ALLOW: &'static str = "edsu:allow";
pub const KEY_NB_PREVIOUS: &'static str = "edsu:previous";
pub const KEY_NB_ONCE: &'static str = "edsu:once";
pub const KEY_NB_EXPIRES: &'static str = "edsu:expires";

// - Appendable Blocks
pub const KEY_AB_APPENDED: &'static str = "edsu:appended";
pub const KEY_AB_DENY: &'static str = "edsu:append-deny";
pub const KEY_AB_ALLOW: &'static str = "edsu:append-allow";
pub const KEY_AB_BYTES_MAX: &'static str = "edsu:append-bytes-max";
pub const KEY_AB_PREVIOUS: &'static str = "edsu:append-previous";
pub const KEY_AB_APPENDED_TTL: &'static str = "edsu:appended-ttl";
pub const KEY_AB_RATE_LIMIT: &'static str = "edsu:append-rate-limit";
pub const FLAG_AB_RATE_SECONDS: &'static str = "seconds";
pub const FLAG_AB_RATE_ALL_USERS: &'static str = "all-users";
pub const FLAG_AB_RATE_PER_USER: &'static str = "per-user";

// - Meta Blocks
pub const KEY_MB_BLOCKS: &'static str = "edsu:blocks";
pub const KEY_MB_METAS: &'static str = "edsu:metablocks";

// - Group Blocks
pub const KEY_GB_USERS: &'static str = "edsu:users";

// - Capabilities Blocks
pub const KEY_CB_VERS: &'static str = "edsu:version";
pub const KEY_CB_NAME_GET: &'static str = "edsu:name-get";
pub const KEY_CB_NAME_PUT: &'static str = "edsu:name-put";
pub const KEY_CB_NAME_APPEND: &'static str = "edsu:name-append";
pub const KEY_CB_OMNI: &'static str = "edsu:omni";
pub const FLAG_CB_DESTRUCTIVE: &'static str = "destructive";
pub const FLAG_CB_VISITOR_GETTABLE: &'static str = "visitor-gettable";
pub const FLAG_CB_VISITOR_APPENDABLE: &'static str = "visitor-appendable";

// Reified Names
pub const NAME_SERVER_TIME: &'static str = "prv.srv.edsu.time.unix";
pub const NAME_BLOCK_EXISTENCE_TEST: &'static str = "prv.srv.edsu.listings.blocks";
pub const NAME_NAME_LIST: &'static str = "prv.srv.edsu.listings.names";
pub const NAME_TOKEN_OWNER_GEN: &'static str = "prv.srv.edsu.authentication.tokens.generate.owner";
pub const NAME_TOKEN_VISITOR_GEN: &'static str =
    "prv.srv.edsu.authentication.tokens.generate.visitor";
pub const NAME_VISITOR_CAPS_ROOT: &'static str =
    "pub.srv.edsu.authentication.visitors.capabilities.";
pub const NAME_AUTH_CHALLENGE_ANSWER_ROOT: &'static str =
    "pub.srv.edsu.authentication.challenge.answer.";
pub const NAME_HOST_CONTACT: &'static str = "pub.srv.edsu.host.contact";


// Srv keys
pub const KEY_SRV_NAME_LIST_AFTER: &'static str = "edsu:after";
pub const KEY_SRV_NAME_LIST_PREFIX: &'static str = "edsu:prefix";
pub const KEY_SRV_NAME_LIST_NAMES: &'static str = "edsu:names";
pub const KEY_SRV_NAME_LIST_TRUNCATED: &'static str = "edsu:truncated";
pub const KEY_SRV_GEN_TOKEN_TTL: &'static str = "edsu:ttl";
pub const KEY_SRV_GEN_TOKEN_TOKEN: &'static str = "edsu:token";
pub const KEY_SRV_HOST_CONTACT_EMAIL: &'static str = "edsu:email";
pub const KEY_SRV_HOST_CONTACT_WWW: &'static str = "edsu:www";


// Oob payloads
pub const OOB_KEY_DEBUG: &'static str = "edsu:debug";
pub const OOB_KEY_ERR: &'static str = "edsu:error";
pub const OOB_ERR_VISITOR_CONN: &'static str = "visitor-connection";



#[derive(PartialEq, Eq, Clone, Hash, Debug)]
pub enum Either<T, U> {
    Left(T),
    Right(U),
}

