#![allow(unused_imports, dead_code)]
use std::{env, io, fs, convert};
use std::path::{Path, PathBuf};
use edsu_common::edsu::{self, Username, Multihash, ValueRead, Value};
use edsu_common::eson::{self, Eson};
use edsu_server::{util};


#[derive(Debug)]
pub enum Error {
    NotFound,
    Io(io::Error),
    Eson(eson::Error),
    Edsu(edsu::Error),
}

impl convert::From<eson::Error> for Error { fn from(e: eson::Error) -> Self { Error::Eson(e) } }
impl convert::From<edsu::Error> for Error { fn from(e: edsu::Error) -> Self { Error::Edsu(e) } }

impl convert::From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        if e.kind() == io::ErrorKind::NotFound {
            Error::NotFound
        } else {
            Error::Io(e)
        }
    }
}

pub struct ConfDir {
    root_dir: PathBuf,
}

impl ConfDir {
    pub fn open() -> Self {
        // Get the dir
        // - Try EDSU_HOME first
        let root_dir = if let Ok(x) = env::var("EDSU_HOME") {
            PathBuf::from(x)
        } else {
            // Then XDG_CONFIG_HOME
            if let Ok(x) = env::var("XDG_CONFIG_HOME") {
                let mut pb = PathBuf::from(x);
                pb.push(".edsu");
                pb
            } else {
                // Then fall back on ~/.edsu
                util::expand_tilde(&PathBuf::from("~/.edsu"))
            }
        };
        ConfDir { root_dir }
    }
    pub fn default_user_put(&self, user: &Username) -> Result<(), Error> {
        let mut path = self.root_dir.clone();
        path.push("default_user");
        bytes_to_file_recursive(&path, &user.to_string().as_bytes())?;
        Ok(())
    }
    pub fn default_user_get(&self) -> Result<Username, Error> {
        let mut path = self.root_dir.clone();
        path.push("default_user");
        let bytes = trim_right_bytes(util::file_to_bytes(&path)?);
        Ok(edsu::from_bytes(&bytes)?)
    }
    pub fn cert_put(&self, user: &Username, cert: &Eson) -> Result<(), Error> {
        let mut path = self.root_dir.clone();
        path.push("certificates");
        fs::DirBuilder::new().recursive(true).create(&path)?;
        path.push(user.to_fs_safe_string());
        let mut bytes = Vec::new();
        cert.write_into(&mut bytes).unwrap();
        bytes_to_file_recursive(&path, &bytes)?;
        Ok(())
    }
    pub fn cert_get(&self, user: &Username) -> Result<Eson, Error> {
        let mut path = self.root_dir.clone();
        path.push("certificates");
        path.push(user.to_fs_safe_string());
        Ok(Eson::from_bytes(&trim_right_bytes(util::file_to_bytes(&path)?))?)
    }
    pub fn token_put(&self, user: &Username, token: &Value) -> Result<(), Error> {
        let mut path = self.root_dir.clone();
        path.push("tokens");
        fs::DirBuilder::new().recursive(true).create(&path)?;
        path.push(user.to_fs_safe_string());
        bytes_to_file_recursive(&path, token.as_bytes())?;
        Ok(())
    }
    pub fn token_get(&self, user: &Username) -> Result<Value, Error> {
        let mut path = self.root_dir.clone();
        path.push("tokens");
        path.push(user.to_fs_safe_string());
        Ok(Value::from_bytes(&trim_right_bytes(util::file_to_bytes(&path)?))?)
    }
    pub fn get_root(&self) -> &Path { &self.root_dir }
}

fn trim_right_bytes(mut x: Vec<u8>) -> Vec<u8> {
    while !x.is_empty() && [0x0a, 0x0d, 0x20].contains(&x[x.len()-1]) {
        let new_len = x.len()-1;
        x.truncate(new_len);
    }
    x
}

fn bytes_to_file_recursive(path: &Path, bytes: &[u8]) -> Result<(), Error> {
    match util::bytes_to_file(&path, bytes, None) {
        Ok(_) => Ok(()),
        Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
            // Missing directory structure - try to create, and try again
            fs::DirBuilder::new()
                .recursive(true)
                .create(&path.parent().unwrap())?;
            bytes_to_file_recursive(path, bytes)
        },
        Err(e) => Err(e)?,
    }
}
