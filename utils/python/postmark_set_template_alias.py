import requests

def set_alias(api_key, template_id, name, alias):
    url = 'https://api.postmarkapp.com/templates/{}'.format(template_id)
    resp = requests.put(url,
                        headers={'Accept': 'application/json',
                                 'Content-Type': 'application/json',
                                 'X-Postmark-Server-Token': api_key},
                        json={'Name': name,
                              'Alias': alias})
    print('Got: {}', resp.status_code)
    return resp.json()
